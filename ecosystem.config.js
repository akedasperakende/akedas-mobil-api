module.exports = {
	apps: [{
		name: 'akedas-api',
		script: 'yarn start',
		args: '',
		instances: 1,
		autorestart: true,
		watch: false,
		max_memory_restart: '8G',
		env: {
			NODE_ENV: 'development'
		},
		env_production: {
			NODE_ENV: 'production',
			HOST: 'https://example.app',
			BLOCK_IP: '',
			IPS: '',
			PORT: 8005,
			LOGLEVEL: 'error',
			NODEID: 'main-1',
			MAINNODE: 'main-1',
			DB: 'akedasdb',
            CDN:"https://example.app/example",
			JWT_SECRET: 'akedaselektrikperakende', 
			HASH_SECRET: 'akedaselektrikperakende',
			MONGO_URL: 'mongodb://localhost:27017/admin',
			ONESIGNAL_APPID:'',
			ONESIGNAL_APIKEY:'', 
			SMTP_HOST:"smtp.yandex.com",
			SMTP_PORT:"465",
			SMTP_EMAIL:"noreply@example.com",
			SMTP_NAME:"example",
			SMTP_PASSWORD:""
		}
	}
	]
};