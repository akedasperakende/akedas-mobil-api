'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');

module.exports = {
	name: 'surveys',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'surveys',


	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			name:{type: 'string'},
			lang:{type: 'string', optional: true },
			active: 'boolean',
			created: 'date',
			// endDate: 'date',
			// startDate: 'date',
			registeredUsers: [{type :'array', items: 'string', optional: true}],
			usersFile: {'type': 'object', optional: true, props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}},
			questions: {
				'type': 'array',
				items: {
					type: 'object',
					props: {
						type: {type: 'string', enum: ['S','F','M']},
						name: 'string',
						is_answered: 'boolean',
						other: {type: 'string', optional: true},
						choices: {
							type: 'array',
							items: {
								type: 'object',
								props: {
									name: 'string',
									is_other: 'boolean',
									other: 'string',
									is_selected: 'boolean'
								}
							}
						}
					}
				}
			}
		}	
	},

	actions: {
		// async archive() {
		// 	this.adapter.updateMany({
				
		// 	}, {
		// 		$set : {
		// 			archive: true
		// 		}
		// 	});
		// },
		async byUser (ctx) {
			let user = ctx.params.user;
		
			let query;
				query = {
					active: true,
					$or: [{registeredUsers: user._id}, {registeredUsers: {$size: 0}}],
					lang : ctx.params.lang
				}
			

			let now = new Date();
			let surveys = await this._find(ctx, {
				query: query,
				fields: ['_id', 'name']
			});

			let filleds = await this.broker.call('surveyAnswers.find', 
				{query: {
					registered_user_id  : user._id,
					survey_id: {$in: surveys.map(x => x._id)} 
				}});

			surveys = surveys.map(survey => {
				let filled = filleds.find(f => f.survey_id === survey._id);
				if(filled){
					survey.completed = true;
				}

				return survey;
			});
			surveys = surveys.filter(x => x.completed !=true);
			return surveys;
		},
	

		async byUserId (ctx) {
			let user = ctx.params.user;
			let survey = await this.broker.call('surveys.get', {id: ctx.params.id , fields: ['_id', 'name', 'completed', 'questions'] });
			let filleds = await this.broker.call('surveyAnswers.find', 
				{query: {
					registered_user_id  : user._id,
					survey_id: survey._id
				}});

			if(filleds.length > 0){
				survey.completed = true;
				survey.questions = filleds[0].questions;
			}

			return survey;

		},

	},

	entityCreated: (json, ctx) => {
		let surveyIcon = {
			'_id' : 'survey.png',
			'url' : process.env.CDN + '/modules/survey.png',
			'thumb' :  process.env.CDN + '/modules/survey.png',
			'type' : 'image',
			'mimeType' : 'image/png'
		};
		
		ctx.broker.call('pushNotifications.create', {
			date: json.created,
			title: json.name,
			active: false,
			registeredUsers: json.registeredUsers,
			content: 'Bekleyen bir anketiniz var' ,
			completed: false,
			icon : surveyIcon,
			media: surveyIcon,
			type: 'survey'
		});
	},

	hooks: {
		before: {
			list: [(ctx) => {
				let query = ctx.params.query ;
				
				if(query && query.archive === 'true'){
					query.archive = query && query.archive === 'true';
				}
				else if(query) query.archive = null;
				
			}],
			create: [(ctx) => {
				
				let survey = ctx.params;
				survey._id = nanoid(25);
				survey.created = new Date();
				// survey.startDate = dayjs(survey.startDate).toDate();
				// survey.endDate = dayjs(survey.endDate).toDate();
				survey.active = survey.active || false;
				survey.registeredUsers = survey.registeredUsers || [];
				survey.questions.forEach(q => {
					q.is_answered = false;
					if(q.choices)
						q.choices.forEach((c,i) => {
							c.other = '';
							c.is_other = i < (q.choices.length - 1) ? false : (c.is_other || false);
							c.is_selected = false;
						});
					else q.choices = [];
				});

			}],

			update: [(ctx) => {
				let survey = ctx.params;
				survey.created = dayjs(ctx.params.created || new Date()).toDate();
				// survey.startDate = dayjs(survey.startDate).toDate();
				// survey.endDate = dayjs(survey.endDate).toDate();
				survey.active = survey.active || false;
				survey.registeredUsers = survey.registeredUsers || [];
				survey.questions.forEach(q => {
					q.is_answered = false;
					if(q.choices)
						q.choices.forEach((c,i) => {
							c.other = '';
							c.is_other = i < (q.choices.length - 1) ? false : (c.is_other || false);
							c.is_selected = false;
						});
					else q.choices = [];
				});

			}]
		}
	}
};