'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let nanoid = require('nanoid');

let icon =  {
	'_id': 'notification.png',
	'url': process.env.CDN + '/general/notification.png',
	'thumb': process.env.CDN + '/general/notification.png',
	'type': 'image',
	'mimeType': 'image/png'
}

module.exports = {
	name: 'notifications',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'notifications',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			title: {type: 'string'},
			active: 'boolean',
			isIndividual: 'boolean',			
			muhatapNo:{ type: 'string', optional: true },
			sendNotification: {type: 'boolean', optional: true},
			pushNotificationId: {type: 'string', optional: true },
			item_id: {type: 'string', optional: true},
			content: 'string',
			date: 'date',
			created: 'date',
			registeredUserId: {type: 'string', optional: true },
			lang:{ type:'string', optional:true },
		//	groups:{ type:'array', items:'string', optional:true },
		//	type: {type: 'string', enum: ['not_modul', 'notifications', 'pushNotifications',  'survey', 'banners','user']},
			area: [{ type: 'array', items: 'string', optional: true }],
			district: [{ type: 'array', items: 'string', optional: true }],
			neighbourhood: [{ type: 'array', items: 'string', optional: true }],
			subtype: {type: 'string', optional: true},
			isRead: 'boolean',
			icon: {'type': 'object', props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}},
			// media: {'type': 'object', props: {
			// 	type: {type:'string'},
			// 	thumb: {type:'string'},
			// 	url: {type:'string'},
			// 	_id: {type:'string'},
			// 	mimeType: {type:'string'}
			// }},
			readUsers: {type: 'array', optional:true, items: {type: 'object', props: {
				id: 'string',
				date: 'date'
			}}}
		}
	},

	actions: {
		updateAll: {
			handler(ctx){
				this.adapter.updateMany({pushNotificationId: ctx.params.pushNotificationId}, {
					$set: {
						...ctx.params
					}
				})
			}
		},

		async delete(ctx) {
			let x = ctx.params
			let result ;
			try {
				await Promise.all(x.ids.map(async (id) => {
					let notification = await ctx.broker.call('notifications.get', { id })

					console.log("id: "+notification.registeredUserId)
					console.log("user: "+ctx.meta.user._id )
					if(notification.registeredUserId === ""){
						console.log("boş")
						let [resultMessage] = await ctx.call('resultMessage.find',{ query: {code: "0080",lang:ctx.params.lang}, fields:["type","title","message"]})
						
						result = {
							result: false,
							result_message: resultMessage
						}	
					}
					else if (ctx.meta.user._id !== notification.registeredUserId){
						console.log("silmek")
						throw Error('Silmek istediğiniz bildirim bulunamadı.')
					}
					else{
						await ctx.broker.call('notifications.remove', { id })
						result =  {
							result: true,
							result_message: {
								type: 'success',
								title: 'Bilgi',
								message: x.ids.length > 1 ? 'Bildirimler başarılı bir şekilde silinmiştir.' : 'Bildirim başarılı bir şekilde silinmiştir.'
							}
						};
					}	
				}))
				return result;	
			}
			catch (err) {
				if (err)
					throw Error('Silmek istediğiniz bildirim bulunamadı.')
			}
		},

		async readNotification(ctx) {
			let notificationId = ctx.params._id
			let meId = ctx.params.registeredUserId

			let [notification] = await ctx.call("notifications.find",{
				query:{
					_id:notificationId
				}   
			})

			if(notification){ 
				if(notification.registeredUserId === "" ){
					if(notification.readUsers){
						let me  = notification.readUsers.filter(x => x._id == meId)
						console.log(me.length)
						if(me.length === 0 ){
							await this.adapter.updateById(notificationId,
								{$push: {
									readUsers: {_id: meId, date: new Date()},
								}}
							);
							this.clearCache()
						}
					}
					else{
						await this.adapter.updateById(notificationId,
                        	{$push: {
                            	readUsers: {_id: meId, date: new Date()},
                        	}}
						);
						this.clearCache()
					}
					
				}
				else{
					await this.adapter.updateById(notificationId,
						{$set: {
                            isRead: true,
                        }}
					);
					this.clearCache()
				}
			}
		},

		async notificationCount(ctx) {
			let registeredUserId = ctx.params._id;
			let groupQwry = ctx.params.groupQwry;
			let lang = ctx.params.lang;
			let i = 0;
			
			let notifications = await ctx.call("notifications.find",{
				query:{
					lang: lang,
					active:true,
					isRead:false,
					"$or": groupQwry ? groupQwry : [{ "groups": { $size: 0 } }],
					"$or":[ {registeredUserId :registeredUserId}, {registeredUserId : ""} ]
				}   
			})

			let pinNotifications  = notifications.filter(x => x.registeredUserId === "")

			let readPinNotificationsCount  = pinNotifications.filter(x => x.readUsers.some(y => registeredUserId.includes(y._id))).length

			// if(pinNotifications){
			// 	pinNotifications.forEach(element => {
			// 		if(element.readUsers){
			// 			let me  = element.readUsers.filter(x => x._id == registeredUserId)
			// 			if(me.length !== 0 ){
			// 				i = i + 1
			// 			}
			// 		}
			// 	});
			// }
			
			let count = notifications.length - readPinNotificationsCount;

			return count;
		},

		async itemsRemove (ctx) {
			let item_id = ctx.params.item_id;

			let many = await this.adapter.removeMany({
				item_id: item_id
			});

		}
	},

	hooks: {
		before: {
			insert: [ctx => {
				ctx.params.entities.map(e => {
					e._id = nanoid(25);
					e.created = new Date();
					e.isRead = false;
				//	e.type = e.type  || 'notification';
					e.icon = e.icon || icon;
				//	e.media = e.media || icon;
				//	e.groups = e.groups || [],
				e.area = e.area || [],
				e.district = e.district || [],
				e.neighbourhood = e.neighbourhood || [],
					e.lang= e.lang || 'TR',
					e.subtype = e.subtype || ''
				});
			}],
			create: [ctx => {
				ctx.params._id = nanoid(25);
				ctx.params.created = new Date();
				ctx.params.isRead = false;
				//ctx.params.type = ctx.params.type  || 'notification';
				ctx.params.icon = ctx.params.icon || icon;
			//	ctx.params.media = ctx.params.media || icon 
				ctx.params.subtype = ctx.params.subtype || ''
			}]
		}
	}
};