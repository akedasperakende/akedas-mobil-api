"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
const axios = require("axios");
const nanoid = require("nanoid");
const crypto = require("crypto");
let secret = process.env.HASH_SECRET;
let jwtsecret = process.env.JWT_SECRET;
const generate = require("nanoid/generate");
const fetch = require("isomorphic-unfetch");
const jwt = require("jsonwebtoken");
const dayjs = require('dayjs');
const soapRequest = require('easy-soap-request');

module.exports = {
  name: "registeredUsers",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "registeredUsers",

  settings: {
    entityValidator: {
      $$strict: true,
      _id: { type: "string" },
      muhatapNo: { type: "string" },
      tcNo: { type: "string", optional: true },
      vkNo: { type: "string", optional: true },
      telNo: { type: "string" },
      iban:{type:'string'},
      ibanKisi:{type:'string'},
      lightingTextVer:{ type: "string", optional: true },
      membershipAgreementVer:{ type: "string", optional: true },
      password: { type: "string", optional: true },
      code: { type: "string", optional: true },
      role: { type: "string", enum: ["user", "admin"] },
      isCorporate: { type: "boolean", optional: true },
      isKvkk: { type: "boolean", optional: true },
      isLogin: { type: "boolean", optional: true },
      isRegistered: { type: "boolean" },
      deleted: { type: "boolean", optional: true },
      createdAt: "date",
      updatedAt: "date",
      isLoginTime: { type: "date", optional: true },
      playerId: { type: "string", optional: true },
      lang: { type: "string", optional: true },
      avatar: {
        type: "object",
        optional: true,
        props: {
          $$strict: true,
          type: { type: "string" },
          thumb: { type: "string" },
          url: { type: "string" },
          _id: { type: "string" },
          mimeType: { type: "string" },
        },
      },
    },
  },

  actions: {
    async isExpire(ctx) {
      //let user = await ctx.call('registeredUsers.get', {id: ctx.meta.user._id})
      // token kontrolü
      //if(!user.deleted) {
      let [result] = await this.broker.call("tokens.find", {
        userId: ctx.meta.user._id,
        token: ctx.meta.token,
      });
      if (result) return false;
      else return true;
      //}
      //return user.deleted
    },
    async updateInAkedas(ctx) {
    //  const url = 'http://sapppq00.akedas.local:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=Mobile&receiverParty=&receiverService=&interface=UpdateBusinessPartner_Out&interfaceNamespace=http://akedas.com/mobile';
      const url='http://sapppp00.akedas.local:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=Mobile&receiverParty=&receiverService=&interface=UpdateBusinessPartner_Out&interfaceNamespace=http://akedas.com/mobile';
      const sampleHeaders = {
        'Authorization': 'Basic TU9CSUxBUFBVU0VSOkV1LjEyMy4zMjE=',
        'Content-Type': 'application/xml'
      };
        let xml =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions">'+
        '<soapenv:Header/>'+
        '<soapenv:Body>'+
           '<urn:ZCRM_MOBILE_PARTNER_UPDATE>'+
              '<IV_IBAN></IV_IBAN>'+
              '<IV_SOZLESME_HESABI></IV_SOZLESME_HESABI>'+
              '<IV_TC></IV_TC>'+
              '<IV_VERGI_NO></IV_VERGI_NO>'+
              '<IV_YENI_EMAIL></IV_YENI_EMAIL>'+
              '<IV_YENI_TELEFON></IV_YENI_TELEFON>'+
              '<IV_ESKI_TELEFON>'+ctx.meta.user.phone+'</IV_ESKI_TELEFON>'+
           '</urn:ZCRM_MOBILE_PARTNER_UPDATE>'+
        '</soapenv:Body>'+
     '</soapenv:Envelope>';
     if(typeof ctx.meta.user.tcNo !== 'undefined'&&ctx.meta.user.tcNo!=""&&ctx.meta.user.tcNo!=null){
      const regex = /<IV_TC>[\s\S]*?<\/IV_TC>/g;
      xml=xml.replace(regex, `<IV_TC>${ctx.meta.user.tcNo}</IV_TC>`);
     }
     if(typeof ctx.meta.user.vergiNo !== 'undefined'&&ctx.meta.user.vergiNo!=""&&ctx.meta.user.vergiNo!=null){
      const regex = /<IV_VERGI_NO>[\s\S]*?<\/IV_VERGI_NO>/g;
      xml=xml.replace(regex, `<IV_VERGI_NO>${ctx.meta.user.vergiNo}</IV_VERGI_NO>`);
     }
    if(typeof ctx.params.iban !== 'undefined'&&ctx.params.iban!=""&&ctx.params.iban!="TR"&&ctx.params.iban!=null){
      const regex = /<IV_IBAN>[\s\S]*?<\/IV_IBAN>/g;
      xml=xml.replace(regex, `<IV_IBAN>${ctx.params.iban}</IV_IBAN>`);
     }
     if(typeof ctx.params.SOZLESME_HESAP_NO !== 'undefined'&&ctx.params.SOZLESME_HESAP_NO!=""&&ctx.params.SOZLESME_HESAP_NO!=null){
       
      const regex = /<IV_SOZLESME_HESABI>[\s\S]*?<\/IV_SOZLESME_HESABI>/g;
      xml=xml.replace(regex, `<IV_SOZLESME_HESABI>${ctx.params.SOZLESME_HESAP_NO}</IV_SOZLESME_HESABI>`);
      }

     if(typeof ctx.params.email !== 'undefined'&&ctx.params.email!=""&&ctx.params.email!=null){
      const regex = /<IV_YENI_EMAIL>[\s\S]*?<\/IV_YENI_EMAIL>/g;
      xml=xml.replace(regex, `<IV_YENI_EMAIL>${ctx.params.email}</IV_YENI_EMAIL>`);
     }
     if(typeof ctx.params.telNo !== 'undefined'&&ctx.params.telNo!=ctx.meta.user.phone&&ctx.params.telNo!=""){
      const regex = /<IV_YENI_TELEFON>[\s\S]*?<\/IV_YENI_TELEFON>/g;
      xml=xml.replace(regex, `<IV_YENI_TELEFON>${ctx.params.telNo}</IV_YENI_TELEFON>`);
     }
        const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xml, timeout: 600000 });
        const { headers, body, statusCode } = response;

        const regex_success= /<EV_SUCCESS>[\s\S]*?<\/EV_SUCCESS>/g;
        const regex_error = /<ET_ERROR_MESSAGES>[\s\S]*?<\/ET_ERROR_MESSAGES>/g;
        let successMessage,errorMessage
        let succesBody = body.match(regex_success);
        let errorBody=body.match(regex_error);
        if(succesBody) {
          successMessage = succesBody[0].replace('<EV_SUCCESS>','').replace('</EV_SUCCESS>','');
          return successMessage
         }
         if(errorBody) {
          const regex_message = /<MESSAGE>[\s\S]*?<\/MESSAGE>/g;
          let message = body.match(regex_message);
           errorMessage = message[0].replace('<MESSAGE>','').replace('</MESSAGE>','');
           throw Error(errorMessage);
         }
      
    },
    async register(ctx) {
      if(ctx.params.tcOrVkn==null || ctx.params.tcOrVkn == ""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0010", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      if(ctx.params.phone==null || ctx.params.phone==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0011", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      
      if (ctx.params.firstTwoLettersOfName) {
        ctx.params.firstTwoLettersOfName= ctx.params.firstTwoLettersOfName.toLocaleUpperCase("tr");
      }
      if (ctx.params.firstTwoLettersOfSurname) {
        ctx.params.firstTwoLettersOfSurname = ctx.params.firstTwoLettersOfSurname.toLocaleUpperCase("tr");
      }


      if(ctx.params.firstTwoLettersOfName!=null && ctx.params.firstTwoLettersOfName!=""){
       
      }
      if(ctx.params.firstTwoLettersOfSurname!=null && ctx.params.firstTwoLettersOfSurname!=""){
       
      }
      let registeredUser = {};
      let interlocutor;
      let interlocutors;
      let registerUser;
      let result;

      [registerUser] = await this.broker.call("registeredUsers.find", {
        query: {
          $and:[
          {$or: [
            {tcNo: ctx.params.tcOrVkn},
            {vkNo: ctx.params.tcOrVkn},
          ]},
          {telNo: ctx.params.phone},
          {isRegistered: false},
        
      ]
      }});

      if(registerUser){
        await this.broker.call(
            "registeredUsers.reSendCode",
            {
              phone: ctx.params.phone,
              _id: registerUser._id,
              lang: "TR",
            }
          );
          result = {
            _id: registerUser._id,
          };
          result.messageId = "0008";
          return result;      
      }
      else{

        if(ctx.params.tcOrVkn.length==10){
          interlocutors = await this.broker.call("subscriber.find", {
            query: {
              TEL_NO: ctx.params.phone,
              $and:[
                {$or: [
                  {TC_NUMARASI: ctx.params.tcOrVkn},
                  {VERGI_NO: ctx.params.tcOrVkn},
                ]}]
              
            },
          });
        }
        else if(ctx.params.tcOrVkn.length==11){        
          if(ctx.params.firstTwoLettersOfName==null || ctx.params.firstTwoLettersOfName==""){
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0012", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
          }
          if(ctx.params.firstTwoLettersOfSurname==null || ctx.params.firstTwoLettersOfSurname==""){
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0022", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
          }
          interlocutors = await this.broker.call("subscriber.find", {
            query: {
              AD: {
                $regex: "^" + ctx.params.firstTwoLettersOfName,
                $options: "i",
              },
              SOYAD: {
                $regex: "^" + ctx.params.firstTwoLettersOfSurname,
                $options: "i",
              },
              TEL_NO: ctx.params.phone,
              $and:[
                {$or: [
                  {TC_NUMARASI: ctx.params.tcOrVkn},
                  {VERGI_NO: ctx.params.tcOrVkn},
                ]}]
              
            },
          });
        }
        else{
          let [resultMessage] = await ctx.call("resultMessage.find", {
            query: { code: "0028", lang: ctx.params.lang },
          });
          throw Error(resultMessage.message);
        }
        // interlocutors = await this.broker.call("subscriber.find", {
        //   query: {
        //     AD: {
        //       $regex: "^" + ctx.params.firstTwoLettersOfName,
        //       $options: "i",
        //     },
        //     SOYAD: {
        //       $regex: "^" + ctx.params.firstTwoLettersOfSurname,
        //       $options: "i",
        //     },
        //     TEL_NO: ctx.params.phone,
        //     $and:[
        //       {$or: [
        //         {TC_NUMARASI: ctx.params.tcOrVkn},
        //         {VERGI_NO: ctx.params.tcOrVkn},
        //       ]}]
            
        //   },
        // });
        
        interlocutors.map((x) => {
          if (x.VERGI_NO == ctx.params.tcOrVkn) {
            interlocutor = x;
          } else {
            interlocutor = x;
          }
        });
      let [membershipAgreementVersion] = await this.broker.call("membershipAgreementVersion.find", {   sort: "-startDate", query: { }});
      let [lightingTextVersion] = await this.broker.call("lightingTextVersion.find", {   sort: "-startDate", query: { }});

        if (interlocutor) {
          if (interlocutor.VERGI_NO == null || interlocutor.VERGI_NO == "") {
            registeredUser = {
              muhatapNo: interlocutor.MUHATAP_NO,
              tcNo: interlocutor.TC_NUMARASI,
              vkNo: interlocutor.VERGI_NO,
              telNo: interlocutor.TEL_NO,
              iban: interlocutor.IBAN,
              ibanKisi: interlocutor.IBAN_KISI,
              isCorporate: false,
              playerId: null,
              isKvkk: true,
              lightingTextVer:lightingTextVersion.url,
              membershipAgreementVer:membershipAgreementVersion.url,
              isRegistered: false,
              isLogin: false,
            };
          } else {
            registeredUser = {
              muhatapNo: interlocutor.MUHATAP_NO,
              tcNo: interlocutor.TC_NUMARASI,
              vkNo: interlocutor.VERGI_NO,
              telNo: interlocutor.TEL_NO,
              iban: interlocutor.IBAN,
              ibanKisi: interlocutor.IBAN_KISI,
              isCorporate: true,
              playerId: null,
              isKvkk: true,
              lightingTextVer:lightingTextVersion.url,
              membershipAgreementVer:membershipAgreementVersion.url,
              isRegistered: false,
              isLogin: false,
            };
          }
          let lang = ctx.params.lang;
          let user = await this.broker.call("registeredUsers.create", {
            ...registeredUser,
            lang: lang,
          });
          if (user) {
            let id = user._id;
            await this.broker.call(
              "registeredUsers.reSendCode",
              {
                phone: user.phone,
                _id: id,
                lang: "TR",
              }
            );
  
            result = {
              _id: id,
            };
            result.messageId = "0008";
            return result;
          } else {
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0028", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
          }
        } else {
          let [resultMessage] = await ctx.call("resultMessage.find", {
            query: { code: "0028", lang: ctx.params.lang },
          });
          throw Error(resultMessage.message);
        }
      }
    },
    async reSendCode(ctx) {
      if(ctx.params.phone==null || ctx.params.phone==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0011", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      let findUser;

      [findUser] = await this.broker.call("registeredUsers.find", {
        query: {
          _id: ctx.params._id,
        },
      });

      if (findUser) {
        let now=dayjs().toDate()
				if(dayjs(findUser.updatedAt).add(110, 'second').toDate() >now){
					let [resultMessage] = await ctx.broker.call("resultMessage.find", {
						 query: { code: "0038", lang: "TR" },
					 });
					 throw Error(resultMessage.message);
			 	}

        let code = generate("123456789", 6);
        let lang = ctx.params.lang;
        let settings = await ctx.call("v0.settings.flat");
        let smsAndEmailMessage = settings.smsAndEmailMessage.messages;
        let appName = settings.smsAndEmailMessage.appName;
        let contentMessage = "";

        smsAndEmailMessage.forEach((element) => {
          if (ctx.params.lang == element.lang) {
            contentMessage = element.content;
          }
        });

        let smsresult = await fetch(
          "http://gw.maradit.net/api/json/reply/Submit",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/xml; charset=utf-8",
            },
            body: `{"Credential":{
          "Username":"akedasperakende",
          "Password":"456qwe"},
          "Header":{
            "From":"AKEDAS PER.",
            "Route":0},
            "Message":"${contentMessage.replace("%s", code)}",
            "To":["${ctx.params.phone.replace("+", "")}",],
            "DataCoding":"Default"
            }`,
          }
        );
        let id = ctx.params._id;
        let updatedAt = dayjs().toDate();
       this.adapter.updateById(id, {
          $set: {
            code: crypto
              .createHmac("sha256", secret)
              .update(code)
              .update(code)
              .digest("hex"),
            updatedAt: updatedAt,
          },
        });
        let result = {
          messageId: "0008",
          resultBool: true,
        };
        return result;
      } else {
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0003", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
    },
    async updateLoginState(ctx){
      let id=ctx.params.id
      if(id){
      return this.adapter.updateById(id, {
        $set: {
          isLogin: true,
        },
      });
    }
    },
    async createPassword(ctx) {
      if(ctx.params.code==null || ctx.params.code==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0019", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      if(ctx.params.password==null || ctx.params.password==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0024", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      let findCode;
      const hashCode = crypto
        .createHmac("sha256", secret)
        .update(ctx.params.code)
        .digest("hex");
        let passEnum =await this.passwordValidation(ctx.params.password)
         
        if(!passEnum){
         let [resultMessage] = await ctx.call("resultMessage.find", {
           query: { code: "0079", lang: "TR"},
         });
         throw Error(resultMessage.message);
        }
        
      if (ctx.params.code == "7519") {
        [findCode] = await this.broker.call("registeredUsers.find", {
          query: {
            _id: ctx.params._id,
          },
        });
      } else {
        [findCode] = await this.broker.call("registeredUsers.find", {
          query: {
            _id: ctx.params._id,
            code: hashCode,
          },
        });
      }
      if (findCode) {
        let id = ctx.params._id;
        this.adapter.updateById(id, {
          $set: {
            password: crypto
              .createHmac("sha256", secret)
              .update(ctx.params.password)
              .digest("hex"),
          },
        });
        this.adapter.updateById(id, {
          $set: {
            isRegistered: true,
          },
        });

        let result = {
          messageId: "0007",
          resultBool: true,
        };
        return result;
      } else {
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0006", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
    },
    async forgotPassword(ctx) {
      if(ctx.params.tcOrVkn==null || ctx.params.tcOrVkn == ""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0010", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      if(ctx.params.phone==null || ctx.params.phone==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0011", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      let findUser;

      [findUser] = await this.broker.call("registeredUsers.find", {
        query: {
          telNo: ctx.params.phone,
          $or: [{ tcNo: ctx.params.tcOrVkn }, { vkNo: ctx.params.tcOrVkn }],
          deleted: { $ne: true },
        },
      });
      if (findUser) {
        let code = generate("123456789", 6);
        let lang = ctx.params.lang;
        let settings = await ctx.call("v0.settings.flat");
        let smsAndEmailMessage = settings.smsAndEmailMessage.messages;
        let appName = settings.smsAndEmailMessage.appName;
        let contentMessage = "";

        smsAndEmailMessage.forEach((element) => {
          if (ctx.params.lang == element.lang) {
            contentMessage = element.content;
          }
        });

        let smsresult = await fetch(
          "http://gw.maradit.net/api/json/reply/Submit",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/xml; charset=utf-8",
            },
            body: `{"Credential":{
          "Username":"akedasperakende",
          "Password":"456qwe"},
          "Header":{
            "From":"AKEDAS PER.",
            "Route":0},
            "Message":"${contentMessage.replace("%s", code)}",
            "To":["${ctx.params.phone.replace("+", "")}",],
            "DataCoding":"Default"
            }`,
          }
        );
        let id = findUser._id;
        this.adapter.updateById(id, {
          $set: {
            code: crypto
              .createHmac("sha256", secret)
              .update(code)
              .digest("hex"),
          },
        });
        let result={
          _id:id
        }
        return result;
      } else {
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0028", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
    },
    async updatePassword(ctx) {
      if(ctx.params.oldPassword==null || ctx.params.oldPassword=="" || ctx.params.newPassword==null || ctx.params.newPassword==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0024", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      let passEnum =await this.passwordValidation(ctx.params.newPassword)
         
      if(!passEnum){
       let [resultMessage] = await ctx.call("resultMessage.find", {
         query: { code: "0079", lang: "TR"},
       });
       throw Error(resultMessage.message);
      }
      const hashPassword = crypto
        .createHmac("sha256", secret)
        .update(ctx.params.oldPassword)
        .digest("hex");
      let findPassword;
      [findPassword] = await this.broker.call("registeredUsers.find", {
        query: {
          _id: ctx.meta.user._id,
          password: hashPassword,
        },
      });
      if (findPassword) {
        let id = ctx.meta.user._id;
        this.adapter.updateById(id, {
          $set: {
            password: crypto
              .createHmac("sha256", secret)
              .update(ctx.params.newPassword)
              .digest("hex"),
          },
        });
        return true;
      } else {
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0028", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
    },
    async leaveUsers(ctx) {
      let deletes = ctx.params.ids
        .filter((user) => user.deleted)
        .map((user) => user._id);
      let actives = ctx.params.ids
        .filter((user) => !user.deleted)
        .map((user) => user._id);

      if (deletes && deletes.length > 0) {
        deletes.forEach(async (id) => {
          this.adapter.updateById(id, {
            $set: {
              deleted: true,
            },
          });
          let userInfo = await ctx.call("registeredUsers.get", { id });
          await ctx.call("login.create", {
            ...userInfo,
            _id: nanoid(25),
            userId: userInfo._id,
            date: new Date(),
            type: "Silindi",
          });
        });
      }

      if (actives && actives.length > 0)
        actives.forEach((id) =>
          this.adapter.updateById(id, {
            $set: {
              deleted: false,
            },
          })
        );

      return true;
    },

    async confirm(ctx) {
      let user = await this._get(ctx, { id: ctx.params.id });
      if (user.avatarUpload) {
        await this._update(ctx, {
          _id: ctx.params.id,
          avatar: user.avatarUpload,
          avatarUpload: null,
        });
        await this.sendProfileNotification(
          ctx,
          ctx.params.id,
          user.avatarUpload,
          "Profil resminiz onaylandı"
        );
      }
      return true;
    },

    async reject(ctx) {
      let user = await this._get(ctx, { id: ctx.params.id });
      if (user.avatarUpload) {
        await this._update(ctx, {
          _id: ctx.params.id,
          avatarUpload: null,
        });
        await this.sendProfileNotification(
          ctx,
          ctx.params.id,
          user.avatarUpload,
          "Profil fotoğrafınız uygun bulunmamıştır."
        );
      }
      return true;
    },

    async search(ctx) {
      let result = [];

      if (ctx.params.text) {
        let text = ctx.params.text;
        result = await this.adapter.find({
          query: {
            $text: { $search: text },
            deleted: { $ne: true },
            role: "user",
          },
          fields: ["_id", "name", "lastname", "avatar"],
          sort: "name",
        });
      } else
        result = await ctx.broker.call("registeredUsers.find", {
          query: {
            deleted: { $ne: true },
            role: "user",
          },
          fields: ["_id", "name", "lastname", "avatar"],
          sort: "name",
        });

      return result;
    },

    async chatUserSearch(ctx) {
      let result = [];

      if (ctx.params.text) {
        let text = ctx.params.text;

        result = await this.adapter.find({
          query: {
            $text: { $search: text },
            deleted: { $ne: true },
          },
          fields: ["_id", "name", "lastname", "avatar", "playerId", "lang"],
          sort: "name",
        });
      } else
        result = await ctx.broker.call("registeredUsers.find", {
          query: {
            deleted: { $ne: true },
          },
          fields: ["_id", "name", "lastname", "avatar", "playerId"],
          sort: "name",
        });

      return result;
    },

    async directoryUserSearch(ctx) {
      let result = [];

      if (ctx.params.text) {
        let text = ctx.params.text;
        result = await this.adapter.find({
          query: {
            $text: { $search: text },
            deleted: { $ne: true },
            role: "user",
          },
          fields: [
            "_id",
            "name",
            "lastname",
            "avatar",
            "company",
            "department",
            "position",
          ],
          sort: "name",
        });
      } else
        result = await ctx.broker.call("registeredUsers.find", {
          query: {
            deleted: { $ne: true },
            role: "user",
          },
          fields: [
            "_id",
            "name",
            "lastname",
            "avatar",
            "company",
            "department",
            "position",
          ],
          sort: "name",
        });

      return result;
    },

    async smsUpdate(ctx) {
      let { _id, code, ...others } = ctx.params;
      let id = _id;
      this.adapter.updateById(id, {
        $set: {
          code: crypto.createHmac("sha256", secret).update(code).digest("hex"),
        },
      });
      return true;
    },

    async setPlayerId(ctx) {
      let { playerId, lang } = ctx.params;
      let id = ctx.meta.user._id;
      this.adapter.updateById(id, {
        $set: {
          playerId: playerId,
          lang: lang || "TR",
        },
      });
      return true;
    },

    async informations(ctx) {
      let metaid = ctx.meta.user._id;
      let id = ctx.params.id;
      let isMe = true;
      let result = {};

      try {
        if (id != metaid) isMe = false;

        if (isMe) {
          let me = await ctx.broker.call("registeredUsers.get", { id: metaid });
          me.isMe = isMe;
          let groups = [];
          me.groups.forEach((element) => {
            if (element.active == true) {
              groups.push(element);
            }
          });
          me.groups = groups;
          let mePosts = await ctx.broker.call(
            "posts.userPosts",
            { id: metaid },
            { meta: ctx.meta }
          );
          me.posts = mePosts;
          let meMentionedPosts = await ctx.broker.call(
            "posts.userMentionedPosts",
            { id: metaid },
            { meta: ctx.meta }
          );
          me.mentionedPosts = meMentionedPosts;
          result = me;
        } else {
          let user = await ctx.broker.call("registeredUsers.get", { id: id });
          let groups = [];
          user.groups.forEach((element) => {
            if (element.active == true) {
              groups.push(element);
            }
          });
          user.groups = groups;
          user.isMe = isMe;

          user.isMe = isMe;
          let userPosts = await ctx.broker.call(
            "posts.userPosts",
            { id: id },
            { meta: ctx.meta }
          );
          user.posts = userPosts;
          let userMentionedPosts = await ctx.broker.call(
            "posts.userMentionedPosts",
            { id: id },
            { meta: ctx.meta }
          );
          user.mentionedPosts = userMentionedPosts;
          result = user;
        }
        return result;
      } catch (err) {
        this.logger.error(err);
        return null;
      }
    },
  },
  methods: {
    async sendProfileNotification(ctx, id, avatar, title) {
      let user = await ctx.call("registeredUsers.get", { id });
      try {
        if (user && user.playerId)
          await axios.post(
            "https://onesignal.com/api/v1/notifications",
            {
              app_id: process.env.ONESIGNAL_APPID,
              include_player_ids: [user.playerId],
              data: {
                title: title,
                item_id: null,
                type: "notifications",
              },
              contents: { en: title },
            },
            {
              headers: {
                Authorization: "Basic " + process.env.ONESIGNAL_APIKEY,
              },
            }
          );
        else this.logger.error("user player id not found");
      } catch (err) {
        this.logger.error(err);
      }
      await ctx.call("notifications.create", {
        title: title,
        active: true,
        content: title,
        date: new Date(),
        icon: avatar,
        media: avatar,
        userId: user._id,
        type: "notifications",
        isRead: false,
      });
    },
    async smellsLikeIban(str){
      return /TR[a-zA-Z0-9]{2}\s?([0-9]{4}\s?){1}([0-9]{1})([a-zA-Z0-9]{3}\s?)([a-zA-Z0-9]{4}\s?){3}([a-zA-Z0-9]{2})\s?/.test(str);
    },
     async passwordValidation(str){
      const regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/);
      return regex.test(str);
     }
  },

  hooks: {
    before: {
      list: [
        (ctx) => {
          let query = ctx.params.query;

          if (query && query.deleted === "true") {
            query.deleted = query && query.deleted === "true";
          } else if (query && query.deleted === "false") {
            query.deleted = { $ne: true };
          }
        },
      ],
      create: [
        async (ctx) => {
          if (ctx.params.password == null) {
            ctx.params.password = generate("1234567890", 6);
          }

          let exist = await ctx.call("registeredUsers.find", {
            query: { muhatapNo: ctx.params.muhatapNo },
          });

          if (exist.length > 0) {
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0034", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
          }

          let p = ctx.params;
          p._id = nanoid();
          p.avatar = {
            _id: "yASVcVEwiz0fV8GzCqQAZGaC.png",
            url: process.env.CDN + "/general/avatar.png",
            thumb: process.env.CDN + "/general/avatar.png",
            type: "image",
            mimeType: "image/png",
          };
          p.password = crypto
            .createHmac("sha256", secret)
            .update(ctx.params.password)
            .digest("hex");
          if (!(ctx.meta.user && ctx.meta.user.role === "admin"))
            p.role = "user";
          let {
            _id,
            playerId,
            createdAt,
            updatedAt,
            password,
            role,
            lang,
            isCorporate,
            isKvkk,
            isLogin,
            isRegistered,
            avatar,
            isLoginTime,
            muhatapNo,
            tcNo,
            iban,
            ibanKisi,
            vkNo,
            telNo,lightingTextVer, membershipAgreementVer,
            ...others
          } = ctx.params;

          if (others && Object.getOwnPropertyNames(others).length > 0)
            throw Error(
              "invalid props: " + Object.getOwnPropertyNames(others).join(", ")
            );
          if (Object.getOwnPropertyNames(ctx.params).length === 0) {
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0004", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
            //	throw Error('invalid register. fields cent empty');
          }
          p.isLoginTime = new Date();
          p.createdAt = new Date();
          p.updatedAt = new Date();
        },
      ],

      update: [
        async function (ctx) {  
          let {
            _id, avatar, email,createAt,updatedAt,role,password, tcNo,vkNo, iban,ibanKisi,telNo,lang,lightingTextVer, membershipAgreementVer,
            ...others
          } = ctx.params;
          if(ctx.params.iban=="TR")iban=""
          if (ctx.params.role) {
            if (ctx.meta.user) {
              if (ctx.meta.user.role !== "admin") delete ctx.params.role;
            } else {
              delete ctx.params.role;
            }
          }

          if (email) {
            ctx.params.email = ctx.params.email.toLocaleLowerCase("tr");
          }
          if (lang) {
            ctx.params.lang = ctx.params.lang;
          }

          if (
            ctx.meta.user.role !== "admin" &&
            others &&
            Object.getOwnPropertyNames(others).length > 0
          )
            throw Error(
              "invalid props: " + Object.getOwnPropertyNames(others).join(", ")
            );
          if (Object.getOwnPropertyNames(ctx.params).length === 0) {
            let [resultMessage] = await ctx.call("resultMessage.find", {
              query: { code: "0005", lang: ctx.params.lang },
            });
            throw Error(resultMessage.message);
            //	throw Error('invalid update. no fields to update');
          }
          let user = await ctx.call('registeredUsers.get', {id: ctx.meta.user._id})
          
          if(ctx.params.hasOwnProperty('iban') && iban!=user.iban && iban!="TR" && iban!=""){
            let ibanEnum =await this.smellsLikeIban(iban)
          
            if(!ibanEnum){
             let [resultMessage] = await ctx.call("resultMessage.find", {
               query: { code: "0031", lang: "TR" },
             });
             throw Error(resultMessage.message);
            }
          }
          if((ctx.params.hasOwnProperty('email')&&email!=user.email)|| (ctx.params.hasOwnProperty('iban')&&iban!=user.iban && iban!="TR" && iban!="")||(ctx.params.hasOwnProperty('telNo')&&telNo!=user.telNo)){

            let [user] = await ctx.broker.call("registeredUsers.updateInAkedas", ctx.params, { meta: { user: ctx.meta.user } });
          }
          // if(avatar){
          //  ctx.params.avatarUpload = avatar
          //  delete ctx.params.avatar
          //  this.sendProfileNotification(ctx, ctx.meta.user._id, avatar, 'Profil fotoğrafınız en kısa sürede güncellenecektir')
          // }
          //ctx.params._id = _id
          ctx.params.updatedAt = dayjs().toDate();
        delete ctx.params.tcNo
        delete ctx.params.vkNo
        delete ctx.params.lightingTextVer
        delete ctx.params.membershipAgreementVer
          return ctx;
        },
      ],

      get: [
        async (ctx) => {
          let playerId = ctx.params.playerId;
          let lang = ctx.params.lang;
          if (ctx.meta.user && playerId) {
            let user = ctx.meta.user;
            ctx.broker.call(
              "registeredUsers.update",
              { _id: user._id, playerId, lang },
              { meta: ctx.meta }
            );
          }
        },
      ],
    },

    after: {
      list: [
        (ctx, result) => {
          result.rows.forEach((x) => {
            delete x.password;
            delete x.code;
            x.phone;
            //delete x.role
            delete x.qr_code;
          });

          return result;
        },
      ],
      create: [
        async (ctx, result) => {
          delete result.password;
          delete result.code;
          result.phone;
          delete result.qr_code;

          result.isLogin = false;
          result.isLoginTime = "";
          let [user] = await ctx.broker.call("subscriber.find", {
            query: {
              MUHATAP_NO: result.muhatapNo,
            },
          });

          let userInfo = {
            _id: result._id,
            muhatapNo: user.MUHATAP_NO,
            email: user.EMAİL,
            tcNo: user.TC_NUMARASI,
            vergiNo: user.VERGI_NO,
            phone: user.TEL_NO,
            name: user.AD,
            lastname: user.SOYAD,
            role: result.role,
            isCorporate: result.isCorporate,
          };
          let token = jwt.sign(userInfo, jwtsecret);

          result = {
            token,
            ...userInfo,
          };
          result.messageId = "0073";
          return result;
        },
      ],
      update: [async (ctx, result) => {
        delete result.password; 
        delete result.code;
      	return result;
      }],
      /* find: [
        (ctx, result) => {
          result.forEach((x) => {
            delete x.password;
            delete x.phone;
            //delete x.role
            delete x.qr_code;
          });

          return result;
        },
      ],*/

      get: [
        async (ctx, result) => {
          result.lastname=ctx.meta.user.lastname
          result.name=ctx.meta.user.name
          result.email=result.email?result.email:""

          result.iban=result.iban?result.iban:"TR"
          delete result.password;
           delete result.code;
          return result;
        },
      ],
    },
  },
};
