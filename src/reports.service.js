'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let xlxs = require('xlsx');
let dayjs = require('dayjs');

let posts = {
	_id: 'posts',
	title: 'Paylaşım Duvarı',
	sort: '-date',
	fields: [
		{name: 'comment', 	title:'Açıklama', list: 1, type: 'text'},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'date', 		title:'Paylaşım Tarihi', type: 'date', showTime: true, required: true},
		{name: 'coordinate', title: 'Konum', 'type' :'map'},
		{name: 'medias', 	title:'Medyalar', 	type: 'file', multi: true, ext:['image', 'video']},
	]
};

let stories = {
	_id: 'stories',
	title: 'Hikayeler',
	sort: '-startDate',
	filters: [
		{name: 'archive', title: 'Arşiv', type: 'boolean'}
	],
	fields: [
		{name: 'title', 	title:'Açıklama', list: 1, type: 'text', multi: true, required:true},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'startDate', end: 'endDate', list: 1,	title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
		{name: 'icon', 	title:'Ikon', type: 'file', multi: false, ext:['image'], required: false},
		{name: 'items', 	title:'Medyalar', type: 'file', multi: true, ext:['image', 'video'], required: true },
	]
};

let surveys = {
	_id: 'surveys',
	title: 'Anketler',
	sort: '-startDate',
	report: 'survey',
	fields: [
		{name: 'name', 	title:'Başlık', list: 1, type: 'text', required:true},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'startDate', end: 'endDate', list: 1,	title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true}
	]
};

let events = {
	_id: 'events',
	title: 'Etkinlikler',
	sort: '-startDate',
	filters: [
		{name: 'archive', title: 'Arşiv', type: 'boolean'}
	],
	fields: [
		{name: 'name', 	title:'Başlık', list: 1, type: 'text', required:true},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'releaseDate', title: 'Yayın Tarihi', List: 1, type: 'date', required: true},
		{name: 'startDate', end: 'endDate', list: 1,	title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
		//{name: 'showTime', title: 'Saati Göster', type: 'boolean'},
		// {name: 'description', title:'Açıklama', type: 'text', multi: true},
		// {name: 'content', title:'İçerik', type: 'richtext'},
		{name: 'speakers_names', title:'Konuşmacılar', list: 1, type: 'text'},
		{name: 'coordinate', title: 'Konum', 'type' :'map'},
		{name: 'limit', title: 'kota', type: 'number'}
	]
};

let news = {
	_id: 'news',
	title: 'Haberler',
	sort: '-date',
	filters: [
		{name: 'archive', title: 'Arşiv', type: 'boolean'}
	],
	fields: [
		{name: 'name', 	title:'Açıklama', list: 1, type: 'text'},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'date', end: 'endDate', list: 1,	title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
		{name: 'content', title:'İçerik', type: 'richtext', multi: true},
		{name: 'coordinate', title: 'Konum', 'type' :'map'},
		{name: 'icon', 	title:'İkon', type: 'file', multi: false, ext:['image'], required: true},
		{name: 'media', 	title:'İçerik Medya', type: 'file', multi: false, required: true },
	]
};

let notifications = {
	_id: 'pushNotifications',
	title: 'Bildirimler',
	sort: '-date',
	fields: [
		{name: 'title', title:'Başlık', list: 1, type: 'text', required: true},
		{name: 'active', 	title:'Aktif', list: 1,		type: 'boolean', required: true},
		{name: 'date', title: 'Gönderim Zamanı', List: 1, type: 'date', required: true},
		{name: 'content', title:'İçerik', type: 'text', required: true},
		{name: 'type', title: 'Açılacak Sayfa', type:'module'},
		{name: 'icon', 	title:'İkon', type: 'file', multi: false, ext:['image'], required: true},
		{name: 'media', 	title:'Görsel', type: 'file', multi: false, required: true }
	]
};

let businessCategories = {
	_id : 'businesscategories',
	title: 'Kapmanya Kategorileri',
	sort: 'order',
	fields: [
		{name: 'name', title:'İsim', list: 1, type: 'text', required: true},
		{name: 'order', title: 'Sıra', list: 1, type: 'number', required: true}
	]
};


let campaings = {
	_id : 'campaings',
	title: 'Kampanyalar',
	sort: '-startDate',
	filters: [
		{name: 'archive', title: 'Arşiv', type: 'boolean'}
	],
	fields: [
		{name: 'name', title:'İsim', list: 1, type: 'text', required: true},
		{name: 'code', title:'Kod', list: 1, type: 'text'},
		{name: 'active', title:'Aktif', list: 1, type: 'boolean', required: true},
		{name: 'category_id', title: 'Kategori', list: 1, type: 'businessCategory', required: true},
		{name: 'sub_title', title: 'İçerik', type: 'richtext'},
		{name: 'startDate', end: 'endDate', list: 1, title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
		{name: 'cities', title: 'Şehirler', type: 'city', multi: true, required: true},
		{name: 'media', title:'Görsel', type: 'file', multi: false, required: true }
	]
};

let banners = {
	_id : 'banners',
	title: 'Bannerler',
	sort: '-startDate',
	filters: [
		{name: 'archive', title: 'Arşiv', type: 'boolean'}
	],
	fields: [
		{name: 'title', title:'İsim', list: 1, type: 'text'},
		{name: 'order', title:'Sıra', list: 1, type: 'number', required: true},
		{name: 'active', title:'Aktif', list: 1, type: 'boolean', required: true},
		{name: 'startDate', end: 'endDate', list: 1, title:'Başlangıç/Bitiş Tarihi', type: 'date', required: true},
		{name: 'registeredUsers', title:'Kullanıcılar (Sicil No)', type:'registeredUsers', required: false},
		{name: 'type', title: 'Açılacak Sayfa', type:'module'},
		{name: 'media', title:'Banner', type: 'file', multi: false, required: true }
	]
};

let bloodNeeds = {
	_id: 'bloodNeeds',
	title: 'Kan İhtiyacı',
	sort: '-createDate',
	fields: [
		{name: 'contactPerson', title:'İsim', list: 1, type: 'text', required: true},
		{name: 'contactPhone', title:'Telefon', list: 1, type: 'text', required: true},
		{name: 'hospitalName', title:'Hastane', list: 1, type: 'text', required: true},
		{name: 'city', title:'Şehir', list: 1, type: 'text', required: true},
		{name: 'bloodName', title:'Kan Tipi', list: 1, type: 'blood', required: true},
		{name: 'bloodDonation', title:'Kan', list: 1, type: 'boolean', required: true},
		{name: 'plateletDonation', title:'Tranposit', list: 1, type: 'boolean', required: true},
		{name: 'startDate', end: 'endDate', title:'Başlanğıç/Bitiş Tarihi', list: 1, type: 'date', required: true},
	]
};

let registeredUsers = {
	_id: 'registeredUsers',
	title: 'Kullanıcılar',
	sort: 'name',
	listOnly: true,
	fields: [
		{name: '_id', title:'Sicil', list: 1, type: 'text'},
		{name: 'name', title:'İsim', list: 1, type: 'text'}
	]
};

let resources = {
	posts,
	stories,
	surveys,
	events,
	news,
	notifications,
	businessCategories,
	campaings,
	banners,
	bloodNeeds,
	registeredUsers
};

module.exports = {
	name: 'reports',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'reports',

	actions: {
		async report (ctx){
			let resource = resources[ctx.params.resource];
			let data = await this.broker.call(ctx.params.resource + '.find', 
				{
					fields: resource.fields.map(x => x.name)
				});

			data.forEach(d => {
				let result = {};
				resources.fields.forEach(({name, type, end, title}) => {
					if(type == 'date'){
						if(!end) result[title] = dayjs(d[name]);
						if(end) result[title] = dayjs(d[name]) + ' - ' + dayjs(d[end]);
					}
					else if(type == 'text') result[title] = d[name];
					else if(type == 'number') result[title] = d[name];
				});
			});

			let ws = xlxs.utils.json_to_sheet(data, {
				headers: data.length > 0 ? Object.getOwnPropertyNames(data[0]) : []
			});

			ctx.meta.$responseType = 'text/csv';
			return xlxs.stream.to_csv(ws, {
				FS: ';'
			});
		},

		async survey (ctx){
			
			let survey = await this.broker.call('surveys.get', {id: ctx.params.id});
			let answers = await this.broker.call('surveyAnswers.find', {
				query: {survey_id: survey._id }
			});
			let registeredUsers = await this.broker.call('registeredUsers.find', {
				query: {_id: {$in: answers.map(a => a.user_id)}}
			});

			let rows = [];
			answers.forEach(answer => {
				let user = registeredUsers.find(a => a._id === answer.user_id);

				let row = {
					'Anket': survey.name,
					'Person ID': user ? user._id : 'bulunamadı',
					'Kullanıcı': user ? user.name : 'bulunamadı',
					'Zaman': dayjs(answer.created).add(3, "hour").format('DD-MM-YYYY HH:mm:ss:SSS'),
				};
				
				answer.questions.forEach(q => {
					row[q.name] = '';
					if(q.type === 'F'){
						row[q.name] = q.other;
					}
					else if(q.choices){
						let selects = q.choices.filter(c => c.is_selected);
						row[q.name] = selects.map(x => x.name + (x.is_other ? `'${x.other}'` : '')).join(', ');
					}
				});
				rows.push(row);
			});
			
			let ws = xlxs.utils.json_to_sheet(rows, {
				header: rows.length > 0 ? Object.getOwnPropertyNames(rows[0]) : []
			});

			ctx.meta.$responseType = 'text/csv';
			return xlxs.stream.to_csv(ws, {
				FS: ';'
			});
		},

		async event (ctx){
			let event = await ctx.call('events.get', {id: ctx.params.id})
			let joins =  event.joins || []
			
			let registeredUsers = await ctx.call('registeredUsers.find', {
				query: {
					_id: {$in: joins.map(x => x.id)}
				},
				fields: ["_id", "name"]
			})

			registeredUsers = registeredUsers.map(x => {
				let join = event.joins.find(j => j.id === x._id)
				return {
				"Person ID": x._id,
				"İsim": x.name,
				"Tarih": join ? dayjs(join.date).add(3, "hour").format('DD/MM/YYYY HH:mm:ss:SSS'): '-'
				}
			})


			let ws = xlxs.utils.json_to_sheet(registeredUsers);

			ctx.meta.$responseType = 'text/csv';
			return xlxs.stream.to_csv(ws, {
				FS: ';'
			});

		},


		async logins (ctx){
			let logins = await ctx.call('login.find', {
				fields: ["userId", "name", "email", "date", "role", 'type'],
				sort: "userId,date"
			})

			logins = logins.map(l => ({
				"Person Id": l.userId,
				// "İsim": l.name,
				// "Eposta": l.email,
				"Zaman": dayjs(l.date).add(3, 'hour').format('DD-MM-YYYY HH:mm:ss:ms'),
				"Rol": l.role,
				'Tip': l.type || 'Giriş'
			}))

			let ws = xlxs.utils.json_to_sheet(logins);

			ctx.meta.$responseType = 'text/csv';
			return xlxs.stream.to_csv(ws, {
				FS: ';'
			});

		},

		async coordinates (ctx){
			let items = await ctx.call('coordinates.find', {
				
			})

			items = items.map(i => ({
				"İl": i.city,
				"Sağlık Tesisi Adı": i.name,
				"Adresi": i.address,
				"İlçe": i.district,
				"Semt": i.locality,
				"Telefonu": i.phone,
				"Sağlık Tesisi(Kurum) Kodu": i._id,
				"Tip": i.type
			}))

			let ws = xlxs.utils.json_to_sheet(items);

			ctx.meta.$responseType = 'text/csv';
			return xlxs.stream.to_csv(ws, {
				FS: ';'
			});

		}
	}
};