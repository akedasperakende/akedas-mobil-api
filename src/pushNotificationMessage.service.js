'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let nanoid = require('nanoid');

let icon =  {
	'_id': 'notification.png',
	'url': process.env.CDN + '/general/notification.png',
	'thumb': process.env.CDN + '/general/notification.png',
	'type': 'image',
	'mimeType': 'image/png'
}

module.exports = {
	name: 'pushNotificationMessage',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'pushNotificationMessage',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			title: {type: 'string'},
			content: 'string',
			language:  {type: 'string'},
			
		}
	},
	hooks: {
		before: {
			create: [ctx => {
				ctx.params._id = nanoid(25);
			}]
		}
	}
};