'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');
module.exports = {
	name: 'requestAndComplaintSubCategories',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'requestAndComplaintSubCategories',
	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			name: { type: 'string' },
			active: 'string',
			categoryId: { type: 'string', optional: true },
			createdAt: 'date',
			updatedAt: 'date',

		}

	},


	hooks: {
		before: {

			create: [async ctx => {
				let x = ctx.params
				x._id = nanoid(25);
				x.createdAt = dayjs().toDate();
				x.updatedAt = dayjs().toDate();
				
				if (x.active == true) x.active = "1"
				else x.active = "0"

			}],


			update: [async ctx => {
				let x = ctx.params
				x.updatedAt = dayjs().toDate();
				x.updatedBy = ctx.meta.user._id
				
				if (x.active == true) x.active = "1"
				else x.active = "0"
				}],

		}, after: {
			get: [
				(ctx, result) => {

					if (result.active == "1") result.active = true
					else result.active = false
					return result;
				}
			],
		}
	}

};
