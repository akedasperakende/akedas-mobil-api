"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
const dayjs = require("dayjs");
const soapRequest = require('easy-soap-request');
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "debit",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "alacak",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let debittable = await this.broker.call("debit.count");
      const result = await request.query("select * from alacak");
      let record; 
      if (debittable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let debit = await this.broker.call("debit.count");
        if (debit == 0) {
          record = await result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.SOYAD =r.SOYAD== null ? null: String(r.SOYAD)),
            (r.AD= r.AD== null ? null: String(r.AD)),
            (r.SOZLESME_HESAP_NO = r.SOZLESME_HESAP_NO== null ? null:String(r.SOZLESME_HESAP_NO)),
            (r.FATURA_NO = r.FATURA_NO== null ? null:String(r.FATURA_NO)),
            (r.FATURA_TARIHI =r.FATURA_TARIHI == null ? null:dayjs(r.FATURA_TARIHI).toDate()),
            (r.TUTAR =r.TUTAR == null ? null:Number(r.TUTAR)),
            (r.SON_ODEME =r.SON_ODEME == null ? null:dayjs(r.SON_ODEME).toDate()),
            (r.FATURA_TIPI =r.FATURA_TIPI == null ? null:String(r.FATURA_TIPI)),
            (r.TAKSIT_SIRA =r.TAKSIT_SIRA == null ? null:String(r.TAKSIT_SIRA)),
            (r.PARA_BIRIMI =r.PARA_BIRIMI == null ? null:String(r.PARA_BIRIMI));
            return r;
          });
          await this.adapter.insertMany(record);
          return true;
        }
      }

      if (debittable == 0 && result.recordset.length > 0) {
        record = await result.recordset.map((r) => {
          (r._id = nanoid(25)),
          (r.SOYAD =r.SOYAD== null ? null: String(r.SOYAD)),
          (r.AD= r.AD== null ? null: String(r.AD)),
          (r.SOZLESME_HESAP_NO = r.SOZLESME_HESAP_NO== null ? null:String(r.SOZLESME_HESAP_NO)),
          (r.FATURA_NO = r.FATURA_NO== null ? null:String(r.FATURA_NO)),
          (r.FATURA_TARIHI =r.FATURA_TARIHI == null ? null:dayjs(r.FATURA_TARIHI).toDate()),
          (r.TUTAR =r.TUTAR == null ? null:Number(r.TUTAR)),
          (r.SON_ODEME =r.SON_ODEME == null ? null:dayjs(r.SON_ODEME).toDate()),
          (r.FATURA_TIPI =r.FATURA_TIPI == null ? null:String(r.FATURA_TIPI)),
          (r.TAKSIT_SIRA =r.TAKSIT_SIRA == null ? null:String(r.TAKSIT_SIRA)),
          (r.PARA_BIRIMI =r.PARA_BIRIMI == null ? null:String(r.PARA_BIRIMI));
          return r;
        });
        await this.adapter.insertMany(record);
        return true;
      }
    },

    async totalDebit(ctx){
      let debitCount;
      let json;
      let array=[];
      await Promise.all(ctx.params.SOZLESME_HESAP_NO.map(async (x)=>{
        let total = 0;
      debitCount=await this.broker.call('debit.debitFind',{
          SOZLESME_HESAP_NO: [x],
      });
      if(debitCount){
      for (let i = 0; i < debitCount.length; i++) {
        total += debitCount[i].TUTAR;
      }
      json={
        _id:x,
        TUTAR:total
      }
      array.push(json);
    }
      }))
      return array;
    },
    async debitFind(ctx){
      let result=[];
      const url = 'http://sapppp00.akedas.local:50000/XISOAPAdapter/MessageServlet?senderService=Logical_BankEcd&interface=OpenItemList_Out&interfaceNamespace=http://alfayazilim.com/xi/BANKINT';
      const sampleHeaders = {
        'user-agent': 'sampleTest',
        'Content-Type': 'text/xml; charset=utf-8',
        'Authorization': 'Basic UE1CTF8zTDFGNDVGOkxtOTN1NHRyMDMhKg=='
      };
      await Promise.all(ctx.params.SOZLESME_HESAP_NO.map(async (x)=>{
        const xml =  '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ban="http://alfayazilim.com/xi/BANKINT">'+
        '<soapenv:Header/>'+
        '<soapenv:Body>'+
          ' <ban:OpenItemListQueryMessage>'+
         '     <BankCode>MBL</BankCode>'+
         '<PaymentChannel>M</PaymentChannel>'+
              '<ContractAccountNumber>'+x+'</ContractAccountNumber>'+
          ' </ban:OpenItemListQueryMessage>'+
       ' </soapenv:Body>'+
       '</soapenv:Envelope>';
  
        const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xml, timeout: 600000 });
        const { headers, body, statusCode } = response;

        const regex_message = /<Message>[\s\S]*?<\/Message>/g;
        const regex_sozlesme_hesap_no = /<ContractAccountNumber>[\s\S]*?<\/ContractAccountNumber>/g;
        const regex_ad=/<Name>[\s\S]*?<\/Name>/g;
        const regex_soyad=/<Surname>[\s\S]*?<\/Surname>/g;
        const regex_fatura_no=/<InvoiceNumber>[\s\S]*?<\/InvoiceNumber>/g;
        const regex_fatura_tarihi=/<InvoiceDate>[\s\S]*?<\/InvoiceDate>/g;
        const regex_fatura_tipi=/<InvoiceType>[\s\S]*?<\/InvoiceType>/g;
        const regex_tutar=/<InvoiceAmount>[\s\S]*?<\/InvoiceAmount>/g;
        const regex_para_birimi=/<Currency>[\s\S]*?<\/Currency>/g;
        const regex_odeme_tarihi=/<InvoiceDueDate>[\s\S]*?<\/InvoiceDueDate>/g;
        const regex_taksit_sira=/<OrderOfInstallment>[\s\S]*?<\/OrderOfInstallment>/g;
        
        let message = body.match(regex_message);
        let sozlesme_hesap_no=body.match(regex_sozlesme_hesap_no);
        let ad=body.match(regex_ad);
        let soyad=body.match(regex_soyad);
        let fatura_no=body.match(regex_fatura_no);
        let fatura_tarihi=body.match(regex_fatura_tarihi);
        let fatura_tipi=body.match(regex_fatura_tipi);
        let tutar=body.match(regex_tutar);
        let para_birimi=body.match(regex_para_birimi);
        let odeme_tarihi=body.match(regex_odeme_tarihi);
        let taksit_sira=body.match(regex_taksit_sira);
        
        if(message) {
          message = message[0].replace('<Message>','').replace('</Message>','');
         }
         let debits=[]
         let debitDetail;
         if(sozlesme_hesap_no){
         let count=sozlesme_hesap_no.length

       for(let i=0;i<count;i++){ 
           let bill_number=String(fatura_no[i].replace('<InvoiceNumber>','').replace('</InvoiceNumber>',''));
           bill_number=bill_number.substring(1);
          let installment_order=taksit_sira[i].replace('<OrderOfInstallment>','').replace('</OrderOfInstallment>','');
          let amount=tutar[i].replace('<InvoiceAmount>','').replace('</InvoiceAmount>','');
          debitDetail={
              _id:bill_number.startsWith('270')  == true ? bill_number+'00'+String(installment_order)+tutar[i].replace('<InvoiceAmount>','').replace('</InvoiceAmount>','') : bill_number,
              SOZLESME_HESAP_NO:sozlesme_hesap_no[i].replace('<ContractAccountNumber>','').replace('</ContractAccountNumber>',''),
              AD:ad[i].replace('<Name>','').replace('</Name>',''),
              SOYAD:soyad[i].replace('<Surname>','').replace('</Surname>',''),
              FATURA_NO:bill_number,
              FATURA_TARIHI:fatura_tarihi[i].replace('<InvoiceDate>','').replace('</InvoiceDate>',''),
              FATURA_TIPI:fatura_tipi[i].replace('<InvoiceType>','').replace('</InvoiceType>',''),
              TUTAR:Number(amount),
              PARA_BIRIMI:para_birimi[i].replace('<Currency>','').replace('</Currency>',''),
              SON_ODEME:odeme_tarihi[i].replace('<InvoiceDueDate>','').replace('</InvoiceDueDate>',''),
              TAKSIT_SIRA:Number(installment_order),
            }
        debits.push(debitDetail)
        }
      }

      if(debits.length>0){
        debits.map((x)=>{
          result.push(x) 
        })       
      }
      }))
      return result;
    },
  },
};