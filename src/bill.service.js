"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
const dayjs = require("dayjs");
const nodemailer = require("nodemailer");
const soapRequest = require('easy-soap-request');

var config = {
  user: process.env.USER,

  password: process.env.PASSWORD,

  server: process.env.SERVER,

  database: process.env.DATABASE,

  dialect: process.env.DIALECT,

  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },

  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};

module.exports = {
  name: "bill",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "fatura",
  settings: {
    maxPageSize: 10000000,
  },
  actions: {
    async insertBills() {
      let dongu = await this.broker.call("bill.countBillMssql");
      let pool = await sql.connect(config);
      const request = pool.request();
      let billtable = await this.broker.call("bill.count");
      if (billtable > 0 && dongu > 0) {
        await this.adapter.removeMany({});
        let billtable = await this.broker.call("bill.count");
        let record; 
        if (billtable == 0) {
          for (var i = 0; i < dongu; i++) {
            const partial_result = await request.query(
              "select * from fatura_2 ORDER BY SOZLESME_HESAP_NO OFFSET " +
                i * 1000000 +
                " ROWS FETCH NEXT 1000000 ROWS ONLY;"
            );
            record = await partial_result.recordset.map((r) => {
              (r._id = nanoid(25)),
                (r.YAZDIRMA_BELGESI =r.YAZDIRMA_BELGESI== null ? null: String(r.YAZDIRMA_BELGESI)),
                (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
                (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
                (r.SOZLESME_NO = r.SOZLESME_NO== null ? null:String(r.SOZLESME_NO)),
                (r.FATURA_TARIHI = r.FATURA_TARIHI== null ? null:dayjs(r.FATURA_TARIHI).toDate()),
                (r.YIL = r.YIL== null ? null:String(r.YIL)),
                (r.AY = r.AY== null ? null:String(r.AY)),
                (r.FATURA_THK_BAS =r.FATURA_THK_BAS == null ? null:dayjs(r.FATURA_THK_BAS).toDate()),
                (r.FATURA_THK_SON =r.FATURA_THK_SON == null ? null:dayjs(r.FATURA_THK_SON).toDate()),
                (r.TUKETIM =r.TUKETIM == null ? null:Number(r.TUKETIM)),
                (r.FATURA_SERI =r.FATURA_SERI == null ? null:String(r.FATURA_SERI)),
                (r.EUUID =r.EUUID == null ? null:String(r.EUUID)),
                (r.ODEME_DURUM =r.ODEME_DURUM == null ? null:String(r.ODEME_DURUM)),
                (r.ODEME_TUTAR =r.ODEME_TUTAR == null ? null:Number(r.ODEME_TUTAR)),
                (r.ORTALAMA_TUKETIM=r.ORTALAMA_TUKETIM == null ? null:Number(r.ORTALAMA_TUKETIM)),
                (r.TAKSIT_BELGE_NO =r.TAKSIT_BELGE_NO == null ? null:String(r.TAKSIT_BELGE_NO)),
                (r.BIRIM_FIYAT =r.BIRIM_FIYAT == null ? null:Number(r.BIRIM_FIYAT)),
                (r.BIRIM_FIYAT_KD1 =r.BIRIM_FIYAT_KD1 == null ? null:Number(r.BIRIM_FIYAT_KD1)),
                (r.BIRIM_FIYAT_KD2 =r.BIRIM_FIYAT_KD2 == null ? null:Number(r.BIRIM_FIYAT_KD2)),
                (r.SON_ODEME_TARIHI =r.SON_ODEME_TARIHI == null ? null:dayjs(r.SON_ODEME_TARIHI).toDate());
              return r;
            });
            await this.adapter.insertMany(record);
          }
          return true;
        }
      }

      if (billtable == 0 && dongu > 0) {
        let record;
        for (var i = 0; i < dongu; i++) {
          const partial_result = await request.query(
            "select * from fatura_2 ORDER BY SOZLESME_HESAP_NO OFFSET " +
              i * 1000000 +
              " ROWS FETCH NEXT 1000000 ROWS ONLY;"
          );
          record = await partial_result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.YAZDIRMA_BELGESI =r.YAZDIRMA_BELGESI== null ? null: String(r.YAZDIRMA_BELGESI)),
            (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
            (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
            (r.SOZLESME_NO = r.SOZLESME_NO== null ? null:String(r.SOZLESME_NO)),
            (r.FATURA_TARIHI = r.FATURA_TARIHI== null ? null:dayjs(r.FATURA_TARIHI).toDate()),
            (r.YIL = r.YIL== null ? null:String(r.YIL)),
            (r.AY = r.AY== null ? null:String(r.AY)),
            (r.FATURA_THK_BAS =r.FATURA_THK_BAS == null ? null:dayjs(r.FATURA_THK_BAS).toDate()),
            (r.FATURA_THK_SON =r.FATURA_THK_SON == null ? null:dayjs(r.FATURA_THK_SON).toDate()),
            (r.TUKETIM =r.TUKETIM == null ? null:Number(r.TUKETIM)),
            (r.FATURA_SERI =r.FATURA_SERI == null ? null:String(r.FATURA_SERI)),
            (r.EUUID =r.EUUID == null ? null:String(r.EUUID)),
            (r.ODEME_DURUM =r.ODEME_DURUM == null ? null:String(r.ODEME_DURUM)),
            (r.ODEME_TUTAR =r.ODEME_TUTAR == null ? null:Number(r.ODEME_TUTAR)),
            (r.ORTALAMA_TUKETIM=r.ORTALAMA_TUKETIM == null ? null:Number(r.ORTALAMA_TUKETIM)),
            (r.TAKSIT_BELGE_NO =r.TAKSIT_BELGE_NO == null ? null:String(r.TAKSIT_BELGE_NO)),
            (r.BIRIM_FIYAT =r.BIRIM_FIYAT == null ? null:Number(r.BIRIM_FIYAT)),
            (r.BIRIM_FIYAT_KD1 =r.BIRIM_FIYAT_KD1 == null ? null:Number(r.BIRIM_FIYAT_KD1)),
            (r.BIRIM_FIYAT_KD2 =r.BIRIM_FIYAT_KD2 == null ? null:Number(r.BIRIM_FIYAT_KD2)),
            (r.SON_ODEME_TARIHI =r.SON_ODEME_TARIHI == null ? null:dayjs(r.SON_ODEME_TARIHI).toDate());
            return r;
          });
          await this.adapter.insertMany(record);
        }
        return true;
      }
    },
    async changeBill(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();

      let today="fatura_"+ctx.params.today
      let yesterday="fatura_"+ctx.params.yesterday
      let month=ctx.params.month
      const partial_result = await request.query(
        " select * from "+today+"  where [AY]!= "+month+" except  select * from "+yesterday+"  where [AY]!= "+month);
      let  record = await partial_result.recordset.map((r) => {
        (r._id = nanoid(25)),
        (r.YAZDIRMA_BELGESI =r.YAZDIRMA_BELGESI== null ? null: String(r.YAZDIRMA_BELGESI)),
        (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
        (r.TUTAR = r.TUTAR== null ? null:Number(r.TUTAR)),
        (r.SOZLESME_NO = r.SOZLESME_NO== null ? null:String(r.SOZLESME_NO)),
        (r.FATURA_TARIHI = r.FATURA_TARIHI== null ? null:dayjs(r.FATURA_TARIHI).toDate()),
        (r.YIL = r.YIL== null ? null:String(r.YIL)),
        (r.AY = r.AY== null ? null:String(r.AY)),
        (r.FATURA_THK_BAS =r.FATURA_THK_BAS == null ? null:dayjs(r.FATURA_THK_BAS).toDate()),
        (r.FATURA_THK_SON =r.FATURA_THK_SON == null ? null:dayjs(r.FATURA_THK_SON).toDate()),
        (r.TUKETIM =r.TUKETIM == null ? null:Number(r.TUKETIM)),
        (r.FATURA_SERI =r.FATURA_SERI == null ? null:String(r.FATURA_SERI)),
        (r.EUUID =r.EUUID == null ? null:String(r.EUUID)),
        (r.ODEME_DURUM =r.ODEME_DURUM == null ? null:String(r.ODEME_DURUM)),
        (r.ODEME_TUTAR =r.ODEME_TUTAR == null ? null:Number(r.ODEME_TUTAR)),
        (r.ORTALAMA_TUKETIM=r.ORTALAMA_TUKETIM == null ? null:Number(r.ORTALAMA_TUKETIM)),
        (r.TAKSIT_BELGE_NO =r.TAKSIT_BELGE_NO == null ? null:String(r.TAKSIT_BELGE_NO)),
        (r.BIRIM_FIYAT =r.BIRIM_FIYAT == null ? null:Number(r.BIRIM_FIYAT)),
        (r.BIRIM_FIYAT_KD1 =r.BIRIM_FIYAT_KD1 == null ? null:Number(r.BIRIM_FIYAT_KD1)),
        (r.BIRIM_FIYAT_KD2 =r.BIRIM_FIYAT_KD2 == null ? null:Number(r.BIRIM_FIYAT_KD2)),
        (r.SON_ODEME_TARIHI =r.SON_ODEME_TARIHI == null ? null:dayjs(r.SON_ODEME_TARIHI).toDate());
          return r;
        });

      let dataInsert=await this.adapter.insertMany(record);
      return dataInsert.length
    },
    async deleteBill(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();

      let today="fatura_"+ctx.params.today
      let yesterday="fatura_"+ctx.params.yesterday
      let month=ctx.params.month

      const partial_result = await request.query(
       " select * from "+yesterday+"  where [AY]!= "+month+" except  select * from "+today+"  where [AY]!= "+month);

        let bills =[]
       await Promise.all(
         await  partial_result.recordset.map(async (x) =>{
          let a = await this.broker.call("bill.find", {
              query: {
                SOZLESME_HESAP_NO: x.SOZLESME_HESAP_NO,
                YAZDIRMA_BELGESI: x.YAZDIRMA_BELGESI
              }
            });
            a.map((t) => {
               bills.push(t._id)
            });
           
            return bills 
            
           }))
           let rem = await this.adapter.removeMany({
            _id: {
               $in: bills.map((x) => x)
          }
          });
      return rem
    },
    async deletingOldDataBill(ctx) {
      
    let month=(ctx.params.month).toString();

      let bills =await this.broker.call("bill.find",{query:{AY: month }})
      let rem = await this.adapter.removeMany({
        _id: {
           $in: bills.map((x) => x._id)
      }
      });

      return rem
    },
    async getmove(ctx) {
      let pool = await sql.connect(config);

      const request = pool.request();
      const partial_result = await request.query(
        "select top (1000000) * from fatura"
      );
      await this.adapter.insertMany(partial_result.recordset);
    },
    async deleteBills() {
      await this.adapter.removeMany({});
      return true;
    },
    async countBillMssql() {
      let pool = await sql.connect(config);
      const request = pool.request();
      const result = await request.query(
        "select count(*) as boyut from fatura_2;"
      );
      let dongu = Math.ceil(result.recordset[0].boyut / 1000000);
      return dongu;
    },
    async subscribersearch(ctx) {
      let subscriber = await this.broker.call("subscriber.find", {
        query: {
          $or: [
            { TC_NUMARASI: ctx.params.tcOrVkn },
            { VERGI_NO: ctx.params.tcOrVkn },
          ],
        },
      });
      return subscriber.map((x) => ({
        SOZLESME_HESAP_NO: x.SOZLESME_HESAP_NO,
        ABONE_SINIFI: x.ABONE_SINIFI,
      }));
    },
    async billFind(ctx) {
      let subscriber;
      let index=0;
      let array=[];
      let totalInst=0;
      let data;
      let debitTotal;
      if (ctx.meta.user.vergiNo) {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }
      data = await this.broker.call("debit.debitFind", {
        SOZLESME_HESAP_NO: subscriber.map((x) => x.SOZLESME_HESAP_NO)
      });
      let Bills = await this.broker.call("bill.find", {
        query: {
          SOZLESME_HESAP_NO: {
            $in: subscriber.map((x) => x.SOZLESME_HESAP_NO),
          },
        },
        fields: [
          "_id",
          "SOZLESME_HESAP_NO",
          "FATURA_THK_SON",
          "TAKSIT_BELGE_NO",
          "TUTAR",
          "ODEME_DURUM",
          "ODEME_TUTAR",
          "FATURA_SERI",
        ],
        sort: '-FATURA_THK_SON'
      });

      let bills=[];
      if(data){
        let bill;
        await Promise.all(Bills.map((x)=>{
        [bill]=data.filter(z=>z.FATURA_NO == x.FATURA_SERI ||  z.FATURA_NO == x.TAKSIT_BELGE_NO)
            if(bill){
            bills.push(x);
            }  
        }))

        await Promise.all(data.map((x)=>{
          let livingDebt
          [livingDebt]=bills.filter(z=>z.FATURA_SERI == x.FATURA_NO ||  z.TAKSIT_BELGE_NO == x.FATURA_NO)
              if(!livingDebt){
                let unpaidDebt ={
                  "_id": x._id,
                  "SOZLESME_HESAP_NO":x.SOZLESME_HESAP_NO,
                  "TUTAR": x.TUTAR,
                  "ODEME_TUTAR": 0,
                  "INSTALLMENTBILLS": [],
                  "PAYMENTSTATUS": "NOTPAID",
                  "BILLSTATUS": "PAYONCE",
                  "ISSELECT": true,
                  "SON_ODEME": x.SON_ODEME
                }
              bills.push(unpaidDebt);
              }  
          }))
        await Promise.all(
          bills.map(async (x, i) => {
            bills[i].TUTAR = x.TUTAR - x.ODEME_TUTAR;
            if (x.ODEME_DURUM == "Ödenmedi") {
              bills[i].INSTALLMENTBILLS = [];
              bills[i].PAYMENTSTATUS = "NOTPAID";
              bills[i].BILLSTATUS = "PAYONCE";
            }
            if (x.ODEME_DURUM == "Kısmi Ödeme") {
              bills[i].INSTALLMENTBILLS = [];
              bills[i].PAYMENTSTATUS = "NOTPAID";
              bills[i].BILLSTATUS = "PARTIALPAYMENT";
            }
            if (x.ODEME_DURUM == "Ödendi") {
              bills[i].INSTALLMENTBILLS = [];
              bills[i].PAYMENTSTATUS = "PAID";
              bills[i].BILLSTATUS = "PAYONCE";
            }
            if (x.ODEME_DURUM == "Taksitli") {
              bills[i].BILLSTATUS = "INSTALLMENT";
              let INSTALLMENTBILLS = await this.broker.call(
                "installment.find",
                {
                  query: {
                    BELGE_NO: x.TAKSIT_BELGE_NO,
                    ODEME_DURUM:'Ödenmedi'
                  },
                  fields: [ 
                    "_id",
                    "TAKSIT_SIRA",
                    "ODEME_DURUM",
                    "SON_ODEME",
                    "BELGE_NO",
                    "TUTAR",
                  ],
                  sort: 'TAKSIT_SIRA'
                }
              );
              await INSTALLMENTBILLS.map(async (y,j) => {
                y.ORDER=Number(y.TAKSIT_SIRA);
                y.ISSELECT=false;
                if (y.ODEME_DURUM == "Ödenmedi") {
                  y.PAYMENTSTATUS = "NOTPAID";
                  bills[i].PAYMENTSTATUS = "NOTPAID";
                  let insBill=data.filter(z =>z.FATURA_NO == y.BELGE_NO)
                  let instBill=insBill.filter(z =>z.TAKSIT_SIRA == y.TAKSIT_SIRA)
                  if(instBill){
                    totalInst += y.TUTAR;
                    array.push(y)
                  }
                  bills[i].TUTAR=totalInst;
                } else {
                  y.ISSELECT=true;
                  y.PAYMENTSTATUS = "PAID";
                  bills[i].PAYMENTSTATUS = "PAID";
                }
              });
              if(array.length>0){
                bills[i].INSTALLMENTBILLS=array
              }
            }
          })
        );
        let findDebt = false;
            if (data.length > 0) {
              await data.map(async (y) => {
                await bills.map((z) => {
                  if (z.FATURA_SERI === y.FATURA_NO || z.TAKSIT_BELGE_NO === y.FATURA_NO) {
                    findDebt = true;
                  }
                });
                if (findDebt == false) {
                  y.UNPAIDDEPT = y.TUTAR;
                  y.INSTALLMENTBILLS = [];
                  y.PAYMENTSTATUS = "NOTPAID";
                  y.BILLSTATUS = "PAYONCE";
                  bills.push(y);
                }
              });
            }
        let total=0;
          //aynı id ait faturalar teke indirildi

          var lookup = {};
          var items = bills;
          var distBills = [];
          for (var item, i = 0; item = items[i++];) {
            var _id = item._id;
            if (!(_id in lookup)) {
              lookup[_id] = 1;
              distBills.push(item);
            }
          }

        
        await Promise.all(distBills.map(async(x,i) => {
          delete x.TAKSIT_BELGE_NO;
          delete x.ODEME_DURUM;
          delete x.FATURA_SERI;
          delete x.FATURA_NO;
          distBills[i].ISSELECT=false;
          if (x.FATURA_THK_SON) {
            x.SON_ODEME = x.FATURA_THK_SON;
            delete x.FATURA_THK_SON;
          }
          if(ctx.params.DEBITLIST.length>0){
            await Promise.all(ctx.params.DEBITLIST.map(async (y)=>{ 
              if(y._id==x._id){               
                if(x.BILLSTATUS == "INSTALLMENT"){
                  if(y.SUBIDS.length>0){
                    await Promise.all(array.map(async (z,j)=>{
                      await Promise.all(y.SUBIDS.map(async (y)=>{
                        if(z._id==y._id ){
                          if(j==index){
                          index++;
                          distBills[i].ISSELECT=true;
                          z.ISSELECT=true; 
                          total+=z.TUTAR;                       
                          }
                          else{
                            distBills[i].ISSELECT=false;
                            z.ISSELECT=false; 
                          }                      
                        }
                      }))
                    }))
                  }
                  else{             
                  array.map((z)=>{
                      distBills[i].ISSELECT=true;
                      z.ISSELECT=true;
                      total+=z.TUTAR;  
                  })                  
                  }
                }
                else{
                  distBills[i].ISSELECT=true;
                  total+=x.TUTAR;
                }
              }
            }))
          }
        }));
        let myBills=[]


        await Promise.all(
          subscriber.map(async (x,i) => {
            let faturalar=distBills.filter(y => y.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO) 
              if(faturalar.length>0){
                let  json={
                  SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO,
                  ABONE_GRUBU:x.KULLANIM_TIPI,
                  FATURALAR:faturalar
                }
               myBills.push(json)
              }
            
            })
        );
        debitTotal={
          Bills:myBills,
          total:total
        }
      }
      return debitTotal;
    },
    async bills(ctx) {
      let subscriber;
      let data;
      let state=false;
      if (ctx.meta.user.vergiNo) {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }
      data = await this.broker.call("debit.debitFind", {
        SOZLESME_HESAP_NO: subscriber.map((x) => x.SOZLESME_HESAP_NO)
      });
      let total = 0;
      let installmentBills;
      let monthNames = ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs","Haziran","Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım","Aralık"];
      let bills = await await this.broker.call("bill.find", {
        query: {
          SOZLESME_HESAP_NO: {
            $in: subscriber.map((x) => x.SOZLESME_HESAP_NO),
          },
        },
        fields: [
          "_id",
          "SOZLESME_HESAP_NO",
          "FATURA_SERI",
          "FATURA_THK_SON",
          "TAKSIT_BELGE_NO",
          "TUTAR",
          "AY",
          "ODEME_DURUM",
          "ODEME_TUTAR",
        ],
        sort: '-FATURA_THK_SON'
      });    
      await Promise.all(
        bills.map(async (x, i) => {
          bills[i].DONEM=monthNames[x.AY-1];
          bills[i].BILLSTATUS = "PAYONCE";
          bills[i].UNPAIDDEPT=0;
          if (x.ODEME_DURUM == "Ödenmedi") {
            if(data){
            let find=data.filter(z=>z.FATURA_NO==x.FATURA_SERI);
            if(find.length>0){
              bills[i].PAYMENTSTATUS = "NOTPAID";
            }
            else{
              bills[i].PAYMENTSTATUS = "PAID";       
            }              
            }
            else{
              bills[i].PAYMENTSTATUS = "PAID";       
            }
          }
          if (x.ODEME_DURUM == "Kısmi Ödeme") {
            if(data){
            let find=data.filter(z=>z.FATURA_NO==x.FATURA_SERI);
            if(find.length>0){
              bills[i].BILLSTATUS="PARTIALPAYMENT"
              bills[i].UNPAIDDEPT = x.TUTAR - x.ODEME_TUTAR;
              bills[i].PAYMENTSTATUS = "NOTPAID";
            }
            else{
              bills[i].PAYMENTSTATUS = "PAID";
            }              
            }
            else{
              bills[i].PAYMENTSTATUS = "PAID";       
            }
          }
          if (x.ODEME_DURUM == "Ödendi") {
            bills[i].PAYMENTSTATUS = "PAID";
          }
          if (x.ODEME_DURUM == "Taksitli") {
            installmentBills = await this.broker.call("installment.find", {
              query: {
                BELGE_NO: x.TAKSIT_BELGE_NO,
              },
              fields: [
                "_id",
                "TAKSIT_SIRA",
                "ODEME_DURUM",
                "SON_ODEME",
                "TUTAR",
                "BELGE_NO"
              ],
            });
            installmentBills.map((y) => {
              if(data){
                let find=data.filter(z=>z.FATURA_NO==y.BELGE_NO);
                let instBill=find.find(z =>z.TAKSIT_SIRA == y.TAKSIT_SIRA)
                if(instBill){
                  if(y.ODEME_DURUM=='Ödenmedi'){
                  total += y.TUTAR;
                  bills[i].UNPAIDDEPT = total;
                  bills[i].PAYMENTSTATUS = "NOTPAID";
                  bills[i].BILLSTATUS = "INSTALLMENT";
                  state=true;                    
                  }
                } 
                if(state==false) {
                  bills[i].PAYMENTSTATUS = "PAID";
                }
              }else {
                bills[i].PAYMENTSTATUS = "PAID";
              }
              });
          }
        })
      );
      bills.map((x) => {
        delete x.TAKSIT_BELGE_NO;
        delete x.ODEME_TUTAR;
        delete x.ODEME_DURUM;
        delete x.FATURA_SERI;
        delete x.FATURA_NO;
        delete x.AY;
        if (x.FATURA_THK_SON) {
          x.SON_ODEME = x.FATURA_THK_SON;
          delete x.FATURA_THK_SON;
        }
      });
      let myBills=[]
      await Promise.all(

        subscriber.map(async (x,i) => {
          let faturalar=bills.filter(y => y.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO)
          if(faturalar.length>0){
            let  json={
              SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO,
              ABONE_GRUBU:x.KULLANIM_TIPI,
              FATURALAR:faturalar
            }
           myBills.push(json)
          }
        })
      );
      
      return myBills;
    },
    async billDetail(ctx){
      let bill;
      let invoice;
      let installmentBills;
      [bill] = await await this.broker.call("bill.find", {
        query: {
          _id:ctx.params._id
        },
        fields: [
          "_id",
          "SOZLESME_HESAP_NO",
          "EUUID",
          "FATURA_TARIHI",
          "SON_ODEME_TARIHI",
          "TAKSIT_BELGE_NO",
          "TUTAR",
          "ODEME_DURUM",
          "ODEME_TUTAR",
          "ORTALAMA_TUKETIM",
        ],
      });
      let subscriber;
      [subscriber]=await this.broker.call("subscriber.find",{
        query:{
          SOZLESME_HESAP_NO:bill.SOZLESME_HESAP_NO
        }
      })
     let subscriberToken;
      if (ctx.meta.user.vergiNo) {
        subscriberToken = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriberToken = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }

    let findSubs=  await subscriberToken.find(item => {
        return item.SOZLESME_HESAP_NO == bill.SOZLESME_HESAP_NO
      })
    if(!findSubs){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0001", lang: "TR" },
      });
      throw Error(resultMessage.message);
    }


      let data;
      data = await this.broker.call("debit.debitFind", {
        SOZLESME_HESAP_NO: [bill.SOZLESME_HESAP_NO]
      });
      invoice=await await this.broker.call("bill.find", {
        query: {
          SOZLESME_HESAP_NO:bill.SOZLESME_HESAP_NO
        },
        fields: [
          "_id",
          "FATURA_TARIHI",
        ],
        sort:"-FATURA_TARIHI"
      });
      bill.SINIF=subscriber.ABONE_SINIFI;
      if (bill.ODEME_DURUM == "Ödenmedi") {
        bill.UNPAIDDEPT=0;
        bill.MESSAGE="Borcunuz Bulunmaktadır."
        bill.PAYMENTSTATUS = "NOTPAID";
      }
      if (bill.ODEME_DURUM == "Kısmi Ödeme") {
        bill.UNPAIDDEPT = bill.TUTAR - bill.ODEME_TUTAR;
        bill.MESSAGE="Borcunuz Bulunmaktadır."
        bill.PAYMENTSTATUS = "NOTPAID";
      }
      if (bill.ODEME_DURUM == "Ödendi") {
        bill.UNPAIDDEPT=0;
        bill.MESSAGE="Borcunuz Bulunmamaktadır."
        bill.PAYMENTSTATUS = "PAID";
      } 
      if (bill.ODEME_DURUM == "Taksitli") {
        installmentBills = await this.broker.call("installment.find", {
          query: {
            BELGE_NO: bill.TAKSIT_BELGE_NO,
          },
          fields: [
            "_id",
            "TAKSIT_SIRA",
            "ODEME_DURUM",
            "SON_ODEME",
            "TUTAR",
            "BELGE_NO"
          ],
        });
        let total = 0;
        installmentBills.map((y) => {
          if(data){
            let find=data.filter(z=>z.FATURA_NO==y.BELGE_NO);
            let instBill=find.find(z =>z.TAKSIT_SIRA == y.TAKSIT_SIRA)
            if(instBill){
              if(y.ODEME_DURUM=='Ödenmedi'){
              total += y.TUTAR;
              bill.UNPAIDDEPT = total;
              bill.PAYMENTSTATUS = "NOTPAID";
              bill.MESSAGE="Borcunuz Bulunmaktadır."
              }
            } 
          }else {
            bill.MESSAGE="Borcunuz Bulunmamaktadır."
            bill.PAYMENTSTATUS = "PAID";
          }
          });
      }
      bill.DEBITLIST=[
        {
            "_id": bill._id,
            "SUBIDS": []
        }
      ]  
      delete bill.ODEME_TUTAR;
      delete bill.ODEME_DURUM;
      delete bill.TAKSIT_BELGE_NO;
      let json={
        lastInvoiceList:invoice,
        bill:bill
      }
      return json;
    },
    async paidBills(ctx){
      let subscriber;
      let data;
      if (ctx.meta.user.vergiNo) {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }
      let lastInstallmentBill;
      let installmentBills;

      let bills = await this.broker.call("bill.find", {
        query: {
          SOZLESME_HESAP_NO: {
            $in: subscriber.map((x) => x.SOZLESME_HESAP_NO),
          },        
        },
        fields: [
          "_id",
          "SOZLESME_HESAP_NO",
          "FATURA_THK_SON",
          "TAKSIT_BELGE_NO",
          "TUTAR",
          "ODEME_DURUM",
          "ODEME_TUTAR",
          "FATURA_SERI",
        ],
        sort: '-FATURA_THK_SON'
      });
      data = await this.broker.call("debit.debitFind", {
        SOZLESME_HESAP_NO: subscriber.map((x) => x.SOZLESME_HESAP_NO)
      });
      let find;
      if(!data){
        find=bills.filter(z=>z.ODEME_DURUM=='Ödenmedi');
      }
      let paidBills=[];
      await Promise.all(
        bills.map(async (x,i) => {
         let [paidBillInfo] = await this.broker.call("paymentInfo.find",{
          query: {
                FATURA_NO: x.FATURA_SERI
          }});
          x.ODEME_YONTEM=paidBillInfo ?paidBillInfo.ODEME_KANALI:"-";
          x.SON_ODEME=paidBillInfo ?paidBillInfo.ODEME_TARIHI:x.FATURA_THK_SON;
          if(x.ODEME_DURUM=='Ödenmedi'){
            if(find){
            find.map((a)=>{
              if(a.FATURA_SERI==x.FATURA_SERI){
                paidBills.push(x)
              }
            })              
            }
          }
          if (x.ODEME_DURUM == "Ödendi") {
            paidBills.push(x)
          }
          if (x.ODEME_DURUM == "Kısmi Ödeme") {
            bills[i].TUTAR= x.ODEME_TUTAR;
            paidBills.push(x)
          }
          if (x.ODEME_DURUM == "Taksitli") {
            installmentBills = await this.broker.call("installment.find", {
              query: {
                BELGE_NO: x.TAKSIT_BELGE_NO,
              },
              fields: [
                "_id",
                "TAKSIT_SIRA",
                "SOZLESME_HESAP_NO",
                "ODEME_DURUM",
                "SON_ODEME",
                "TUTAR",
              ],
            });
            await installmentBills.map((y) => {
              if (y.ODEME_DURUM == "Ödendi") {
                lastInstallmentBill=y;
                return lastInstallmentBill;
              }
            });
            if(lastInstallmentBill){
              lastInstallmentBill.ODEME_YONTEM="Kredi Kartı";
              paidBills.push(lastInstallmentBill)
            }
          }
        })
      );

      
      paidBills.map((x) => {
        delete x.TAKSIT_BELGE_NO;
        delete x.ODEME_DURUM;
        delete x.ODEME_TUTAR;
        delete x.TAKSIT_SIRA;
        delete x.FATURA_SERI;
        delete x.FATURA_THK_SON;
      });
      let myBills=[]
      await Promise.all(

        subscriber.map(async (x,i) => {

          let faturalar=paidBills.filter(y => y.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO)
        if(faturalar.length> 0) 
        faturalar.sort((a,b) => (new Date(b.SON_ODEME))-(new Date(a.SON_ODEME)))
        let  json={
            SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO,
            ABONE_GRUBU:x.KULLANIM_TIPI,
           FATURALAR:faturalar.length== 0 ? null : faturalar,
          }
         myBills.push(json)

        })
      );
      return myBills;
    },
    async comparisonBills2(ctx){
      let subscriber=ctx.params.SUBSCRIBER.sort(function(a, b) {
        return a.SOZLESME_HESAP_NO - b.SOZLESME_HESAP_NO;
      });

      let monthNames = ["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      let result;
      let array=[];
      let total = [];
      total = await this.adapter.db
        .collection("fatura")
        .aggregate([
          {
            $match: {
              SOZLESME_HESAP_NO: {
                $in: subscriber.map((x) => x.SOZLESME_HESAP_NO),
              },
            },
          },
          {
            $group: {
              _id: {
                FATURA_THK_SON:"$FATURA_THK_SON",
                SOZLESME_HESAP_NO: "$SOZLESME_HESAP_NO",
                AY: "$AY"
              },
              ORTALAMA_TUKETIM: { $sum: "$TUKETIM" },
              TUTAR: { $sum: "$TUTAR" },
            },
          }
        ])
        .toArray();

        
        var flags = [];
        var distinctMonth = [];
        var l = total.length;
        var i;
        for( i=0; i<l; i++) {
        if( flags[total[i]._id.AY]) continue;
        flags[total[i]._id.AY] = true;
        distinctMonth.push(total[i]._id.AY);
        }
        distinctMonth.sort(function(a, b) {
          return a - b;
        });
        let findBills;
        let ay=[];
        let find;
        await Promise.all(subscriber.map(async(x)=>{
          ay=[];
          findBills=total.filter(y=>y._id.SOZLESME_HESAP_NO==x.SOZLESME_HESAP_NO)
           await findBills.map(async(a)=>{
            ay.push(a._id.AY)
            if(ay.length==findBills.length){
              await Promise.all(distinctMonth.map(async (y,i)=>{
                find=ay.includes(y)
                if(find==false){
                  let billJson={
                    _id:{SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO,AY:y},
                    ORTALAMA_TUKETIM:0,
                    TUTAR:0
                  }
                  total.push(billJson)
                }
              }))
            } 
          })       
        })) 
        total.sort(function(a, b) {
          return a._id.FATURA_THK_SON - b._id.FATURA_THK_SON;
        });
        let json = [];
        total.map((x, y) => {
          json[y] = {
            AY: x._id.AY,
            ORTALAMA_TUKETIM: x.ORTALAMA_TUKETIM,
            TUTAR:x.TUTAR
          };
        });
        let comparison;
        await Promise.all(json.map((x)=>{
          comparison=json.filter(z => z.AY==x.AY)
          result={
            title: monthNames[x.AY-1],
            comparison:comparison
          } 
          array.push(result)
        }))
      let resultLast={
        SUBSCRIBER:subscriber,
        COMPARISON:array
      }
      return resultLast;
    },
   
    async comparisonBills(ctx){
      
      let subscriber=ctx.params.SUBSCRIBER
      let subscriberToken;

      if (ctx.meta.user.vergiNo) {
        subscriberToken = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriberToken = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }
      let findSubs=  await Promise.all(subscriber.map(async(x) =>{
          return  await subscriberToken.find(item => {
           return item.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO
           })
      }))
    const found = findSubs.includes(undefined);
    if(found){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0001", lang: "TR" },
      });
      throw Error(resultMessage.message);
    }


      let monthNames = ["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      let result;
      let array=[];

        let bills = await this.broker.call("bill.find", {
          query: {
            SOZLESME_HESAP_NO: {
              $in: subscriber.map((x) => x.SOZLESME_HESAP_NO),
            },        
          },
          sort: 'FATURA_THK_SON'
        });

      var flags = [];
        var distinctMonth = [];
        for(let i=0; i<bills.length; i++) {
        if( flags[bills[i].AY]) continue;
        flags[bills[i].AY] = true;
        distinctMonth.push(bills[i].AY);
        }
        
   
        let findBills;
        let ay=[];
        let find;

        await Promise.all(subscriber.map(async(x)=>{
          ay=[];
          findBills=bills.filter(y=>y.SOZLESME_HESAP_NO==x.SOZLESME_HESAP_NO)
           await findBills.map(async(a)=>{
            ay.push(a.AY)
            if(ay.length==findBills.length){
              await Promise.all(distinctMonth.map(async (y,i)=>{
                find=ay.includes(y)
                if(find==false){
                  let billJson={
                    _id:{SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO,AY:y},
                    ORTALAMA_TUKETIM:0,
                    TUTAR:0
                  }  
                  bills.push(billJson)
                }
              }))
            } 
          })       
        })) 
        let json = [];
        bills.map((x, y) => {

          json[y] = {
            AY: x.AY,
            ORTALAMA_TUKETIM: x.ORTALAMA_TUKETIM,
            TUTAR:x.TUTAR
          };
        });
        let comparison;

        await Promise.all(ay.map((x)=>{
          comparison=json.filter(z => z.AY==x)
          result={
            title: monthNames[x-1],
            comparison:comparison
          } 
          array.push(result)
        }))
      let resultLast={
        SUBSCRIBER:subscriber,
        COMPARISON:array
      }
      return resultLast;
    },
    async viewBill(ctx){
   
			const url = 'http://integration.iceteknoloji.com.tr/integration.asmx';
		
			const sampleHeaders = {
				'user-agent': 'sampleTest',
        'SOAPAction': 'http://tempuri.org/GetInvoice_HTML',
        'Content-Type': 'text/xml; charset=utf-8'

			};
			const xml = '<?xml version="1.0" encoding="utf-8"?>'+
      '<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'+
        '<soap12:Body>'+
          '<GetInvoice_HTML xmlns="http://tempuri.org/">'+
            '<getInvoice_ETTN>'+
              '<Login_Request_Header>'+
                '<Session_ID>D8A96906-8C2F-4DFB-A7BD-A0F5DEFE8506</Session_ID>'+
                '<IP_Number></IP_Number>'+
                '<Security_Key></Security_Key>'+
              '</Login_Request_Header>'+
              '<ETTN>'+ctx.params.euuId+'</ETTN>'+
            '</getInvoice_ETTN>'+
          '</GetInvoice_HTML>'+
        '</soap12:Body>'+
      '</soap12:Envelope>';

			const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xml, timeout: 60000 }); // Optional timeout parameter(milliseconds)
			
			const { headers, body, statusCode } = response;

			const regex = /<GetInvoice_HTMLResult>[\s\S]*?<\/GetInvoice_HTMLResult>/g;
			let found = body.match(regex);
      if(found) {
				found = found[0].replace('<GetInvoice_HTMLResult>','').replace('</GetInvoice_HTMLResult>','');
			}
    let pdfResult  =await this.getPdfDetail(ctx.params.euuId)
    let json={
      billDetail : found,
      pdfDetail:pdfResult
    }
    
       return json;



    },
    async createBillWillImages(ctx){
      let subscriber;
      if (ctx.meta.user.vergiNo) {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }

    let findSubs=  await subscriber.find(item => {
        return item.SOZLESME_HESAP_NO == ctx.params.SOZLESME_HESAP_NO
      })
    if(!findSubs){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0001", lang: "TR" },
      });
      throw Error(resultMessage.message);
    }
     
     
     
      let findSubscriber;
    findSubscriber = await this.broker.call("subscriber.find", {
      query: {
        $and:[
          {SOZLESME_HESAP_NO: ctx.params.SOZLESME_HESAP_NO},
          { ABONE_TURU_STNT : "Kurul onaylı tarifelerle enerji alan tük" },
          { ABONE_GRUBU : "Mesken" }  
        ]
      }})
      if(findSubscriber.length<1){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0089", lang: 'TR' },
        });
        throw Error(resultMessage.message);
      }
      let transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: process.env.SMTP_PORT,
        secure: false,
        tls: {
          rejectUnauthorized: false
      }
      });

      let mailerName = process.env.SMTP_NAME || "Akedaş";
      let text=ctx.params.SOZLESME_HESAP_NO+"/ "+findSubscriber[0].TEKIL_KOD+" Sözleşme Hesap/ Tüketim Noktası numaralı müşteriye ait  Sayaç Fotoğrafları ektedir. \n"
  let   attachments= []
      ctx.params.images.map((x)=>{
        let json={
          filename:x.url,
        path: x.url,
        cid: 'unique@kreata.ee' 
        }
        text=text+x.url+" "
        attachments.push(json)
      })
      let info = await transporter.sendMail({
        from: '"' + mailerName + '" <' + process.env.SMTP_EMAIL + '>', // noreply@arneca.com ile değişecek
        to: "burak.kulakli@akedasdagitim.com.tr,ORHAN.GUNES@akedasdagitim.com.tr,helpdesk@akedasdagitim.com.tr", // list of receivers
        subject: "Fatura Oluşturma", // Subject line
        text:  text, // plain text body
        html: text, // html body
        attachments: attachments,
      });
     let result={}
     
      result.resultBool= true
      result.messageId = "0025";
      return result;
    },
  },
  methods: {
    async getPdfDetail(euuId) {
      const url = 'http://integration.iceteknoloji.com.tr/integration.asmx';
		
			const sampleHeaders = {
        'SOAPAction': 'http://tempuri.org/GetInvoice_PDF',
        'Content-Type': 'text/xml; charset=utf-8'

			};

			const xml = '<?xml version="1.0" encoding="utf-8"?>'+
      '<soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'+
        '<soap12:Body>'+
            '<GetInvoice_PDF xmlns="http://tempuri.org/">'+
              '<getInvoice_ETTN>'+
                      '<Login_Request_Header>'+
                       '<Session_ID>D8A96906-8C2F-4DFB-A7BD-A0F5DEFE8506</Session_ID>'+
                        '<IP_Number></IP_Number>'+
                        '<Security_Key></Security_Key>'+
                        '</Login_Request_Header>'+   
                        '<ETTN>'+euuId+'</ETTN>'+
              '</getInvoice_ETTN>'+
            '</GetInvoice_PDF>'+
          '</soap12:Body>'+
      '</soap12:Envelope>';

			const { response } = await soapRequest({ url: url, headers: sampleHeaders, xml: xml, timeout: 60000 }); // Optional timeout parameter(milliseconds)
			
			const { headers, body, statusCode } = response;

			const regex = /<GetInvoice_PDFResult>[\s\S]*?<\/GetInvoice_PDFResult>/g;
			let found = body.match(regex);
      if(found) {
				found = found[0].replace('<GetInvoice_PDFResult>','').replace('</GetInvoice_PDFResult>','');
			}
      return  found;
    },
  },

};