"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var axios = require("axios");
var FormData = require("form-data");
var CryptoJS = require("crypto-js");
let ApiReferenceID = nanoid(25);
let securityHelper = require("./helpers/securityHelper");
const moment = require("moment");
module.exports = {
  name: "payment",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "payment",

  settings: {
    entityValidator: {
      $$strict: true,
      _id: { type: "string" },
      pan: { type: "string" },
      expiry: { type: "string" },
      amount: { type: "number" },
      cvv: { type: "string" },
      installmentCount: { type: "string" },
      requestType: { type: "string" },
      requestTime: { type: "date" },
      status: { type: "string" },
    },
  },
  actions: {
    async moka(ctx) {
      const Amount=securityHelper.decrypt(ctx.params.price)
      let mokaData = {
        PaymentDealerAuthentication: {
          DealerCode: "1731",
          Username: "TestMoka2",
          Password: "HYSYHDS8DU8HU",
          CheckKey:
            "1c1cccfe19b782415c207f1d66f97889cf11ed6d1e1ad6f585e5fe70b6f5da90",
        },
        //https://service.RefMoka.com/PaymentDealerThreeD
        WebPosRequest: {
          Amount: Amount,
          Currency: "TL",
          InstallmentNumber: 1,
          OtherTrxCode: "",
          ClientWebPosTypeId: 0,
          RedirectUrl: `https://testmobil.akedas.com.tr/api/mokaTahsilat?A=${Amount || 60}`,
          RedirectType: 0,
          IsThreeD: 1,
          IsPreAuth: 0,
          IsPoolPayment: 0,
          IsTokenized: 0,
          Language: "",
          SubMerchantName: "",
          Description: "",
        },
      };
      var config = {
        method: "post",
        url: "https://clientwebpos.RefMoka.com/Api/WebPos/CreateWebPosRequest",
        headers: {
          "Content-Type": "application/json",
        },
        data: mokaData,
      };
      return await axios(config)
        .then(function (response) {
          let mokaUrl = response.data.Data.Url;
          return mokaUrl;
        })
        .catch(function (error) {
          console.log(error);
        });
    },

    async octet(ctx) {

      const Amount=securityHelper.decrypt(ctx.params.price)
      let x=securityHelper.decrypt(ctx.params.user)
      let user=x.split(",")
if(user[3]){
  user[3]=user[3].replace("+","00")
}
console.log(user[2])
     console.log(user)
      let addedDate = moment(new Date()).add(3, "minutes");
      let last = moment(addedDate).format("YYYY-MM-DD HH:mm:ss");

      var secretKey = "AKEDAS65478397HFG352CVPS";
      let data = {
        action: "CREATE_COMMON_PAYMENT_PAGE_REQUEST",
        partnerCode: "AKEDASELEKTRIK",
        sellerReference: "Akedas elektrik",
        paymentAmount: Amount,
        currency: "TRY",
        expireDateTime: last,
        expireDateShowText: "Kalan Süre {time} ",
        buyerName: user[0]!=undefined?user[0]:null,
        buyerSurname:  user[1]!=undefined?user[1]:null,
        buyerMobilePhone:  user[3]!=undefined?user[3]:null,
        // buyerEmail:  user[4]!=undefined?user[4]:null,
       // buyerTCKN: user[2]!=undefined?user[2]:null,
       // buyerName: "Buyer name",
       // buyerSurname: "buyer surname",
      //  buyerMobilePhone: "00905991111111",
        buyerEmail: "",
        buyerTCKN: "11111111111",
        buyerCompanyName: "Buyer Comp.",
        buyerCompanyName: "Akedas",
        buyerTaxOffice: "Kurumlar",
        buyerTaxNumber: "1111111111",
        language: "tr",
        apiReferenceID: ApiReferenceID,
        apiReferenceInfo: "",
        //"https://uat.octet.com.tr/cppdemo/result.aspx"
        returnPage: `https://testmobil.akedas.com.tr/api/octetTahsilat?A=${Amount || 60}}`,
        // returnPage: `http://localhost:5000/api/octetTahsilat?A=${Amount || 60}`,
        consumerCardInstallmentLimit: "",
        commercialCardInstallmentLimit: "",
        additionalAgreementText_CheckBoxText: "",
        additionalAgreementText_Data: "",
      };
      let securityKey = "";
      var formData = new FormData();
      Object.keys(data).forEach(function (key) {
        formData.append(key, data[key]);
        securityKey += data[key];
      });

      securityKey = CryptoJS.SHA1(securityKey + secretKey).toString();
      formData.append("securityKey", securityKey);

      var config = {
        method: "post",
        url: "https://uat.octet.com.tr/api/http_api.aspx",
        headers: {
          ...formData.getHeaders(),
        },
        data: formData,
      };
      return await axios(config)
        .then(function (response) {
          console.log("ss")
          console.log(response.data)
          let octetUrl = response.data.resultData.commonPaymentPageURL;
          return octetUrl;
        })
        .catch(function (error) {
          console.log("error")
          console.log(error);
        });
    },

    async payment(ctx) {
      if (ctx.params.DEBITLIST.length > 0) {
        let data = await this.broker.call(
          "bill.billFind",
          {
            DEBITLIST: ctx.params.DEBITLIST,
          },
          { meta: { user: ctx.meta.user } }
        );
        let total = data.total.toString();
        const hashCode = securityHelper.encrypt(total);
        let user=ctx.meta.user.name+","+ctx.meta.user.lastname+","+ctx.meta.user.tcNo+","+ctx.meta.user.phone+","+ctx.meta.user.email+","+ctx.meta.user.vergiNo
        //  const decodeHashCode=securityHelper.decrypt(hashCode)
        const uHash = securityHelper.encrypt(user);
        let url = "https://vpos.eventapp.app/?t=" + hashCode+"&u="+uHash;
        let json = {
          url: url,
        };
        return json;
      } else {
        throw Error("Ödeme yapılacak Fatura seçilmelidir.");
      }
    },
  },
  methods: {},
};
