'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');
module.exports = {
	name: 'requestAndComplaintCategories',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'requestAndComplaintCategories',
	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			name: { type: 'string', optional: true },
			lang:{type: 'string', optional: true},
			order: 'number',
			createdAt: 'date',
			updatedAt: 'date',

		}

	},
	hooks: {
		before: {
			create: ctx => {
				let x = ctx.params
				x._id = nanoid(25);
				x.createdAt = dayjs().toDate();
				x.updatedAt = dayjs().toDate();

			},

			update: ctx => {
				let x = ctx.params
				x.updatedAt = dayjs().toDate();
				x.updatedBy = ctx.meta.user._id
			},

		},
	}

};
