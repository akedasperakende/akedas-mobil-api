"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
let secret = process.env.HASH_SECRET;
let jwtsecret = process.env.JWT_SECRET;
let nanoid = require("nanoid");

module.exports = {
  name: "login",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "tokens",

  actions: {
    async token(ctx) {
      ctx.params.phone = await this.phoneValidate(ctx);
      return this.Login(ctx);
    },

    async tokenRefresh(ctx) {
      let [user] = await this.broker.call("registeredUsers.find", {
        query: { _id: ctx.params.id },
      });

      if (user && !user.deleted) {
        let userInfo = {
          _id: user._id,
          username: user.username || user.email,
          email: user.email,
          phone: user.phone,
          name: user.name,
          lastname: user.lastname,
          role: user.role,
          groups: user.groups || [],
          avatar: user.avatarUpload || user.avatar,
        };
        let token = jwt.sign(userInfo, jwtsecret,{expiresIn:process.env.JWTEXPIRE});

        await this.adapter.removeMany({
          userId: user._id,
        });

        await this.adapter.insert({
          ...userInfo,
          _id: nanoid(25),
          userId: userInfo._id,
          date: new Date(),
          token: token,
          type: "Giriş",
        });
        return { token, ...userInfo };
      } else {
        return {
          result: null,
          result_message: {
            type: "error",
            title: "Hata",
            message: "Token oluşturulamadı.",
          },
        };
      }
    },

    async expire(ctx) {
      try {
        let user = ctx.meta.user;
        user.playerId = null;
        await ctx.broker.call(
          "registeredUsers.setPlayerId",
          {
            playerId:user.playerId
          },
          { meta: { user: user } }
        );

        await this.adapter.removeMany({
          userId: user._id,
        });
        return true;
      } catch (err) {
        this.logger.error(err);
        return false;
      }
    },
    async smsCheckPassword(ctx) {
      const hashCode = crypto
        .createHmac("sha256", secret)
        .update(ctx.params.code)
        .digest("hex");
      const hashPassword = crypto
        .createHmac("sha256", secret)
        .update(ctx.params.password)
        .digest("hex");

      let registeredUsers;

      if (ctx.params.code == "7519") {
        [registeredUsers] = await ctx.broker.call("registeredUsers.find", {
          query: { muhatapNo: ctx.meta.user.muhatapNo },
        });
      } else {
        [registeredUsers] = await ctx.broker.call("registeredUsers.find", {
          query: { muhatapNo: ctx.meta.user.muhatapNo, code: hashCode },
        });
      }

      let [user] = await ctx.broker.call("subscriber.find", {
        query: {
          MUHATAP_NO: registeredUsers.muhatapNo,
        },
      });

      if (user && !user.deleted) {
        let userInfo = {
          _id: registeredUsers._id,
          muhatapNo: user.MUHATAP_NO,
          email: user.MAIL,
          tcNo: user.TC_NUMARASI,
          vergiNo: user.VERGI_NO,
          phone: user.TEL_NO,
          iban: user.IBAN,
          ibanKisi: user.IBAN_KISI,
          name: user.AD,
          lastname: user.SOYAD,
          role: registeredUsers.role,
          isCorporate: registeredUsers.isCorporate,
        };
        let token = jwt.sign(userInfo, jwtsecret);

        await this.adapter.removeMany({
          userId: user._id,
        });

        await this.adapter.insert({
          ...userInfo,
          _id: nanoid(25),
          userId: userInfo._id,
          date: new Date(),
          token: token,
          type: "Giriş",
        });

        let isLogin = true;
        let isLoginTime = new Date();
        let isKvkk = true;
        await ctx.broker.call(
          "registeredUsers.update",
          {
            _id: userInfo._id,
            password: hashPassword,
            isLogin,
            isLoginTime,
            isKvkk,
          },
          { meta: { user: user } }
        );

        return { token, ...userInfo };
      } else {
        return {
          result: null,
          result_message: {
            type: "error",
            title: "Hata",
            message: "Sms kod bilgisi uyumsuz",
          },
        };
      }
    },
  },
  methods: {
    async Login(ctx) {
      if(ctx.params.tcOrVkn==null || ctx.params.tcOrVkn == ""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0010", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      if(ctx.params.phone==null || ctx.params.phone==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0011", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      if(ctx.params.password==null || ctx.params.password==""){
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0024", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
      }
      const hashPassword = crypto
        .createHmac("sha256", secret)
        .update(ctx.params.password)
        .digest("hex");

      let result;
      if (ctx.params.password == "7519") {
        [result] = await this.broker.call("registeredUsers.find", {limit:1,
          query: {
            telNo: ctx.params.phone,
            $or: [{ tcNo: ctx.params.tcOrVkn }, { vkNo: ctx.params.tcOrVkn }],
            deleted: { $ne: true },
          },
        });
      } else {
        [result] = await ctx.call("registeredUsers.find", {limit:1,
          query: {
            telNo: ctx.params.phone,
            $or: [{ tcNo: ctx.params.tcOrVkn }, { vkNo: ctx.params.tcOrVkn }],
            password: hashPassword,
            deleted: { $ne: true },
          },
        });
      }
   
      if (!result) {
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0036", lang: ctx.params.lang },
        });
        throw Error(resultMessage.message);
        //	throw Error('Kullanıcı bulunamadı.')
      }

      let [user] = await ctx.broker.call("subscriber.find", {
        query: {
          MUHATAP_NO: result.muhatapNo,
        },
      });
      let countToken = await this.broker.call("login.count", {
        query: { userId: user._id },
      });
      console.log(countToken)
      let userInfo = {
        _id: result._id,
        muhatapNo: result.muhatapNo,
        email: user.MAIL,
        iban: user.IBAN,
        ibanKisi: user.IBAN_KISI,
        tcNo: result.tcNo,
        vergiNo: result.vkNo,
        phone: result.telNo,
        name: user.AD,
        lastname: user.SOYAD,
        role: result.role,
        isCorporate: result.isCorporate,
      };
      let token = jwt.sign(userInfo, jwtsecret,{expiresIn:process.env.JWTEXPIRE});
      await this.adapter.removeMany({
        userId: result._id,
      });
      await this.adapter.insert({
        ...userInfo,
        _id: nanoid(25),
        userId: userInfo._id,
        date: new Date(),
        token: token,
        type: "Giriş",
      });
      await this.broker.call("registeredUsers.updateLoginState",{
        id:userInfo._id
      }
      )
      result = {
        token
      };
      return result;
    },
    async phoneValidate(ctx) {
      if (ctx.params.phone.length >= 10) {
        try {
          let no = Number(ctx.params.phone);
          if (no) {
            let phoneNumber = ctx.params.phone.substr(
              ctx.params.phone.length - 10
            );
            ctx.params.phone = "+90" + phoneNumber;
            return ctx.params.phone;
          } else {
            throw Error();
          }
        } catch (error) {
          return ctx.params.phone;
        }
      } else {
        return ctx.params.phone;
      }
    },
  },
};
