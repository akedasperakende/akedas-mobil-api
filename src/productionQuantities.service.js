"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
const dayjs = require('dayjs');
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "productionQuantities",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "uretim_miktarlari",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let productionQuantitiestable = await this.broker.call(
        "productionQuantities.count"
      );
      const result = await request.query(
        "SELECT * FROM uretim_miktarları_servis_donus"
      );

      console.log(result);

      if (productionQuantitiestable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let productionQuantities = await this.broker.call(
          "productionQuantities.count"
        );
        if (productionQuantities == 0) {
          let record = await result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.date =r.date  == null ? null: dayjs(r.date).toDate()),
              (r.mounth =dayjs(r.date).month() + 1),
              (r.fueloil =r.fueloil  == null ? null: Number(r.fueloil)),
              (r.gasOil =r.gasOil == null ? null: Number(r.gasOil)),
              (r.blackCoal =r.blackCoal == null ? null: Number(r.blackCoal)),
              (r.lignite =r.lignite == null ? null: Number(r.lignite)),
              (r.geothermal =r.geothermal == null ? null: Number(r.geothermal)),
              (r.naturalGas =r.naturalGas == null ? null: Number(r.naturalGas)),
              (r.river = r.river== null ? null: Number(r.river)),
              (r.dammedHydro =r.dammedHydro == null ? null: Number(r.dammedHydro)),
              (r.lng =r.lng == null ? null: Number(r.lng)),
              (r.biomass =r.biomass == null ? null: Number(r.biomass)),
              (r.naphta =r.naphta == null ? null: Number(r.naphta)),
              (r.importCoal =r.importCoal == null ? null: Number(r.importCoal)),
              (r.asphaltiteCoal =r.asphaltiteCoal == null ? null: Number(r.asphaltiteCoal)),
              (r.wind = r.wind== null ? null: Number(r.wind)),
              (r.nucklear =r.nucklear == null ? null: Number(r.nucklear)),
              (r.sun =r.sun == null ? null: Number(r.sun)),
              (r.importExport =r.importExport == null ? null: Number(r.importExport)),
              (r.total = r.total== null ? null: Number(r.total)),
              (r.id_santral =r.id_santral == null ? null: Number(r.id_santral)),
              (r.santral_adi =r.santral_adi == null ? null:String(r.santral_adi));
            return r;
          });
          await this.adapter.insertMany(record);
          return true;
        }
      }

      if (productionQuantitiestable == 0 && result.recordset.length > 0) {
        let record = await result.recordset.map((r) => {
          (r._id = nanoid(25)),
            (r.date =r.date  == null ? null: dayjs(r.date).toDate()),
              (r.mounth =dayjs(r.date).month() + 1),
              (r.fueloil =r.fueloil  == null ? null: Number(r.fueloil)),
              (r.gasOil =r.gasOil == null ? null: Number(r.gasOil)),
              (r.blackCoal =r.blackCoal == null ? null: Number(r.blackCoal)),
              (r.lignite =r.lignite == null ? null: Number(r.lignite)),
              (r.geothermal =r.geothermal == null ? null: Number(r.geothermal)),
              (r.naturalGas =r.naturalGas == null ? null: Number(r.naturalGas)),
              (r.river = r.river== null ? null: Number(r.river)),
              (r.dammedHydro =r.dammedHydro == null ? null: Number(r.dammedHydro)),
              (r.lng =r.lng == null ? null: Number(r.lng)),
              (r.biomass =r.biomass == null ? null: Number(r.biomass)),
              (r.naphta =r.naphta == null ? null: Number(r.naphta)),
              (r.importCoal =r.importCoal == null ? null: Number(r.importCoal)),
              (r.asphaltiteCoal =r.asphaltiteCoal == null ? null: Number(r.asphaltiteCoal)),
              (r.wind = r.wind== null ? null: Number(r.wind)),
              (r.nucklear =r.nucklear == null ? null: Number(r.nucklear)),
              (r.sun =r.sun == null ? null: Number(r.sun)),
              (r.importExport =r.importExport == null ? null: Number(r.importExport)),
              (r.total = r.total== null ? null: Number(r.total)),
              (r.id_santral =r.id_santral == null ? null: Number(r.id_santral)),
              (r.santral_adi =r.santral_adi == null ? null:String(r.santral_adi));
          return r;
        });
        await this.adapter.insertMany(record);
        return true;
      }
    },
    async quantities() {
      let total;
      
      total = await this.adapter.db
        .collection("uretim_miktarlari")
        .aggregate([
          {$group: {
              _id:  "$mounth", 
              "fueloil": {$sum: "$fueloil"}, 
              "gasOil": {$sum: "$gasOil"},
              "blackCoal": {$sum: "$blackCoal"}, 
              "lignite": {$sum: "$lignite"},
              "geothermal": {$sum: "$geothermal"}, 
              "naturalGas": {$sum: "$naturalGas"},
              "river": {$sum: "$river"}, 
              "dammedHydro": {$sum: "$dammedHydro"},
              "lng": {$sum: "$lng"}, 
              "biomass": {$sum: "$biomass"},
              "naphta": {$sum: "$naphta"}, 
              "gasOil": {$sum: "$gasOil"},
              "importCoal": {$sum: "$importCoal"}, 
              "asphaltiteCoal": {$sum: "$asphaltiteCoal"},
              "wind": {$sum: "$wind"}, 
              "nucklear": {$sum: "$nucklear"},
              "sun": {$sum: "$sun"}, 
              "importExport": {$sum: "$importExport"}
          }},
      ]).toArray();

        let data = [];
        let sumJson=[];
        let sum={};
        await Promise.all( 
          total.map(async (x, i) => {
            const event = new Date(Date.UTC(0, x._id, 0, 0, 0, 0));
            let month=event.toLocaleDateString("tr-TR", { month: 'long'})
          data[i] = {
            month: month,
            _id:  x._id, 
            'Akaryakıt Enerji Santrali':x.fueloil,
            'Akaryakıt Enerji Santrali': x.gasOil,
            'Termik (Katı Yakıt) Santralleri':x.blackCoal,
            'Linyit Santralleri':x.lignite,
            'Jeotermal Enerji Santraller': x.geothermal,
            'Doğal Gaz Santralleri': x.naturalGas,
            'Akarsu Santralleri':x.river,
            'Hidro Elektrik Santralleri':x.dammedHydro,
            'Akaryakıt Enerji Santrali':x.lng,
            'BiyoKütle': x.biomass,
            'Akaryakıt Enerji Santrali': x.naphta,
            'İthal Kömür Santralleri': x.importCoal, 
            'Termik (Katı Yakıt) Santralleri': x.asphaltiteCoal,
            'Rüzgâr Enerji Santralleri': x.wind,
            'Nükleer Santraller': x.nucklear,
            'Güneş Enerji Santralleri': x.sun,
            importExport: x.importExport,
          };    
          sum={
            month: "Toplam",
            _id: "0",
            "fueloil": sum.hasOwnProperty('fueloil')?sum.fueloil+x.fueloil:x.fueloil, 
            "gasOil":  sum.hasOwnProperty('gasOil')?sum.gasOil+x.gasOil:x.gasOil, 
            "blackCoal": sum.hasOwnProperty('blackCoal')?sum.blackCoal+x.blackCoal:x.blackCoal, 
            "lignite":  sum.hasOwnProperty('lignite')?sum.lignite+x.lignite:x.lignite, 
            "geothermal":  sum.hasOwnProperty('geothermal')?sum.geothermal+x.geothermal:x.geothermal, 
            "naturalGas": sum.hasOwnProperty('naturalGas')?sum.naturalGas+x.naturalGas:x.naturalGas, 
            "river":  sum.hasOwnProperty('river')?sum.river+x.river:x.river, 
            "dammedHydro": sum.hasOwnProperty('dammedHydro')?sum.dammedHydro+x.dammedHydro:x.dammedHydro, 
            "lng":  sum.hasOwnProperty('lng')?sum.lng+x.lng:x.lng, 
            "biomass": sum.hasOwnProperty('biomass')?sum.biomass+x.biomass:x.biomass, 
            "naphta": sum.hasOwnProperty('naphta')?sum.naphta+x.naphta:x.naphta, 
            "gasOil":  sum.hasOwnProperty('gasOil')?sum.gasOil+x.gasOil:x.gasOil, 
            "importCoal":  sum.hasOwnProperty('importCoal')?sum.importCoal+x.importCoal:x.importCoal, 
            "asphaltiteCoal": sum.hasOwnProperty('asphaltiteCoal')?sum.asphaltiteCoal+x.asphaltiteCoal:x.asphaltiteCoal, 
            "wind":  sum.hasOwnProperty('wind')?sum.wind+x.wind:x.wind, 
            "nucklear":  sum.hasOwnProperty('nucklear')?sum.nucklear+x.nucklear:x.nucklear, 
            "sun":  sum.hasOwnProperty('sun')?sum.sun+x.sun:x.sun, 
            "importExport":  sum.hasOwnProperty('importExport')?sum.importExport+x.importExport:x.importExport, 
          }
          sumJson[i] = {
            month: month,
            _id:  x._id, 
          sumValue:(x.fueloil+x.gasOil+x.fueloil+ x.gasOil+x.blackCoal+x.lignite+ x.geothermal+x.naturalGas+x.river+x.dammedHydro+x.lng+ x.biomass+x.naphta+x.importCoal+ x.asphaltiteCoal+x.wind+ x.nucklear+x.sun)
          };                      
        }));
        let toplam=0
        sumJson.map(async (x, i) => {
          toplam=x.sumValue+toplam
        })
        let json={
           month: sum.month,
            _id: sum._id, 
            'Akaryakıt Enerji Santrali':sum.fueloil,
            'Akaryakıt Enerji Santrali':sum.gasOil,
            'Termik (Katı Yakıt) Santralleri':sum.blackCoal,
            'Linyit Santralleri':sum.lignite,
            'Jeotermal Enerji Santraller':sum.geothermal,
            'Doğal Gaz Santralleri':sum.naturalGas,
            'Akarsu Santralleri':sum.river,
            'Hidro Elektrik Santralleri':sum.dammedHydro,
            'Akaryakıt Enerji Santrali':sum.lng,
            'BiyoKütle':sum.biomass,
            'Akaryakıt Enerji Santrali':sum.naphta,
            'İthal Kömür Santralleri':sum.importCoal, 
            'Termik (Katı Yakıt) Santralleri':sum.asphaltiteCoal,
            'Rüzgâr Enerji Santralleri':sum.wind,
            'Nükleer Santraller':sum.nucklear,
            'Güneş Enerji Santralleri':sum.sun,
            importExport:sum.importExport,
        }

        data.push(json)
        let sumData={
          yearTotal:toplam,
          monthsTotal:sumJson
        }
      return {data,sumData};
    },
  },
};
