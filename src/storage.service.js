'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const fs = require('fs');
const nanoid = require('nanoid');
const ThumbnailGenerator = require('video-thumbnail-generator').default;
const getVideoInfo = require('get-video-info');
const path = require('path');
const sharp = require('sharp');
let host = process.env.HOST

module.exports = {
	name: 'storage',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'storage',

	actions: {
		async save(ctx) {

			let result = await Promise.all(ctx.params.files.map(async (file) => {
				return await this.uploadFile(file);
			}));

			return result;
		},

		async deleteUploaded(ctx) {
			await ctx.params.files.forEach((file) => {
				if (ctx.meta.user.role !== "system")
					return false
				else {
					if (file.url)
						fs.unlink(("./public" + file.url.replace(host, '')), (err => {
							if (err) console.log(err);
							else {
								fs.appendFileSync("./deletedFile.txt", JSON.stringify({ "url": file.url, date: new Date() }) + ",");
							}
						}));
					if (file.thumb)
						fs.unlink(("./public" + file.thumb.replace(host, '')), (err => {
							if (err) console.log(err);
							else {
								fs.appendFileSync("./deletedFile.txt", JSON.stringify({ "thumb": file.thumb, date: new Date() }) + ",");
							}

						}));
				}
			});

			return true;
		}
	},

	methods: {
		async uploadFile(file) {
			let filename = file.filename.split('.');
			let id = nanoid(24) + (filename.length >= 2 ? '.' + filename[1] : '');
			let tmpPath = path.join(__dirname, '/../public/files/')
			let mediaPath = path.join(tmpPath, id)
			let thumpPath = path.join(tmpPath, 'thump_' + id)
			let url = host + `/files/${id}`
			let thump = host + `/files/thump_${id}`
			let duration = null
			let type = file.mimetype.split('/')[0];




			if (!(type == 'video' || type == 'image')) {
				type = 'document';
			}


			let imageWidth = 0;
			let imageHeight = 0;
			let imageThumbWidth = 0;
			let imageThumbHeight = 0;

			try {
				if (file.mimetype.startsWith('video')) {

					await new Promise((res, rej) => {
						fs.writeFile(mediaPath, file.data, (err) => {
							if (err) rej(err)
							else res(true);
						});
					});

					let width = 400;
					let height = 200;

					let info = await getVideoInfo(mediaPath)


					if (info && info.streams) {
						let video = info.streams.find(x => x.codec_type === 'video')
						duration = Number(video.duration)

						if (video && video.tags && video.tags.rotate) {
							width = video.height;
							height = video.width;
						}
						else {
							width = video.width;
							height = video.height;
						}
					}

					let ratio = height / width;

					let tg = new ThumbnailGenerator({
						sourcePath: mediaPath,
						thumbnailPath: tmpPath
					});

					imageHeight = width >= height ? (400 * ratio) : 200
					imageWidth = width >= height ? 400 : (200 / ratio)

					let result = null;
					if (width >= height)
						result = await tg.generate({ size: `${400}x${Math.floor(400 * ratio)}` });
					else result = await tg.generate({ size: `${Math.floor(200 / ratio)}x${200}` });

					let _thump = result[0];
					thump = host + `/files/${_thump}`
				}

				else if (file.mimetype.startsWith('image')) {
					await sharp(file.data)
						.metadata()
						.then(({ width, height, size, format }) => {
							let ratio = height / width;
							if (ratio > 1) {
								imageWidth = height < 800 ? width : 800 > height > 2000 ? Math.floor(width / ratio) : Math.floor(1000 / ratio)
								imageHeight = height < 800 ? height : 800 > height > 2000 ? Math.floor(height / ratio) : 1000
							}
							else if (ratio < 1) {
								imageWidth = width < 800 ? width : 800 > width > 2000 ? Math.floor(width / ratio) : 1000
								imageHeight = width < 800 ? height : 800 > width > 2000 ? Math.floor(height / ratio) : Math.floor(ratio * 1000)
							}
							else {
								imageWidth = width > 1000 ? 1000 : width
								imageHeight = height > 1000 ? 1000 : height
							}
							sharp(file.data).rotate().jpeg({ quality: 90 })
								.resize({ width: imageWidth, height: imageHeight }).toFormat("jpeg")
								.toBuffer().then(data => {
									fs.writeFile(mediaPath, data, (err) => {
										if (err) throw Error("Media yüklenirken hata oluştu.")
										else return true
									});
								})
						});

					//THUMB

					await sharp(file.data)
						.metadata()
						.then(({ width, height, size, format }) => {
							let ratio = height / width;
							if (ratio > 1) {
								imageThumbWidth = height > 400 ? Math.floor(400 / ratio) : width
								imageThumbHeight = height > 400 ? 400 : height
							}
							else if (ratio < 1) {
								imageThumbWidth = width > 400 ? 400 : width
								imageThumbHeight = width > 400 ? Math.floor(ratio * 400) : height
							}
							else {
								imageThumbWidth = width > 400 ? 400 : width
								imageThumbHeight = height > 400 ? 400 : height
							}
							sharp(file.data).jpeg({ quality: 100 })
								.resize({ width: imageThumbWidth, height: imageThumbHeight }).toFormat("jpeg")
								.toBuffer().then(data => {
									fs.writeFile(thumpPath, data, (err) => {
										if (err) throw Error("Media yüklenirken hata oluştu.")
										else return true
									});
								})
						});
				}
				else {
					await new Promise((res, rej) => {
						fs.writeFile(mediaPath, file.data, (err) => {
							if (err) rej(err)
							else res(true);
						});
					});
				}
				await this.adapter.insert({
					_id: id,
					url: url,
					thumb: thump || url,
					type,
					mimeType: file.mimetype,
					duration,
					width: Math.floor(imageWidth),
					height: Math.floor(imageHeight)
				});

				return {
					_id: id,
					url: url,
					thumb: thump || url,
					type,
					mimeType: file.mimetype,
					duration,
					width: Math.floor(imageWidth),
					height: Math.floor(imageHeight)
				};
			}
			catch (error) {
				return error.message
			}
		}
	}
};