'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
let dayjs = require('dayjs');
let nanoid = require('nanoid');

let icon =  {
	'_id': 'notification.png',
	'url': process.env.CDN + '/general/notification.png',
	'thumb': process.env.CDN + '/general/notification.png',
	'type': 'image',
	'mimeType': 'image/png'
}

module.exports = {
	name: 'pushNotifications',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'pushNotifications',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			title: {type: 'string'},
			active: 'boolean',
			isIndividual: 'boolean',
			muhatapNo:{ type: 'string', optional: true },
		//	pin: {type: 'boolean', optional: true},
			sendNow: { type: 'boolean', optional: true },
			sendNotification: {type: 'boolean', optional: true},
			completed: 'boolean',
			content: 'string',
			lang:  {type: 'string', optional: true},
			notifyType: {type: 'string', optional: true},
			subtype: {type: 'string', optional: true},
			item_id: {type: 'string', optional: true},
			date: 'date',
			created: 'date',
			registeredUsers: {type: 'array', optional: true, items: 'string'},
			registeredUserId: {type: 'string', optional: true},
			area: [{ type: 'array', items: 'string', optional: true }],
			district: [{ type: 'array', items: 'string', optional: true }],
			neighbourhood: [{ type: 'array', items: 'string', optional: true }],
			// groups: {type: 'array', optional: true, items: 'string'},
			// usersFile: {'type': 'object', optional: true, props: {
			// 	type: {type:'string'},
			// 	thumb: {type:'string'},
			// 	url: {type:'string'},
			// 	_id: {type:'string'},
			// 	mimeType: {type:'string'}
			// }},
			//type: {type: 'string', enum: ['not_modul', 'notifications', 'pushNotifications', 'postwall', 'survey', 'banners', 'user']},
			icon: {'type': 'object', props: {
				type: {type:'string'},
				thumb: {type:'string'},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}}
		}
	},

	actions: {
		async archive() {
			this.adapter.updateMany({
				date: {$lte: new Date()}
			}, {
				$set : {
					archive: true
				}
			});
		},	
		async individualNotification(ctx) {
			let [subscriber] = await this.broker.call("subscriber.find", {
				query: {
					SOZLESME_HESAP_NO: ctx.params.SOZLESME_HESAP_NO,
				},
				fields: ["MUHATAP_NO"]
			  });
			  let log=ctx.call('pushNotifications.create', 
			  {
				"muhatapNo":subscriber.MUHATAP_NO,
				"isIndividual":true,
				"content": ctx.params.message,
				"title": ctx.params.title,
				"active": true,
				"sendNow":true,
				"sendNotification": true
			  });
			  return log
		}
	},

	hooks: {
		before: {
				create: [ctx => {
				ctx.params._id = nanoid(25);
				ctx.params.date = dayjs(ctx.params.date || new Date()).toDate();
				ctx.params.created = new Date();
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
			//	ctx.params.groups = ctx.params.groups || [];
			//	ctx.params.type = ctx.params.type || 'notifications';
				ctx.params.notifyType = 'auto';
				ctx.params.completed = false;
				ctx.params.icon = ctx.params.icon || icon;
				ctx.params.lang = ctx.params.lang || "TR";
				ctx.params.area = ctx.params.area || [];
				ctx.params.district = ctx.params.district || [];
				ctx.params.neighbourhood = ctx.params.neighbourhood || [];
			//	ctx.params.media = ctx.params.media || icon; 
			}],

			update: [ctx => {
				let date = dayjs(ctx.params.date);
				ctx.params.date = date.toDate();
				ctx.params.registeredUsers = ctx.params.registeredUsers || [];
			//	ctx.params.groups = ctx.params.groups || [];
			//	ctx.params.type = ctx.params.type || 'notifications';
				ctx.params.lang = ctx.params.lang || "TR";
				ctx.params.area = ctx.params.area || [];
				ctx.params.district = ctx.params.district || [];
				ctx.params.neighbourhood = ctx.params.neighbourhood || [];
			//	ctx.params.notifyType = 'auto';
				// if(date.isAfter(dayjs(new Date())))
				// 	ctx.params.completed = false;
			}]
		},

		after: {
			update: (ctx,result) => {
				ctx.call('notifications.updateAll', {
					pushNotificationId: result._id,
					title: result.title,
					active: result.active,
					content: result.content,
					icon: result.icon,
				//	media: result.media,
					lang: result.lang,
				//	type: result.type
				})
				return result;
			}
		}
	}
};