'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');
var utc = require('dayjs/plugin/utc')
module.exports = {
	name: 'requestAndComplaint',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL,
		{ useNewUrlParser: true, useUnifiedTopology: true }, process.env.DB),
	collection: 'requestAndComplaint',
	settings: {
		entityValidator: {
			$$strict: true,
			_id: { type: 'string' },
			registeredUserId: { type: 'string'},
			SOZLESME_HESAP_NO: { type: 'string'},
			categoryId: { type: 'string'},
			subCategoryId: { type: 'string'},	
			address: { type: 'string' },
			message: { type: 'string'},
			createdAt: 'date',
			updatedAt: 'date',

		}

	},
	actions: {
	   async getCategory(ctx) {
		   
			let requestAndComplaintCategories = await this.broker.call("requestAndComplaintCategories.find",{sort:'order'});
			let requestAndComplaintSubCategories = await this.broker.call("requestAndComplaintSubCategories.find",{sort:'order'});
		   let subscriber;

		   if (ctx.params.user.vergiNo) {
			   subscriber = await this.broker.call("subscriber.find", {
			   query: {
				   VERGI_NO: ctx.params.user.vergiNo,
			   },
			   });
		   } else {
			   subscriber = await this.broker.call("subscriber.find", {
			   query: {
				   TC_NUMARASI: ctx.params.user.tcNo,
			   },
			   });
		   }
		   let subscribers=[]
		   subscriber.map((x)=>{
			let a={
				SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO
			}
			subscribers.push(a)
		   })
		   let category=[]
		   await Promise.all(

			   requestAndComplaintCategories.map(async (x,i) => {

			   let subCat=requestAndComplaintSubCategories.filter(y => y.categoryId == x._id)
			   let subCategory=[]
			   await subCat.map((x)=>{
				   let a={
					   _id:x._id,
					   name:x.name,
				   }
				   subCategory.push(a)
			   })

			   let  json={
				   _id:x._id,
				   categoryName:x.name,
				   subCategories:subCategory
				   }
				   category.push(json)
	   
			   })
		   );
		   let result={
			   subscriber:subscribers,
			   requestAndComplaintCategory:category
		   }
		   return result
	   
	   },
	 },
	hooks: {
		before: {
			create: async (ctx) => {
				let x = ctx.params
				let subscriber;
				if (ctx.meta.user.vergiNo) {
				  subscriber = await ctx.broker.call("subscriber.find", {
					query: {
					  VERGI_NO: ctx.meta.user.vergiNo,
					},
				  });
				} else {
				  subscriber = await ctx.broker.call("subscriber.find", {
					query: {
					  $and: [
						{$or:[
						  { VERGI_NO: { $eq: null } },
						  { VERGI_NO: { $eq: "null" } },
						]},
						{ TC_NUMARASI: ctx.meta.user.tcNo },             
					  ],
					},
				  });
				}
		  
			  let findSubs=  await subscriber.find(item => {
				  return item.SOZLESME_HESAP_NO == ctx.params.SOZLESME_HESAP_NO
				})
			  if(!findSubs){
				let [resultMessage] = await ctx.call("resultMessage.find", {
				  query: { code: "0001", lang: "TR" },
				});
				throw Error(resultMessage.message);
			  }
				
				let [requestAndComplaint] = await ctx.broker.call('requestAndComplaint.find', {
					sort: "-createdAt",
					query: {
						registeredUserId: x.registeredUserId,
						SOZLESME_HESAP_NO:x.SOZLESME_HESAP_NO
					 }
				});
				let now=dayjs().toDate()
				if(requestAndComplaint && dayjs(requestAndComplaint.createdAt).add(110, 'second').toDate() >now){
					let [resultMessage] = await ctx.broker.call("resultMessage.find", {
						 query: { code: "0037", lang: "TR" },
					 });
					 throw Error(resultMessage.message);
			 	}
				x._id = nanoid(25);
				x.createdAt = dayjs().toDate();
				x.updatedAt = dayjs().toDate();

			},

			update: ctx => {
				let x = ctx.params
				x.updatedAt = dayjs().toDate();
				x.updatedBy = ctx.meta.user._id
			},

		},   
		after: {
			
			create: [
			  async (ctx, result) => {

				result.resultBool= true
				result.messageId = "0027";
				return result;
			  },
			],
		  },
	}

};
