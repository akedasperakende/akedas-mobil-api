"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");

module.exports = {
  name: "home",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "fatura",
  actions: {
    async home(ctx) {

      let titleName = "Merhaba " + ctx.meta.user.name + " " + ctx.meta.user.lastname;
      let banners = await this.broker.call("banners.find",{
        query:{
          active:true
        },
        sort:'order'
      });
      let findSubscriber; 
      if (ctx.meta.user.vergiNo) {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
          fields: ["SOZLESME_HESAP_NO", "MAIL", "KESIK_DURUM"],
        });
      } else {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },

          fields: ["SOZLESME_HESAP_NO", "MAIL", "KESIK_DURUM"],
        });
      }
      let infoMessage="Kağıt Fatura yerine E-Arşiv Fatura'ya geçmek ister misiniz?";
      let isEInvoice = false;
      let invoice=[];
      if (findSubscriber) {
        let debit = await this.broker.call("debit.debitFind", {
            SOZLESME_HESAP_NO:  findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
        });
       let debtFind;
       let findInstallment=false; 
       let invoiceBills;
       let installmentBill;
       let hire_purchase=[];
       let sub_array;
       if(debit.length>0){
          await Promise.all(
            debit.map(async (x)=>{
            if(x.FATURA_TIPI=="Taksit Planı"){
              [sub_array]=await this.broker.call('installment.find',{
                  query:{
                    $and:[
                      {BELGE_NO:x.FATURA_NO},
                      {TAKSIT_SIRA:x.TAKSIT_SIRA}
                    ]
                  }
                });
                if(sub_array){
                hire_purchase.push(sub_array);                
                }
                findInstallment=true;
            }
          }));
          if(hire_purchase.length>0){
            hire_purchase.sort(function(a, b) {
              return a.TAKSIT_SIRA - b.TAKSIT_SIRA;
            }); 
          }
          if(findInstallment==true){
            installmentBill= await this.adapter.db
                                                .collection("fatura")
                                                .distinct("TAKSIT_BELGE_NO", {
                                                  TAKSIT_BELGE_NO:{$in: debit.map((x) => x.FATURA_NO)},
                                                });
                                                invoiceBills=await this.broker.call("bill.find",{query:{
                                                  TAKSIT_BELGE_NO:{
                                                    $in:installmentBill.map((x)=>x)
                                                }
                                                }})
          }
          
          invoice=await this.broker.call("bill.find",{query:{ FATURA_SERI:{$in:debit.map((x)=>x.FATURA_NO)}},sort: '-FATURA_THK_BAS'}) 
        }   
       
        await findSubscriber.map((x) => {
          x.TOTALDEBIT = 0;
          x.MESSAGE = "Borcunuz Bulunmamaktadır.";
          x.LASTBILLID=null;
          x.DEBITLIST=[];
          if (!isEInvoice) {
            isEInvoice = x.MAIL=="null" || x.MAIL==null ? false : true;
          }
          if(invoice.length>0){
              invoice.map((t)=>{
                if(t.FATURA_TIPI=="Taksit Planı"){
                  x.LASTBILLID=t._id;
                }
              })
             
         }
          return x;
        });
        if(isEInvoice==true){
         infoMessage="Kayıtlı e-posta adresiniz tanımlıdır, bildirdiğiniz için teşekkür ederiz. E-posta adresinizi profilden güncelleyebilirsiniz.";
        }

        findSubscriber.map((f, i) => {
          if (f.KESIK_DURUM === "Kesik") {
            findSubscriber[i].MESSAGE = "Hizmet kesildi";
            findSubscriber[i].DEBITSTATUS = "cleanClose";
          } else {
            findSubscriber[i].DEBITSTATUS = "cleanContinue";
          }
        });
        
        if (debit.length>0) {
          let totalDebit = await this.broker.call("debit.totalDebit", {
            SOZLESME_HESAP_NO: findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
          });
          let subJson;
          let installmentBill;
          let installment;
          let notInstallment;
            let unpaid;
          await Promise.all(totalDebit.map(async (e) => {
            await Promise.all(findSubscriber.map(async (f, i) => {
              let array=[];
              if (f.SOZLESME_HESAP_NO === e._id) {
                findSubscriber[i].TOTALDEBIT = e.TUTAR;
               
                if (f.KESIK_DURUM === "Kesik") {
                  findSubscriber[i].MESSAGE = "Hizmet kesildi";
                  findSubscriber[i].DEBITSTATUS = "debtClose";
                } else {
                  if(e.TUTAR>0){
                    findSubscriber[i].MESSAGE = "Borcunuz Bulunmaktadır.";
                    findSubscriber[i].DEBITSTATUS = "debtContinue";
                  }
                }

                debtFind=debit.filter(y => y.SOZLESME_HESAP_NO == f.SOZLESME_HESAP_NO)

                installment=debtFind.filter(y=>y.FATURA_NO.startsWith("270")==true)
                notInstallment=invoice.filter(y=>y.FATURA_SERI.startsWith("270")==false) 
                unpaid=debtFind.filter(y=>y.FATURA_NO.startsWith("270")!=true)

                unpaid = unpaid.filter(function(o1){
                        return !notInstallment.some(function(o2){    //  for diffrent we use NOT (!) befor obj2 here
                           return o1.FATURA_NO == o2.FATURA_SERI;          // id is unnique both array object
                         });
                       });
                 if(installment.length>0){
                  await Promise.all(invoiceBills.map(async (y)=>{
                    installmentBill=hire_purchase.filter(z => z.BELGE_NO == y.TAKSIT_BELGE_NO)
                    installmentBill=installmentBill.sort(function(a, b) {
                      return a.TAKSIT_SIRA - b.TAKSIT_SIRA;
                    });
                    
                    subJson={
                        _id:y._id,
                        SUBIDS:installmentBill
                      } 
                      array.push(subJson)
                  }))
                }                   
                if(notInstallment.length>0){
                    notInstallment=notInstallment.filter(y => y.SOZLESME_HESAP_NO == f.SOZLESME_HESAP_NO)
                    await Promise.all(notInstallment.map(async (x)=>{ 
                      if(f.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO ){
                        subJson={
                          _id:x._id,
                          SUBIDS:[]
                        }
                        array.push(subJson)
                      }
                  }))
                }
                if(unpaid.length>0){
                 let t=unpaid.filter(y => y.SOZLESME_HESAP_NO == f.SOZLESME_HESAP_NO)
                  await Promise.all(t.map(async (x)=>{ 
                    if(f.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO ){
                      subJson={
                        _id:x._id,
                        SUBIDS:[]
                      }
                      array.push(subJson)
                    }
                }))
              }
                findSubscriber[i].DEBITLIST=array

              }
            }));
          }));
          debit.map((x)=>{
            delete x.TAKSIT_SIRA,
            delete x.PARA_BIRIMI,
            delete x.TUTAR,
            delete x.SOYAD,
            delete x.AD,
            delete x.FATURA_TARIHI,
            delete x.SON_ODEME;
            delete x.SOZLESME_HESAP_NO;
            delete x.FATURA_NO;
            delete x.FATURA_TIPI;
          })
        }
        if(hire_purchase.length>0){
          hire_purchase.map((x)=>{
            delete x.BELGE_NO,
            delete x.TAKSIT_SIRA,
            delete x.SON_ODEME,
            delete x.ODEME_DURUM,
            delete x.SOZLESME_HESAP_NO,
            delete x.TUTAR
          })
        }
        findSubscriber.map((x) => {
          delete x.MAIL;
          delete x.KESIK_DURUM;
        });

        let home = {
          titleName: titleName,
          isEInvoice: isEInvoice,
          INFOMESSAGE:infoMessage,
          banners,
          findSubscriber
        };
        return home;
      }
    },
    async findUser(ctx) {
      let subscriber = await this.broker.call("subscriber.find", {
        query: {
          $or: [
            { TC_NUMARASI: ctx.params.tcNo },
            { VERGI_NO: ctx.params.vknno },
          ],
        },
      });
      return subscriber.map((x) => ({
        SOZLESME_HESAP_NO: x.SOZLESME_HESAP_NO,
        IL: x.IL == "ADIYAMAN" ? "02" : "46",
        ILCE: x.ILCE,
        MAHALLE: x.MAHALLE,
      }));
    },
    async homeChart(ctx) {
      let subscriber;
      if (ctx.meta.user.vergiNo) {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        subscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },             
            ],
          },
        });
      }

    let findSubs=  await subscriber.find(item => {
        return item.SOZLESME_HESAP_NO == ctx.params.SOZLESME_HESAP_NO
      })
    if(!findSubs){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0001", lang: "TR" },
      });
      throw Error(resultMessage.message);
    }

      if (ctx.params.type == "province") {
        return this.comparisonProvinceData(ctx);
      } else if (ctx.params.type == "district") {
        return this.comparisonDistrictData(ctx);
      } else if (ctx.params.type == "neighbourhood") {
        return this.comparisonNeighbourhoodData(ctx);
      } else {
        return this.comparisonFirstProvinceData(ctx);
      }
    },
  },
  methods: {
    async comparisonFirstProvinceData(ctx) {
      let subscriber;
      let now = new Date();
      let month=now.getMonth();
      let monthNames=["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      if (ctx.meta.user.vergiNo) {
        [subscriber] = await this.broker.call("subscriber.find", {
          query: {
            VERGI_NO: ctx.meta.user.vergiNo,
          },
        });
      } else {
        [subscriber] = await this.broker.call("subscriber.find", {
          query: {
            TC_NUMARASI: ctx.meta.user.tcNo,
          },
        });
      }

      let location = [];
      location.push(subscriber.IL == "ADIYAMAN" ? "02" : "46");

      let [provinceComparison] = await this.adapter.db

        .collection("musteri_kwh_kiyas")

        .aggregate([
          { $unwind: "$il_kodu" },

          {
            $match: {
              il_kodu: {
                $in: location,
              },
            },
          },

          {
            $group: {
              _id: "$il_kodu",

              Ocak_ort_tuketim: { $avg: "$Ocak_ort_tuketim" },

              Subat_ort_tuketim: { $avg: "$Subat_ort_tuketim" },

              Mart_ort_tuketim: { $avg: "$Mart_ort_tuketim" },

              Nisan_ort_tuketim: { $avg: "$Nisan_ort_tuketim" },

              Mayis_ort_tuketim: { $avg: "$Mayis_ort_tuketim" },

              Haziran_ort_tuketim: { $avg: "$Haziran_ort_tuketim" },

              Temmuz_ort_tuketim: { $avg: "$Temmuz_ort_tuketim" },

              Agustos_ort_tuketim: { $avg: "$Agustos_ort_tuketim" },

              Eylul_ort_tuketim: { $avg: "$Eylul_ort_tuketim" },

              Ekim_ort_tuketim: { $avg: "$Ekim_ort_tuketim" },

              Kasim_ort_tuketim: { $avg: "$Kasim_ort_tuketim" },

              Aralik_ort_tuketim: { $avg: "$Aralik_ort_tuketim" },
            },
          },
        ])

        .toArray();

      let result;
      let monthOrtNames = [provinceComparison.Ocak_ort_tuketim, provinceComparison.Subat_ort_tuketim, provinceComparison.Mart_ort_tuketim, provinceComparison.Nisan_ort_tuketim, provinceComparison.Mayis_ort_tuketim,provinceComparison.Haziran_ort_tuketim,provinceComparison.Temmuz_ort_tuketim, provinceComparison.Agustos_ort_tuketim, provinceComparison.Eylul_ort_tuketim, provinceComparison.Ekim_ort_tuketim, provinceComparison.Kasim_ort_tuketim,provinceComparison.Aralik_ort_tuketim];
      let provinceComparisonUserData;
      [provinceComparisonUserData] = await this.broker.call(
        "comparisondata.find",
        {
          query: {
            SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          },

          fields: [
            "_id",

            "il_kodu",

            "ilce",

            "mahalle",

            "Ocak_mus_tuketim",

            "Subat_mus_tuketim",

            "Mart_mus_tuketim",

            "Nisan_mus_tuketim",

            "Mayis_mus_tuketim",

            "Haziran_mus_tuketim",

            "Temmuz_mus_tuketim",

            "Agustos_mus_tuketim",

            "Eylul_mus_tuketim",

            "Ekim_mus_tuketim",

            "Kasim_mus_tuketim",

            "Aralik_mus_tuketim",

            "SOZLESME_HESAP_NO",
          ],
        }
      );
      if (provinceComparisonUserData) {
        let monthMusNames = [provinceComparisonUserData.Ocak_mus_tuketim, provinceComparisonUserData.Subat_mus_tuketim, provinceComparisonUserData.Mart_mus_tuketim, provinceComparisonUserData.Nisan_mus_tuketim, provinceComparisonUserData.Mayis_mus_tuketim,provinceComparisonUserData.Haziran_mus_tuketim,provinceComparisonUserData.Temmuz_mus_tuketim, provinceComparisonUserData.Agustos_mus_tuketim, provinceComparisonUserData.Eylul_mus_tuketim, provinceComparisonUserData.Ekim_mus_tuketim, provinceComparisonUserData.Kasim_mus_tuketim,provinceComparisonUserData.Aralik_mus_tuketim];
        result = {
          SOZLESME_HESAP_NO: provinceComparisonUserData.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg: month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] : monthMusNames[Math.abs(month-6)] == null
                ? 0
                : month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] :  monthMusNames[Math.abs(month-6)]
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :  monthMusNames[Math.abs(month-5)] == null
                ? 0
                :month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :   monthMusNames[Math.abs(month-5)]
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg:month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)] == null
                ? 0
                :month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)]
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)] == null
                ? 0
                :month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)]
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] : monthMusNames[Math.abs(month-2)] == null
                ? 0
                :month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] :  monthMusNames[Math.abs(month-2)]
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] :  monthMusNames[Math.abs(month-1)] == null
                ? 0
                : month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] : monthMusNames[Math.abs(month-1)]
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: monthMusNames[Math.abs(month)] == null
                ? 0
                : monthMusNames[Math.abs(month)]
              }
            }
          ]
        };
      } else {
        result = {
          SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg:0
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:0
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg: 0
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:0
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:0
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:0
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: 0
              }
            }
          ]
        };
      }
      return result;
    },
    async comparisonDistrictData(ctx) {
      let subscriber;
      let now = new Date();
      let month=now.getMonth();
      let monthNames=["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      [subscriber] = await this.broker.call("subscriber.find", {
        query: {
          SOZLESME_HESAP_NO: ctx.params.SOZLESME_HESAP_NO,
        },
      });

      let [countyComparisonUserData] = await this.broker.call(
        "comparisondata.find",
        {
          query: {
            SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          },

          fields: [
            "_id",

            "il_kodu",

            "ilce",

            "mahalle",

            "Ocak_mus_tuketim",

            "Subat_mus_tuketim",

            "Mart_mus_tuketim",

            "Nisan_mus_tuketim",

            "Mayis_mus_tuketim",

            "Haziran_mus_tuketim",

            "Temmuz_mus_tuketim",

            "Agustos_mus_tuketim",

            "Eylul_mus_tuketim",

            "Ekim_mus_tuketim",

            "Kasim_mus_tuketim",

            "Aralik_mus_tuketim",

            "SOZLESME_HESAP_NO",
          ],
        }
      );

      let [countyComparison] = await this.adapter.db

        .collection("musteri_kwh_kiyas")

        .aggregate([
          { $unwind: "$ilce" },

          {
            $match: {
              ilce: {
                $eq: subscriber.ILCE,
              },
            },
          },

          {
            $group: {
              _id: "$ilce",

              Ocak_ort_tuketim: { $avg: "$Ocak_ort_tuketim" },

              Subat_ort_tuketim: { $avg: "$Subat_ort_tuketim" },

              Mart_ort_tuketim: { $avg: "$Mart_ort_tuketim" },

              Nisan_ort_tuketim: { $avg: "$Nisan_ort_tuketim" },

              Mayis_ort_tuketim: { $avg: "$Mayis_ort_tuketim" },

              Haziran_ort_tuketim: { $avg: "$Haziran_ort_tuketim" },

              Temmuz_ort_tuketim: { $avg: "$Temmuz_ort_tuketim" },

              Agustos_ort_tuketim: { $avg: "$Agustos_ort_tuketim" },

              Eylul_ort_tuketim: { $avg: "$Eylul_ort_tuketim" },

              Ekim_ort_tuketim: { $avg: "$Ekim_ort_tuketim" },

              Kasim_ort_tuketim: { $avg: "$Kasim_ort_tuketim" },

              Aralik_ort_tuketim: { $avg: "$Aralik_ort_tuketim" },
            },
          },
        ])
        .toArray();

      let result;
      let monthOrtNames = [countyComparison.Ocak_ort_tuketim, countyComparison.Subat_ort_tuketim,countyComparison.Mart_ort_tuketim, countyComparison.Nisan_ort_tuketim, countyComparison.Mayis_ort_tuketim,countyComparison.Haziran_ort_tuketim,countyComparison.Temmuz_ort_tuketim, countyComparison.Agustos_ort_tuketim, countyComparison.Eylul_ort_tuketim, countyComparison.Ekim_ort_tuketim, countyComparison.Kasim_ort_tuketim,countyComparison.Aralik_ort_tuketim];
      if (countyComparisonUserData) {
        let monthMusNames = [countyComparisonUserData.Ocak_mus_tuketim, countyComparisonUserData.Subat_mus_tuketim, countyComparisonUserData.Mart_mus_tuketim, countyComparisonUserData.Nisan_mus_tuketim, countyComparisonUserData.Mayis_mus_tuketim,countyComparisonUserData.Haziran_mus_tuketim,countyComparisonUserData.Temmuz_mus_tuketim, countyComparisonUserData.Agustos_mus_tuketim, countyComparisonUserData.Eylul_mus_tuketim, countyComparisonUserData.Ekim_mus_tuketim, countyComparisonUserData.Kasim_mus_tuketim,countyComparisonUserData.Aralik_mus_tuketim];
        result = {
          SOZLESME_HESAP_NO: countyComparisonUserData.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg: month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] : monthMusNames[Math.abs(month-6)] == null
                ? 0
                : month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] :  monthMusNames[Math.abs(month-6)]
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :  monthMusNames[Math.abs(month-5)] == null
                ? 0
                :month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :   monthMusNames[Math.abs(month-5)]
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg:month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)] == null
                ? 0
                :month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)]
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)] == null
                ? 0
                :month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)]
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] : monthMusNames[Math.abs(month-2)] == null
                ? 0
                :month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] :  monthMusNames[Math.abs(month-2)]
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] :  monthMusNames[Math.abs(month-1)] == null
                ? 0
                : month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] : monthMusNames[Math.abs(month-1)]
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: monthMusNames[Math.abs(month)] == null
                ? 0
                : monthMusNames[Math.abs(month)]
              }
            }
          ]
        };
      } else {
        result = {
          SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg:0
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:0
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg: 0
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:0
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:0
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:0
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: 0
              }
            }
          ]
         
        };
      }

      return result;
    },

    async comparisonProvinceData(ctx) {
      let now = new Date();
      let month=now.getMonth();
      let monthNames=["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      let subscriber;
      [subscriber] = await this.broker.call("subscriber.find", {
        query: {
          SOZLESME_HESAP_NO: ctx.params.SOZLESME_HESAP_NO,
        },
      });

      let provinceComparisonUserData;

      [provinceComparisonUserData] = await this.broker.call(
        "comparisondata.find",
        {
          query: {
            SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          },

          fields: [
            "_id",

            "il_kodu",

            "ilce",

            "mahalle",

            "Ocak_mus_tuketim",

            "Subat_mus_tuketim",

            "Mart_mus_tuketim",

            "Nisan_mus_tuketim",

            "Mayis_mus_tuketim",

            "Haziran_mus_tuketim",

            "Temmuz_mus_tuketim",

            "Agustos_mus_tuketim",

            "Eylul_mus_tuketim",

            "Ekim_mus_tuketim",

            "Kasim_mus_tuketim",

            "Aralik_mus_tuketim",

            "SOZLESME_HESAP_NO",
          ],
        }
      );
      let location = [];
      location.push(subscriber.IL == "ADIYAMAN" ? "02" : "46");

      let [provinceComparison] = await this.adapter.db

        .collection("musteri_kwh_kiyas")

        .aggregate([
          { $unwind: "$il_kodu" },

          {
            $match: {
              il_kodu: {
                $in: location,
              },
            },
          },

          {
            $group: {
              _id: "$il_kodu",

              Ocak_ort_tuketim: { $avg: "$Ocak_ort_tuketim" },

              Subat_ort_tuketim: { $avg: "$Subat_ort_tuketim" },

              Mart_ort_tuketim: { $avg: "$Mart_ort_tuketim" },

              Nisan_ort_tuketim: { $avg: "$Nisan_ort_tuketim" },

              Mayis_ort_tuketim: { $avg: "$Mayis_ort_tuketim" },

              Haziran_ort_tuketim: { $avg: "$Haziran_ort_tuketim" },

              Temmuz_ort_tuketim: { $avg: "$Temmuz_ort_tuketim" },

              Agustos_ort_tuketim: { $avg: "$Agustos_ort_tuketim" },

              Eylul_ort_tuketim: { $avg: "$Eylul_ort_tuketim" },

              Ekim_ort_tuketim: { $avg: "$Ekim_ort_tuketim" },

              Kasim_ort_tuketim: { $avg: "$Kasim_ort_tuketim" },

              Aralik_ort_tuketim: { $avg: "$Aralik_ort_tuketim" },
            },
          },
        ])

        .toArray();
        let monthOrtNames = [provinceComparison.Ocak_ort_tuketim, provinceComparison.Subat_ort_tuketim, provinceComparison.Mart_ort_tuketim, provinceComparison.Nisan_ort_tuketim, provinceComparison.Mayis_ort_tuketim,provinceComparison.Haziran_ort_tuketim,provinceComparison.Temmuz_ort_tuketim, provinceComparison.Agustos_ort_tuketim, provinceComparison.Eylul_ort_tuketim, provinceComparison.Ekim_ort_tuketim, provinceComparison.Kasim_ort_tuketim,provinceComparison.Aralik_ort_tuketim];
        let result;
      if (provinceComparisonUserData) {
        let monthMusNames = [provinceComparisonUserData.Ocak_mus_tuketim, provinceComparisonUserData.Subat_mus_tuketim, provinceComparisonUserData.Mart_mus_tuketim, provinceComparisonUserData.Nisan_mus_tuketim, provinceComparisonUserData.Mayis_mus_tuketim,provinceComparisonUserData.Haziran_mus_tuketim,provinceComparisonUserData.Temmuz_mus_tuketim, provinceComparisonUserData.Agustos_mus_tuketim, provinceComparisonUserData.Eylul_mus_tuketim, provinceComparisonUserData.Ekim_mus_tuketim, provinceComparisonUserData.Kasim_mus_tuketim,provinceComparisonUserData.Aralik_mus_tuketim];
        result = {
          SOZLESME_HESAP_NO: provinceComparisonUserData.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg: month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] : monthMusNames[Math.abs(month-6)] == null
                ? 0
                : month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] :  monthMusNames[Math.abs(month-6)]
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :  monthMusNames[Math.abs(month-5)] == null
                ? 0
                :month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :   monthMusNames[Math.abs(month-5)]
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg:month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)] == null
                ? 0
                :month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)]
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)] == null
                ? 0
                :month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)]
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] : monthMusNames[Math.abs(month-2)] == null
                ? 0
                :month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] :  monthMusNames[Math.abs(month-2)]
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] :  monthMusNames[Math.abs(month-1)] == null
                ? 0
                : month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] : monthMusNames[Math.abs(month-1)]
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: monthMusNames[Math.abs(month)] == null
                ? 0
                : monthMusNames[Math.abs(month)]
              }
            }
          ]
        };
      } else {
        result = {
          SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg:0
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:0
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg: 0
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:0
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:0
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:0
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: 0
              }
            }
          ]     
        };
      }
      return result;
    },

    async comparisonNeighbourhoodData(ctx) {
      let subscriber;
      let now = new Date();
      let month=now.getMonth();
      let monthNames=["Ocak", "Subat", "Mart", "Nisan", "Mayis","Haziran","Temmuz", "Agustos", "Eylul", "Ekim", "Kasim","Aralik"];
      [subscriber] = await this.broker.call("subscriber.find", {
        query: {
          SOZLESME_HESAP_NO: ctx.params.SOZLESME_HESAP_NO,
        },
      });

      let [comparisonNeighbourhood] = await this.broker.call(
        "comparisondata.find",
        {
          query: {
            SOZLESME_HESAP_NO: {
              $eq: subscriber.SOZLESME_HESAP_NO,
            },
          },
        }
      );
      let result;
      if (comparisonNeighbourhood) {
        let monthOrtNames = [comparisonNeighbourhood.Ocak_ort_tuketim, comparisonNeighbourhood.Subat_ort_tuketim, comparisonNeighbourhood.Mart_ort_tuketim, comparisonNeighbourhood.Nisan_ort_tuketim, comparisonNeighbourhood.Mayis_ort_tuketim,comparisonNeighbourhood.Haziran_ort_tuketim,comparisonNeighbourhood.Temmuz_ort_tuketim, comparisonNeighbourhood.Agustos_ort_tuketim, comparisonNeighbourhood.Eylul_ort_tuketim, comparisonNeighbourhood.Ekim_ort_tuketim, comparisonNeighbourhood.Kasim_ort_tuketim,comparisonNeighbourhood.Aralik_ort_tuketim];
        let monthMusNames = [comparisonNeighbourhood.Ocak_mus_tuketim, comparisonNeighbourhood.Subat_mus_tuketim, comparisonNeighbourhood.Mart_mus_tuketim, comparisonNeighbourhood.Nisan_mus_tuketim, comparisonNeighbourhood.Mayis_mus_tuketim,comparisonNeighbourhood.Haziran_mus_tuketim,comparisonNeighbourhood.Temmuz_mus_tuketim, comparisonNeighbourhood.Agustos_mus_tuketim, comparisonNeighbourhood.Eylul_mus_tuketim, comparisonNeighbourhood.Ekim_mus_tuketim, comparisonNeighbourhood.Kasim_mus_tuketim,comparisonNeighbourhood.Aralik_mus_tuketim];
        result = {
          SOZLESME_HESAP_NO: comparisonNeighbourhood.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg: month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] : monthMusNames[Math.abs(month-6)] == null
                ? 0
                : month-6<0 ? monthMusNames[monthMusNames.length-Math.abs(month-6)] :  monthMusNames[Math.abs(month-6)]
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :  monthMusNames[Math.abs(month-5)] == null
                ? 0
                :month-5<0 ? monthMusNames[monthMusNames.length-Math.abs(month-5)] :   monthMusNames[Math.abs(month-5)]
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg:month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)] == null
                ? 0
                :month-4<0 ? monthMusNames[monthMusNames.length-Math.abs(month-4)] : monthMusNames[Math.abs(month-4)]
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)] == null
                ? 0
                :month-3<0 ? monthMusNames[monthMusNames.length-Math.abs(month-3)] : monthMusNames[Math.abs(month-3)]
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] : monthMusNames[Math.abs(month-2)] == null
                ? 0
                :month-2<0 ? monthMusNames[monthMusNames.length-Math.abs(month-2)] :  monthMusNames[Math.abs(month-2)]
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] :  monthMusNames[Math.abs(month-1)] == null
                ? 0
                : month-1<0 ? monthMusNames[monthMusNames.length-Math.abs(month-1)] : monthMusNames[Math.abs(month-1)]
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: monthMusNames[Math.abs(month)] == null
                ? 0
                : monthMusNames[Math.abs(month)]
              }
            }
          ]
          
        };
      } else {
        let [comparisonUserNeighbourhood] = await this.broker.call(
          "comparisondata.find",
          {
            query: {
              mahalle: {
                $eq: subscriber.MAHALLE,
              },
            },
          }
        );
        let monthOrtNames = [comparisonUserNeighbourhood.Ocak_ort_tuketim, comparisonUserNeighbourhood.Subat_ort_tuketim, comparisonUserNeighbourhood.Mart_ort_tuketim, comparisonUserNeighbourhood.Nisan_ort_tuketim, comparisonUserNeighbourhood.Mayis_ort_tuketim,comparisonUserNeighbourhood.Haziran_ort_tuketim,comparisonUserNeighbourhood.Temmuz_ort_tuketim, comparisonUserNeighbourhood.Agustos_ort_tuketim, comparisonUserNeighbourhood.Eylul_ort_tuketim, comparisonUserNeighbourhood.Ekim_ort_tuketim, comparisonUserNeighbourhood.Kasim_ort_tuketim,comparisonUserNeighbourhood.Aralik_ort_tuketim];
        result = {
          SOZLESME_HESAP_NO: subscriber.SOZLESME_HESAP_NO,
          chartData:[
            {
              title:month-6<0 ? monthNames[monthNames.length-Math.abs(month-6)] : monthNames[Math.abs(month-6)],
              comparison:{
                generalAvg:month-6<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-6)] : monthOrtNames[Math.abs(month-6)],
                userAvg:0
              }
            }, {
              title:month-5<0 ? monthNames[monthNames.length-Math.abs(month-5)] : monthNames[Math.abs(month-5)],
              comparison:{
                generalAvg:month-5<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-5)] : monthOrtNames[Math.abs(month-5)],
                userAvg:0
              }
            },{
              title:month-4<0 ? monthNames[monthNames.length-Math.abs(month-4)] : monthNames[Math.abs(month-4)],
              comparison:{
                generalAvg:month-4<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-4)] : monthOrtNames[Math.abs(month-4)],
                userAvg: 0
              }
            }, {
              title:month-3<0 ? monthNames[monthNames.length-Math.abs(month-3)] : monthNames[Math.abs(month-3)],
              comparison:{
                generalAvg:month-3<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-3)] : monthOrtNames[Math.abs(month-3)],
                userAvg:0
              }
            },{
              title:month-2<0 ? monthNames[monthNames.length-Math.abs(month-2)] : monthNames[Math.abs(month-2)],
              comparison:{
                generalAvg:month-2<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-2)] : monthOrtNames[Math.abs(month-2)],
                userAvg:0
              }
            }, {
              title:month-1<0 ? monthNames[monthNames.length-Math.abs(month-1)] : monthNames[Math.abs(month-1)],
              comparison:{
                generalAvg:month-1<0 ? monthOrtNames[monthOrtNames.length-Math.abs(month-1)] : monthOrtNames[Math.abs(month-1)],
                userAvg:0
              }
            },{
              title:monthNames[Math.abs(month)],
              comparison:{
                generalAvg:monthOrtNames[Math.abs(month)],
                userAvg: 0
              }
            }
          ]        
        };
      }

      return result;
    },
  },
};
