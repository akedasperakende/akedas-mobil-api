"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
const dayjs = require("dayjs");
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "paymentInfo",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "odemeRapor",
  settings: {
    maxPageSize: 1500000,
  },
  actions: {
    async getmove() {
      let dongu = await this.broker.call("paymentInfo.paymentInfoMssql");
      let pool = await sql.connect(config);
      const request = pool.request();
      let paymentInfoTable = await this.broker.call("paymentInfo.count");
      if (paymentInfoTable > 0 && dongu > 0) {
        await this.adapter.removeMany({});
        let paymentInfoTable = await this.broker.call("paymentInfo.count");
        let record; 
        if (paymentInfoTable == 0) {
          for (var i = 0; i < dongu; i++) {
            const partial_result = await request.query(
              "select * from odeme_rapor_6 ORDER BY SOZLESME_HESABI OFFSET " +
                i * 1500000 +
                " ROWS FETCH NEXT 1500000 ROWS ONLY;"
            );
            record = await partial_result.recordset.map((r) => {
               (r._id = nanoid(25)),
              (r.FATURA_NO = r.FATURA_NO== null ? null:String(r.FATURA_NO)),
              (r.SOZLESME_HESABI= r.SOZLESME_HESABI== null ? null: String(r.SOZLESME_HESABI)),
              (r.ODEME_TARIHI = r.ODEME_TARIHI== null ? null:dayjs(r.ODEME_TARIHI).toDate()),
              (r.ODENEN_TUTAR = r.ODENEN_TUTAR== null ? null:String(r.ODENEN_TUTAR)),
              (r.ODEME_KANALI = r.ODEME_KANALI== null ? null:String(r.ODEME_KANALI));
              return r;
            });
            await this.adapter.insertMany(record);
          }
          return true;
        }
      }

      if (paymentInfoTable == 0 && dongu > 0) {
        let record;
        for (var i = 0; i < dongu; i++) {
          const partial_result = await request.query(
            "select * from odeme_rapor_6 ORDER BY SOZLESME_HESABI OFFSET " +
              i * 1500000 +
              " ROWS FETCH NEXT 1500000 ROWS ONLY;"
          );
          record = await partial_result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.FATURA_NO = r.FATURA_NO== null ? null:String(r.FATURA_NO)),
            (r.SOZLESME_HESABI= r.SOZLESME_HESABI== null ? null: String(r.SOZLESME_HESABI)),
            (r.ODEME_TARIHI = r.ODEME_TARIHI== null ? null:dayjs(r.ODEME_TARIHI).toDate()),
            (r.ODENEN_TUTAR = r.ODENEN_TUTAR== null ? null:String(r.ODENEN_TUTAR)),
            (r.ODEME_KANALI = r.ODEME_KANALI== null ? null:String(r.ODEME_KANALI));
            return r;
          });
          await this.adapter.insertMany(record);
        }
        return true;
      }
    },
    async changePaymentInfo(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();

      let today="odeme_rapor_"+ctx.params.today
      let yesterday="odeme_rapor_"+ctx.params.yesterday
      let month=ctx.params.month
           const partial_result = await request.query(
       " select * from "+today+"  where [AY] != "+month+" except  select * from "+yesterday+"  where [AY] != "+month);

       
      record = await partial_result.recordset.map((r) => {
        (r._id = nanoid(25)),
        (r.SOZLESME_HESAP_NO= r.SOZLESME_HESAP_NO== null ? null: String(r.SOZLESME_HESAP_NO)),
        (r.TUTAR = r.TUTAR== null ? null:String(r.TUTAR)),
        (r.BELGE_NO = r.BELGE_NO== null ? null:String(r.BELGE_NO)),
        (r.SON_ODEME = r.SON_ODEME== null ? null:dayjs(r.SON_ODEME).toDate()),
        (r.ODEME_DURUM = r.ODEME_DURUM== null ? null:String(r.ODEME_DURUM)),
        (r.TAKSIT_SIRA = r.TAKSIT_SIRA== null ? null:String(r.TAKSIT_SIRA));
        return r;
      });
      let addData= await this.adapter.insertMany(record);
      return addData.length
    },
    async deletePaymentInfo(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();
      let today="odeme_rapor_"+ctx.params.today
      let yesterday="odeme_rapor_"+ctx.params.yesterday
      let month=ctx.params.month
      const partial_result = await request.query(
       " select * from "+today+"  where [AY] != "+month+" except  select * from "+yesterday+"  where [AY] != "+month);

      let rem = await this.adapter.removeMany({
        SOZLESME_HESAP_NO: {
           $in: partial_result.recordset.map((x) => x.SOZLESME_HESAP_NO)
      }
      });

      return rem
    },
  async paymentInfoMssql() {
      let pool = await sql.connect(config);
      const request = pool.request();
      const result = await request.query(
        "select count(*) as boyut from odeme_rapor_6;"
      );
      let dongu = Math.ceil(result.recordset[0].boyut / 1500000);
      return dongu;
    },
  },
};
