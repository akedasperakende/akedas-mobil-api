'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');

module.exports = {
	name: 'surveyAnswers',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'surveyAnswers',


	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			survey_id:{type: 'string'},
			registered_user_id: 'string',
			created: 'date',
			completed: 'boolean',
			questions: {
				'type': 'array',
				items: {
					type: 'object',
					props: {
						type: {type: 'string', enum: ['S','F','M']},
						name: 'string',
						is_answered: 'boolean',
						other: {type: 'string', optional: true},
						choices: {
							type: 'array',
							items: {
								type: 'object',
								props: {
									name: 'string',
									is_other: 'boolean',
									other: 'string',
									is_selected: 'boolean'
								}
							}
						}
					}
				}
			}
		}	
	},

	// actions: {
	// 	answerQuestion: {
	// 		handler: async (ctx) => {
	// 			let { survey_id, question_id, answers, other} = ctx.params;
	// 			let user_id = ctx.meta.user._id;

	// 			if (question_id && user_id && survey_id) {
	// 				question_id = Number(question_id);
	// 				let filledSurvey = await this.get(survey_id);

	// 				let question = filledSurvey.questions[question_id];
					
	// 				question.answers = question.answers || [];
	// 				question.is_answered = 1;
					
	// 				if(question.type === 'F'){
	// 					question.freetext = other;
	// 					question.is_selected = 1;
	// 				} 
	// 				else {
	// 					question.answers = answers.map(x => {
	// 						let q = question.choices[Number(x)];
	// 						q.is_selected = 1;
	// 						if(q.is_other == 1)
	// 							q.freetext = other;
							
	// 						return 1;
	// 					}).filter(x => x != null);
	// 					question.other = other;
	// 				}
			
	// 				let completed = true;
	// 				filledSurvey.questions.forEach(x => {
	// 					completed = completed && x.is_answered;
	// 				});
			
	// 				await this.adapter.updateById(survey_id, {$set: filledSurvey});
			
	// 				return true;
	// 			}else {
	// 				return new Error('parametreler eksik');
	// 			}
	// 		}
	// 	} 
	// },

	hooks: {
		before: {
			create: [async (ctx) => {
				let x = ctx.params
				let answer = await ctx.broker.call('surveyAnswers.find', {
					query: {
						survey_id: x.survey_id,
						registered_user_id: x.registered_user_id
					 }
				});
				if(answer.length>0){
					   let [resultMessage] = await ctx.broker.call("resultMessage.find", {
							query: { code: "0002", lang: "TR" },
						});
						throw Error(resultMessage.message);
				}
				x.created = new Date();
				let questionList = await x.questions.map(q => {
					return { ...q, is_answered: true }

				})
				x.questions = questionList
				x._id = nanoid(25);
			}]
		},
		after: {
			create: async(ctx, result) => {
				result.messageId = "0032";
				result.resultBool=true
				return result;
			}
		}

	}
};