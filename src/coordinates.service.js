"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
const nanoid = require("nanoid");
const axios = require("axios");

module.exports = {
  name: "coordinates",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "coordinates",

  actions: {
    async find(ctx) {
      let area=["kahramanmaras","elbistan","adiyaman"]
      let qsmart=[]
      await Promise.all(
        area.map(async (x) =>{
          let datas=  await axios.post('http://qsmart.akedasperakende.local/'+x+'/web_service/report_live_clients_rest.php',  { }, {
                headers: {
                  'Authorization': 'Basic cXNtYXJ0OnFzbWFydA=='
                }
              }); 
              qsmart=qsmart.concat(datas.data.Clients)
       })
     );

      let ids = ctx.params.ids;

      let coordinates;
      if (ids) {
        coordinates = await this.adapter.find({
          query: { _id: { $in: ids.map((x) => x._id) } },
        });
      } else {
        coordinates = await this.adapter.find({});
      }

      coordinates.map(async (coordinate) =>{
     //  let location= qsmart.find(x => x.title == coordinate.name)
       qsmart.map((x)=>{
        if(coordinate.urlCode){

          coordinate.wait=x.wait
          coordinate.ticket=x.ticket
          coordinate.url="https://randevu.akedas.com.tr/"+coordinate.urlCode+"/randevu.php?lang=TR"
         }
         else{
          coordinate.wait=0
          coordinate.ticket=0
          coordinate.url="https://www.akedas.com.tr/randevu"
         }
       })
      })
      return coordinates;
    },
  },
  hooks: {
    before: {
      create: (ctx) => {
        let c = ctx.params;
        c._id = nanoid(16);
        c.code = c._id;
        c.id = c._id;

        if (c.coordinate) {
          c.address = c.coordinate.address;
          if (c.coordinate.position) {
            c.lat = c.coordinate.position.lat.toString();
            c.lon = c.coordinate.position.lng.toString();
          }
        }
        c.content = `<p>Adres: ${c.address || ""}<p/><br><a href="tel:${
          c.phone
        }">Tel: ${c.phone}</>`;

        c.color = "#1e90ff";
      },
      update: (ctx) => {
        let c = ctx.params;
        if (c.coordinate) {
          c.address = c.coordinate.address;
          if (c.coordinate.position) {
            c.lat = c.coordinate.position.lat.toString();
            c.lon = c.coordinate.position.lng.toString();
          }
        }
        c.color = "#1e90ff";
        c.content = `<p style="white-space: pre-wrap">Adres: ${
          c.address || ""
        }<p/><br><a href="tel:${c.phone}">Tel: ${c.phone}</>`;
      },
    },
    after: {
      list: (ctx, result) => {
        result.rows.forEach((c) => {
          if (!c.coordinate) {
            c.locality = c.locality || "";
            c.coordinate = {
              address: c.address,
              position: {
                lat: Number(c.lat),
                lng: Number(c.lon),
              },
            };
          }
        });

        return result;
      },
    },
  },
};
