'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');


module.exports = {
	name: 'cronLog',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'cronLog',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			date: {type: 'date', optional: true},
			table:{type: 'string', optional: true },
			action:{type: 'string', optional: true },
			count: {type: 'number', optional: true },
		}
	},

	hooks: {
		before: {
			create: [(ctx) => {
			
				ctx.params._id = nanoid(25);
				ctx.params.date = new Date();
	
			}],
		}
	}
};