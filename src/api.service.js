"use strict";
const path = require("path");
const jwt = require("jsonwebtoken");
const MongoClient = require("mongodb").MongoClient;
const fastifyPlugin = require("fastify-plugin");
let qs = require("qs");
const http = require('http');
const dev = process.env.NODE_ENV !== "production";
let cmsIPS = process.env.IPS ? process.env.IPS.split(",") : [];
const helmet = require("fastify-helmet");
module.exports = {
  name: "api",

  settings: {
    port: process.env.PORT || 5000,
  },

  created() {
    try {
      const uri = process.env.MONGO_URL || "mongodb://localhost:27017/admin";
      const client = new MongoClient(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
      let db = null;
      client.connect().then(() => {
        db = client.db(process.env.DB);
      });

      let fastify = require("fastify")({
        pluginTimeout: process.env.NODE_ENV === "development" ? 200000 : 200000,
        caseSensitive: false,
        trustProxy: true,
        logger: false,
      });
      this.fastify = fastify;

      fastify.register(helmet, { hidePoweredBy: { setTo: "PHP 4.2.0" } });
      fastify.register(require("fastify-formbody"));
      fastify.register(
        fastifyPlugin(async (fastify) => {
          let mongo = await MongoClient.connect(process.env.MONGO_URL, {
            useUnifiedTopology: true,
          });
          let db = mongo.db(process.env.DB);
          fastify.decorate("db", db);
        })
      );

      fastify.register(require("fastify-cors"), {
        origin: true,
        methods: "GET,PUT,POST,PATCH,DELETE",
        preflight: true,
      });

      fastify.register(require("fastify-multipart"), {
        addToBody: true,
        limits: {
          fieldNameSize: 100, // Max field name size in bytes
          fieldSize: 53000000, // Max field value size in bytes
          fields: 20, // Max number of non-file fields
          fileSize: 53000000, // For multipart forms, the max file size
          files: 10, // Max number of file fields
          headerPairs: 2000, // Max number of header key=>value pairs
        },
      });

      fastify.addHook("preHandler", async (req,reply) => {
        let token = null;
        try {
          if (
            process.env.BLOCK_IP === "block" &&
            (req.req.url.startsWith("/cms") ||
              req.req.url.startsWith("/rest")) &&
            req.ips &&
            req.ips.find((ip) => cmsIPS.find((_ip) => _ip === ip)) == null
          )
            throw Error("Forbidden");

          req.version =
            "v" + (req.headers["x-api-version"] || "0.0").split(".")[0];
          req.lang = req.headers["x-api-lang"]
            ? req.headers["x-api-lang"]
            : "TR";

          this.broker.call("requestLog.createLog", { request: req });



          if (req.req.url.startsWith("/api/Login")) return;
          if (req.req.url.startsWith("/api/replaceThumbs")) return;
          if (req.req.url.startsWith("/api/login")) return;
          if (req.req.url.startsWith("/api/register")) return;
          if (req.req.url.startsWith("/api/Register")) return;
          if (req.req.url.startsWith("/api/createPassword")) return;
          if (req.req.url.startsWith("/api/reSendCode")) return;
          if (req.req.url.startsWith("/api/settings")) return;
          if (req.req.url.startsWith("/api/Settings")) return;
          if (req.req.url.startsWith("/api/forgotPassword")) return;
          if (req.req.url.startsWith("/api/smsCheckPass")) return;
          if (req.req.url.startsWith("/api/subscriberGetmove")) return;
          if (req.req.url.startsWith("/api/pushNotifications")) return; /////??????
          if (req.req.url.startsWith("/api/deletingOldDataBill")) return;
       //   if (req.req.url.startsWith("/api/bill")) return;
          if (req.req.url.startsWith("/api/bill2")) return;
          if (req.req.url.startsWith("/api/deletebill")) return;
          if (req.req.url.startsWith("api/subscribers2")) return;
          if (req.req.url.startsWith("/api/deletesubscriber")) return;
          if (req.req.url.startsWith("/api/individualNotification")) return;
          if (req.req.url.startsWith("/api/debit")) return;
          if (req.req.url.startsWith("/api/installment")) return;
          if (req.req.url.startsWith("/api/paymentInfo")) return;
          if (req.req.url.startsWith("/api/productionQuantities")) return;
          if (req.req.url.startsWith("/api/comparisondata")) return;
          
          if (req.req.url.startsWith("/api/deletecomparisondata")) return;
          
          if (req.req.url.startsWith("/api/comparisondatachange")) return;
          if (req.req.url.startsWith("/api/production")) return;
          if (req.req.url.startsWith("/api/coordinates")) return;
          if (req.req.url.startsWith("/api/productionCentrals")) return;
          if (req.req.url.startsWith("/api/location")) return;
          if (req.req.url.startsWith("/api/mokaTahsilat")) return;
          if (req.req.url.startsWith("/api/octetTahsilat")) return;
          if (req.req.url.startsWith("/api/octet")) return;
          if (req.req.url.startsWith("/api/moka")) return;
          if (!(req.req.url.startsWith("/api") || req.req.url.startsWith("/rest"))  ) return;

            if (req.query && req.query.token !== null) token = req.query.token;
            if (req.body && req.body.token != null) {
              token = req.body.token;
              delete req.body.token;
            }
            if (token == null && req.headers["authorization"]) {
              token = req.headers["authorization"]
                .replace("bearer ", "")
                .replace("Bearer ", "");
            }
            if (token == null && req.headers["Authorization"]) {
              token = req.headers["Authorization"]
                .replace("bearer ", "")
                .replace("Bearer ", "");
            }
            let user = null;
            if (token) {

              user = jwt.verify(token, process.env.JWT_SECRET);
              let logins = await this.broker.call('login.find', {   query: {token:token }})
              if(logins.length<=0){
                reply.code(401).send({
                  result: null,
                  result_message: {
                    type: "error",
                    title: "Uyarı",
                    message: "Geçersiz oturum, lütfen tekrar giriş yapınız.",
                  },
                });
              }
              req.user = user;
            }
          if (token) {
            let _token = await this.broker.call("login.count", {
              query: { userId: user._id },
            });
            if (_token < 1) {
              let resultMessage = await this.broker.call("resultMessage.get", {
                id: "0020",
              });
              let err = new Error(resultMessage.message);
              err.statusCode = 401;
              err.tokentype = "token_expire";
              err.tokentitle = resultMessage.title;
              throw err;
            }
          }
          if (!token) {
            let resultMessage = await this.broker.call("resultMessage.get", {
              id: "0020",
            });
            let err = new Error(resultMessage.message);
            err.statusCode = 401;
            err.tokentype = "token_expire";
            err.tokentitle = resultMessage.title;
            throw err;
          }

          if (user == null || user._id == null) {
            let resultMessage = await this.broker.call("resultMessage.get", {
              id: "0020",
            });
            let err = new Error(resultMessage.message);
            err.statusCode = 401;
            err.tokentype = "token_expire";
            err.tokentitle = resultMessage.title;
            throw err;
          }

          let isExpire = await this.broker.call(
            "registeredUsers.isExpire",
            {},
            { meta: { user: user, token: token } }
          );
          if (isExpire) {
            let resultMessage = await this.broker.call("resultMessage.get", {
              id: "0014",
            });
            let err = new Error(resultMessage.message);
            err.statusCode = 401;
            err.tokentype = "token_expire";
            err.tokentitle = resultMessage.title;
            throw err;
          }
          if ( req.req.url.startsWith("/rest") &&!(user.role === "admin" || user.role === "health")) {
            let resultMessage = await this.broker.call("resultMessage.get", {
              id: "0014",
            });
            let err = new Error(resultMessage.message);
            err.statusCode = 401;
            err.tokentype = "token_expire";
            err.tokentitle = resultMessage.title;
            throw err;
          }
        } catch (err) { 
          this.logger.error(req.req.method);
          this.logger.error(req.req.url);
          this.logger.error(token);
          this.logger.error(req.body);
          this.logger.error(err);
          
          throw err;
        }
      });

      fastify.addHook("preSerialization", async (req, res, payload) => {
        let resultBool = payload.resultBool;
        if (payload && payload.result_message) {
          return payload;
        } else if (payload && payload.messageId) {
          let resultMessage;
          [resultMessage] = await this.broker.call("resultMessage.find", {
            query: { code: payload.messageId, lang: req.lang },
            fields: ["type", "title", "message"],
          });
          delete payload.messageId;
          delete payload.resultBool;
          return {
            result: resultBool ? resultBool : payload,
            result_message: resultMessage,
          };
        } else {
          return {
            result: payload,
            result_message: {
              type: "success",
              title: "Bilgi",
              message: "Başarılı",
            },
          };
        }
      });

      fastify.setErrorHandler((error, request, reply) => {
        this.logger.error(error);
        if (error == "TokenExpiredError: jwt expired")
          reply.code(401).send({
            result: null,
            result_message: {
              type: "error",
              title: "Uyarı",
              message: "Oturumunuz sonlandırılmıştır, lütfen tekrar giriş yapınız.",
            },
          });
        else
          reply.code(200).send({
            result: null,
            result_message: {
              type: "error",
              title: "Uyarı",
              message:
                error.data && Array.isArray(error.data)
                  ? error.data.reduce(
                      (m, x) => m + " " + x.message,
                      error.message
                    )
                  : error.message,
            },
          });
      });

      fastify.get("/api/replaceThumbs", async (req) => {
        return this.broker.call("posts.replaceThumbs", {}, {});
      });

      fastify.post("/api/login/refresh", async (req) =>
        this.broker.call("login.tokenRefresh", {
          ...req.body,
          lang: req.headers["x-api-lang"] || req.query.lang || "TR",
        })
      );

      fastify.post("/api/search", async (req) =>
        this.broker.call("search.search", { text: req.body.text })
      );

      fastify.post("/api/userSearch", async (req) =>
        this.broker.call("registeredUsers.search", { text: req.body.text })
      );
      fastify.post("/api/chatUserSearch", async (req) =>
        this.broker.call("registeredUsers.chatUserSearch", {
          text: req.body.text,
        })
      );
      fastify.post("/api/directoryUserSearch", async (req) =>
        this.broker.call("registeredUsers.directoryUserSearch", {
          text: req.body.text,
        })
      );

      fastify.post("/api/login", async (req) =>
        this.broker.call("login.token", {
          ...req.body,
          lang: req.headers["x-api-lang"] || req.query.lang || "TR",
        })
      );

      fastify.post("/api/register", async (req) =>
        this.broker.call("registeredUsers.register", {
          ...req.body,
          lang: req.headers["x-api-lang"] || req.query.lang || "TR",
        })
      );
      fastify.post("/api/bill/subscribersearch", async (req) =>
        this.broker.call("bill.subscribersearch", {
          ...req.body,
        })
      );
      fastify.post("/api/billFind", async (req) =>
        this.broker.call(
          "bill.billFind",
          { ...req.body },
          { meta: { user: req.user } }
        )
      );
      fastify.post("/api/totalBills", async (req) =>
        this.broker.call(
          "bill.totalBills",
          { ...req.body },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/bills", async (req) =>{
      return  this.broker.call(
          "bill.bills",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
      )
      });

      fastify.post("/api/payment", async (req) =>
        this.broker.call(
          "payment.payment",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/debitFind/:id", async (req) => {
        return this.broker.call("debit.debitFind", {
          SOZLESME_HESAP_NO: req.params.id,
        });
      });
      fastify.get("/api/viewBill/:id", async (req) => {
        return this.broker.call("bill.viewBill", { euuId: req.params.id });
      });
      fastify.get("/api/home", async (req) =>{
        return this.broker.call(
          "home.home",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )}
      );
      fastify.post("/api/totalDebit", async (req) =>
        this.broker.call("debit.totalDebit", {
          ...req.body,
        })
      );
      fastify.get("/api/listSubscriber", async (req) =>
        this.broker.call(
          "subscriber.listSubscriber",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/boardApprovedListSubscriber", async (req) =>
        this.broker.call(
          "subscriber.boardApprovedListSubscriber",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/binaryDeals", async (req) =>
        this.broker.call(
          "subscriber.binaryDeals",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/paidBills", async (req) =>
        this.broker.call(
          "bill.paidBills",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.post("/api/createPassword", async (req) =>
        this.broker.call(
          "registeredUsers.createPassword",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );

      fastify.post("/api/billDetail", async (req) => {
        return this.broker.call(
          "bill.billDetail",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        );
      });
      fastify.post("/api/comparisonBills", async (req) => {

        return this.broker.call(
          "bill.comparisonBills",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        );
      });

      fastify.post("/api/updatePassword", async (req) =>
        this.broker.call(
          "registeredUsers.updatePassword",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.post("/api/reSendCode", async (req) =>
        this.broker.call(
          "registeredUsers.reSendCode",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );
      fastify.post("/api/homeChart", async (req) =>
        this.broker.call(
          "home.homeChart",
          { ...req.body, lang: req.lang },
          { meta: { user: req.user } }
        )
      );

      fastify.post("/api/forgotPassword", async (req) =>
        this.broker.call("registeredUsers.forgotPassword", {
          ...req.body,
          lang: req.headers["x-api-lang"] || req.query.lang || "TR",
        })
      );
      fastify.post("/api/pushNotifications/locationNotification", async (req) =>
        this.broker.call("pushNotifications.locationNotification", {
          ...req.body,
        })
      );
      fastify.post("/api/logout", async (req) =>
        this.broker.call("login.expire", {}, { meta: { user: req.user } })
      );

      fastify.get("/api/settings", async (req) => {
        return this.broker.call(req.version + ".settings.flat", {
          lang: req.lang,
          userId: req.user ? req.user._id : null,
          deviceOs: req.headers["x-device-os"] || "old",
          manufacturer: req.headers["x-device-manufacturer"],
        });
      });

      /*fastify.post(
        "/api/forgotPassword",
        async (req) =>
          await this.broker.call("registeredUsers.forgotPassword", {
            ...req.body,
            lang: req.lang,
          })
      );*/

      fastify.post("/api/smsCheckPass", async (req) =>
        this.broker.call(
          "login.smsCheckPassword",
          { ...req.body },
          { meta: { user: req.user } }
        )
      );
      fastify.get("/api/registeredUsers", async () =>
        this.broker.call("registeredUsers.find", { page: 1, pageSize: 20 })
      );
      fastify.get("/api/registeredUsers/:id", async (req) =>
        this.broker.call("registeredUsers.get", {
          id: req.params.id || req.user._id,
        })
      );

      fastify.get("/api/me", async (req) => {
        let me = await this.broker.call(
          "registeredUsers.get",
          {
            id: req.user._id,
            playerId: req.headers["x-player-id"] || req.query.player_id,
            lang: req.headers["x-api-lang"] || req.query.lang || "TR",
          },
          { meta: { user: req.user } }
        );
        me.isCorporate?me.type="Kurumsal Abonelik":me.type="Bireysel Abonelik"
        return me;
      });

      fastify.post("/api/setPlayerId", async (req) => {
        this.broker.call(
          "registeredUsers.setPlayerId",
          {
            id: req.user._id,
            playerId: req.body.player_id || req.headers["x-player-id"],
            lang: req.headers["x-api-lang"] || req.query.lang || "TR",
          },
          { meta: { user: req.user } }
        );
        return true;
      });

      fastify.get("/api/usersInformation", async (req) => {
        return this.broker.call(
          "registeredUsers.informations",
          { id: req.query.id || req.user._id },
          { meta: { user: req.user } }
        );
      });

      fastify.post("/api/me", async (req) => {
        let me= await this.broker.call(
          "registeredUsers.update",
          { ...req.body, _id: req.user._id, lang: req.headers["x-api-lang"] },
          { meta: { user: req.user } }
        );
        me.isCorporate?me.type="Kurumsal Abonelik":me.type="Bireysel Abonelik"
        return me
      });

      fastify.get("/api/me/surveys/:id", async (req) =>
        this.broker.call("educationSurveys.get", {
          id: req.params.id,
          completed: false,
        })
      );
      fastify.post("/api/me/surveys/:id", async (req) => {
        await this.broker.call("educationSurveys.update", {
          _id: req.params.id,
          completed: true,
          questions: req.body.questions,
        });
        return true;
      });

      fastify.get("/api/coordinates", async (req) => {
        let search = req.query.search;

        if (search && search.length > 0) {
          let ids = await this.broker.call("search.coordinateSearch", {
            text: search,
          });
          let result = await this.broker.call("coordinates.find", {
            ids,
            type: req.query.types,
          });
          return result.slice(0, 2000000);
        } else {
          return this.broker.call("coordinates.find", {
            type: req.query.types,
          });
        }
      });

      fastify.get(
        "/api/notifications_count",
        async (req) =>
          this.broker.call("notifications.notificationCount", {
            _id: req.user._id,
            isRead: true,
            lang: req.lang,
            groupQwry: req.user.groupQwry,
          })
        //this.broker.adapter.db.collection("notifications").find({userId: req.user._id, isRead:false})
      );

      fastify.post("/api/notifications/itemsRemove", async (req) => {
        await this.broker.call(
          "notifications.itemsRemove",
          { item_id: req.body.id },
          { meta: { user: req.user } }
        );
        return true;
      });

      fastify.post("/api/notifications/delete", async (req) => {
        let lang = req.lang;
        return await this.broker.call(
          "notifications.delete",
          { ids: req.body.ids, lang: lang },
          { meta: { user: req.user } }
        );
      });

      fastify.get(
        "/api/notifications",
        async (req) =>
          await this.broker.call("notifications.find", {
            sort: "-date",
            query: {
              lang: req.lang,
              active: true,
              $or: [
                { registeredUserId: req.user._id },
                { registeredUserId: "" },
              ],
            },
            fields: [
              "_id",
              "isRead",
              "registeredUserId",
              "title",
              "content",
              "date",
              "icon",
            ],
          })
      );

      fastify.post("/api/read_notifications", async (req) => {
        let registeredUserId = req.user._id;
        await this.broker.call("notifications.readNotification", {
          _id: req.body.id,
          isRead: true,
          registeredUserId: registeredUserId,
        });
        return true;
      });

      fastify.get(
        "/api/surveys",
        async (req) =>
          await this.broker.call("surveys.byUser", {
            user: req.user,
            lang: req.lang,
          })
      );

      fastify.get(
        "/api/surveys/:id",
        async (req) =>
          await this.broker.call("surveys.byUserId", {
            id: req.params.id,
            user: req.user,
          })
      );

      fastify.post("/api/surveys/:id", async (req) => {
        if (req.body.questions == null) {
          let resultMessage = await this.broker.call("resultMessage.get", {
            id: "0021",
          });
          throw new Error(resultMessage.message);
          //throw Error('questions null olamaz');
        }
        return await this.broker.call("surveyAnswers.create", {
          survey_id: req.params.id,
          registered_user_id: req.user._id,
          completed: true,
          questions: req.body.questions,
        });
      });

      fastify.get("/api/bill", async (req) =>
        this.broker.call("bill.getmove", { ...req.body })
      );
      fastify.get("/api/productionCentrals", async (req) =>
        this.broker.call("productionCentrals.getmove", { ...req.body })
      );
      fastify.get("/api/production", async (req) =>
        this.broker.call("productionQuantities.quantities", { ...req.body })
      );
      fastify.get("/api/production2", async (req) =>
      this.broker.call("productionQuantitiesTotal.quantities", { ...req.body })
      );
      fastify.get("/api/debit", async (req) =>
        this.broker.call("debit.getmove", { ...req.body })
      );
      fastify.get("/api/insertBills", async (req) =>
        this.broker.call("bill.insertBills", { ...req.body })
      );
      fastify.get("/api/bill2", async (req) =>
        this.broker.call("cron.bill", { ...req.body })
      );
      fastify.get("/api/deletebill", async (req) =>
        this.broker.call("cron.deletebill", { ...req.body })
      );
      fastify.get("/api/deletingOldDataBill", async (req) =>
        this.broker.call("cron.deletingOldDataBill", { ...req.body })
      );
      fastify.get("/api/subscribers2", async (req) =>
        this.broker.call("cron.subscriber", { ...req.body })
      );
      fastify.get("/api/deletesubscriber", async (req) =>
        this.broker.call("cron.deletesubscriber", { ...req.body })
      );
      fastify.get("/api/location", async (req) =>
        this.broker.call("cron.location", { ...req.body })
      );
      fastify.post("/api/octetTahsilat", async (req,res) => {
        try{
           const result= await this.broker.call("paymentLog.create", {
          ...req.body,
          ...qs.parse(req.query),
        });   
          res.setHeader('Location', 'https://vpos.eventapp.app/?status=success' )
          res.statusCode = 302
          res.end()
          return res

      //   if(req.body.resultCode!="0"){
      //     return reply.redirect("https://vpos.eventapp.app/?status=error")
      //   }
      //   else{
      //  return   reply.writeHead(301, { "Location": "https://vpos.eventapp.app/?status=success" })
      // //   return reply.redirect("https://vpos.eventapp.app/?status=success")
      //   }
        }
        catch(err){
          return res.redirect("https://vpos.eventapp.app/?status=success")
        }
     
      });
      fastify.post("/api/mokaTahsilat", async (req,reply) => {

        this.broker.call("paymentLog.create", {
          ...req.body,
          ...qs.parse(req.query),
        });
        //http://localhost:3000/?
        //https://vpos.eventapp.app/?
        if(req.body.trxCode){
           return reply.redirect("https://vpos.eventapp.app/?status=success")
        }
        else{
         return reply.redirect("https://vpos.eventapp.app/?status=error")
        }
      });
      fastify.get("/api/octet", async (req) => {
        let octet =  await this.broker.call("payment.octet", {
          price: req.query.price,
          user: req.query.user,
        })
        return octet;
      });
      fastify.get("/api/moka", async (req) => {
        let moka = await this.broker.call(
          "payment.moka",
          { ...req.body, price: req.query.price },
          { meta: { user: req.user } }
        );
        return moka;
      });
      fastify.get("/api/installment/billsInstallment", async (req) =>
        this.broker.call("installment.billsInstallment", { ...req.body })
      );
      fastify.get("/api/subscriberGetmove", async (req) =>
        this.broker.call("subscriber.getmove", { ...req.body })
      );
      fastify.get("/api/comparisondata", async (req) =>
        this.broker.call("comparisondata.getmove", { ...req.body })
      );
      fastify.get("/api/deletecomparisondata", async (req) =>
      this.broker.call("cron.deletecomparisondata", { ...req.body })
      );
      fastify.get("/api/comparisondatachange", async (req) =>
      this.broker.call("cron.comparisondata", { ...req.body })
      );
      fastify.get("/api/installment", async (req) =>
        this.broker.call("installment.getmove", { ...req.body })
      );
      fastify.get("/api/paymentInfo", async (req) =>
        this.broker.call("paymentInfo.getmove", { ...req.body })
      );
      // fastify.get("/api/productionQuantities", async (req) =>
      //   this.broker.call("productionQuantities.getmove", { ...req.body })
      // );
      fastify.get("/api/productionQuantities", async (req) =>
      this.broker.call("productionQuantitiesTotal.getmove", { ...req.body })
    );
      fastify.post("/api/bill/searchRegisteredUser/:tcNo", async (req) =>
        this.broker.call("bill.searchRegisteredUser", {
          tcNo: req.params.tcNo,
        })
      );
      fastify.post("/api/individualNotification", async (req) =>
        this.broker.call("pushNotifications.individualNotification", {
          SOZLESME_HESAP_NO: req.body.SOZLESME_HESAP_NO,
          title: req.body.title,
          message: req.body.message,
        })
      );
      fastify.post(
        "/api/requestAndComplaint",
        async (req) =>
          await this.broker.call(
            "requestAndComplaint.create",
            { registeredUserId: req.user._id, ...req.body },
            { meta: { user: req.user } }
          )
      );
      fastify.get(
        "/api/requestAndComplaint",
        async (req) =>
          await this.broker.call("requestAndComplaint.getCategory", {
            user: req.user,
            lang: req.lang,
          })
      );

      fastify.post("/api/createBillWillImages", async (req) => {
        return await this.broker.call(
          "bill.createBillWillImages",
          { ...req.body, _id: req.user._id, lang: req.headers["x-api-lang"] },
          { meta: { user: req.user } }
        );
      });
      
      fastify.post("/api/subscriberWithQr", async (req) => {
        return await this.broker.call(
          "subscriber.subscriberWithQr",
          { ...req.body, _id: req.user._id, lang: req.headers["x-api-lang"] },
          { meta: { user: req.user } }
        );
      });
      fastify.post("/api/searchSubscriber", async (req) => {
        let [subs] = await this.broker.call("subscriber.find", {
          query: { SOZLESME_HESAP_NO: req.body.SOZLESME_HESAP_NO },
        });

        if (subs) return true;
        else {
          let resultMessage = await this.broker.call("resultMessage.get", {
            id: "0026",
          });
          throw new Error(resultMessage.message);
        }
      });

      fastify.get("/api/cities", async () => {
        return this.broker.call("cities.find", {
          query: { _id: { $ne: "0" } },
          sort: "order,name",
        });
      });

      fastify.get("/rest/reports/:id", async (req) => {
        let { service, id } = req.params;
        //let params = qs.parse(req.query)
        return await this.broker.call(`reports.${id}`, req.query, {
          meta: { user: req.user },
        });
      });

      fastify.get("/rest/:service", async (req) => {
        let { service } = req.params;
        let params = qs.parse(req.query);
        return await this.broker.call(`${service}.list`, params, {
          meta: { user: req.user },
        });
      });

      fastify.get("/rest/:service/:id", async (req) => {
        let { service, id } = req.params;
        //let params = qs.parse(req.query)
        return await this.broker.call(
          `${service}.get`,
          { id: id },
          { meta: { user: req.user } }
        );
      });

      fastify.post("/rest/:service", async (req) => {
        let { service, action } = req.params;
        return await this.broker.call(`${service}.create`, req.body, {
          meta: { user: req.user },
        });
      });

      fastify.post("/rest/:service/:action", async (req) => {
        let { service, action } = req.params;
        return await this.broker.call(`${service}.${action}`, req.body, {
          meta: { user: req.user },
        });
      });

      fastify.put("/rest/:service/:id", async (req) => {
        let { service, action, id } = req.params;
        return await this.broker.call(
          `${service}.update`,
          { ...req.body },
          { meta: { user: req.user } }
        );
      });

      fastify.delete("/rest/:service/:id", async (req) => {
        let { service, id } = req.params;
        return await this.broker.call(`${service}.remove`, { id: id });
      });

      fastify.post("/api/upload", async (req) => {
        let files = [];

        if (req.body.files != null && req.body.files.length > 0) {
          files = req.body.files;
        }
        if (req.body.files_0 != null) {
          for (var i = 0; ; i++) {
            if (req.body[`files_${i}`] != null) {
              files.push(req.body[`files_${i}`][0]);
            } else {
              break;
            }
          }
        }


        let isOkMimtype=["image/avif", "image/bmp", "image/gif", "image/vnd.microsoft.icon","image/jpeg","image/png","image/svg+xml","image/tiff","image/webp"]
     
        let isOkMimtypes=files.map((x)=>{
          let a= isOkMimtype.some(b => x.mimetype === b)
          return a
        })
        
        let checker = arr => arr.every(v => v === true);
  
        if(!checker(isOkMimtypes)){
          let [resultMessage] = await this.broker.call("resultMessage.find", {
            query: { code: "0039", lang: "TR" },
          });
          throw Error(resultMessage.message);
        }
  
        return await this.broker.call("storage.save", { files });
      });

      //console.log(__dirname)

      fastify.register(require("fastify-static"), {
        root: path.join(__dirname, "../public/files"),
        prefix: "/files", // optional: default '/'
      });

      // fastify.register(require('fastify-static'), {
      // 	root: path.join(__dirname, '../public/images'),
      // 	prefix: '/images', // optional: default '/'
      // })
    } catch (err) {
      console.log(err);
    }
  },

  started() {
    this.fastify.listen(this.settings.port, "0.0.0.0").then(
      () => {},
      (err) => console.error(err)
    );
  },

  async stopped() {
    return await this.fastify.close();
  },
};
