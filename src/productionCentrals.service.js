"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "productionCentrals",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "uretim_santralleri",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let productionCentralstable = await this.broker.call(
        "productionCentrals.count"
      );
      const result = await request.query(
        "SELECT * FROM üretim_santralleri"
      );

      if (productionCentralstable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let productionCentrals = await this.broker.call(
          "productionCentrals.count"
        );
        if (productionCentrals == 0) {
          let record = await result.recordset.map((r) => {
            (r._id = String(r.id)),
              (r.id = r.id== null ? null:String(r.id)),
              (r.santral = r.santral== null ? null:String(r.santral))
            return r;
          });
          await this.adapter.insertMany(record);
          return true;
        }
      }

      if (productionCentralstable == 0 && result.recordset.length > 0) {
        let record = await result.recordset.map((r) => {
            (r._id = String(r.id)),
            (r.id = r.id== null ? null:String(r.id)),
            (r.santral = r.santral== null ? null:String(r.santral))
          return r;
        });
        await this.adapter.insertMany(record);
        return true;
      }
    },
  },
};
