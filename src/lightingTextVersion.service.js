'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'lightingTextVersion',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'lightingTextVersion',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			name:{type: 'string'},
			url: {type: 'string', optional: true},
			startDate: {type: 'date', optional: true},
		}
	}
};