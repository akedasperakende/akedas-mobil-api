const Cron = require('moleculer-cron');
let axios = require('axios');

var config = {
	user: process.env.USER,
  
	password: process.env.PASSWORD,
  
	server: process.env.SERVER,
  
	database: process.env.DATABASE,
  
	dialect: process.env.DIALECT,
	connectionTimeout: 600000,
	requestTimeout: 600000,
	pool: {
	  idleTimeoutMillis: 600000,
	  max: 100,
	},
	dialectOptions: {
	  instanceName: "SQLEXPRESS",
	},
  
	options: {
	  trustedConnection: true,
	  encrypt: true,
	  enableArithAbort: true,
	  trustServerCertificate: true,
	},
  };

module.exports = {
	name: 'cron',

	mixins: [Cron],

	crons: [
		{
			name: 'sentNotification',
			cronTime: '* * * * *',
			onTick: function () {
				console.log({
					nodeId: process.env.NODEID,
					MAINNODE: process.env.MAINNODE,
					isMain: process.env.NODEID == process.env.MAINNODE
				})
				if (process.env.NODEID == process.env.MAINNODE) {
					this.getLocalService('cron')
						.actions.sentNotifications();
				}
			}
		},
		
		{
			name: 'search-index',
			cronTime: '0 1/1 * * *',
			onTick: function () {
				this.getLocalService('search')
					.actions.reindex();
			}
		},
		{
			name: 'pharmacy',
			cronTime: '0/15 8 * * *',
			onTick: function () {
				this.getLocalService('cron')
					.actions.pharmacy();
			}
		},
		{
            name: 'deletesubscriber',
           cronTime: '0 6 * * *',
            onTick: function () {
                     this.getLocalService('cron')
                 .actions.deletesubscriber();
             }
         },
		{
            name: 'subscriber',
            cronTime: '5 6 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.subscriber();
            }
        },
        {
            name: 'deletingOldDataBill',
            cronTime:'5 5 5 * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.deletingOldDataBill();
            }
        },
        {
            name: 'deletebill',
            cronTime: '0 7 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.deletebill();
            }
        },
        {
            name: 'bill',
            cronTime: '5 7 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.bill();
            }
        },
        {
            name: 'deleteinstallment',
            cronTime: '0 8 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.deleteinstallment();
            }
        },
        {
            name: 'installment',
            cronTime: '5 8 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.installment();
            }
        },
		{
            name: 'deletecomparisondata',
			cronTime: '7 7 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.deletecomparisondata();
            }
        },
        {
            name: 'comparisondata',
			cronTime: '8 7 * * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.comparisondata();
            }
        },		
        {
            name: 'location',
            cronTime: '5 5 5 * *',
            onTick: function () {
                    this.getLocalService('cron')
                    .actions.location();
            }
        },
 
	],

	actions: {
		

		sentNotifications: {
			async handler(ctx) {
				if (process.env.NODE_ENV === 'development') return;


				let notifications = await ctx.broker.call('pushNotifications.find', {
					query: {
						$or: [{ active: true }, { sendNotification: true }],
						completed: false,
						date: { $lte: new Date() }
					}
				});
				notifications.forEach(async n => {
					let registeredUsers = [];
					this.broker.call('pushNotifications.update', {
						...n,
						completed: true
					});
					// if (n.registeredUsers.length > 0) {
					// 	registeredUsers = await ctx.broker.call('registeredUsers.find', {
					// 		query: { _id: { $in: n.registeredUsers } },
					// 		fields: ['_id', 'playerId', 'lang']
					// 	});
					// 	registeredUsers.forEach(element => {
					// 		if (element["lang"] === n.lang) {
					// 			n.sendNotification = true
					// 		}
					// 		else n.sendNotification = false
					// 	});
					// }
					if (n.area.length > 0) {
						registeredUsers = await ctx.broker.call('registeredUsers.find', {
							query: {},
							fields: ['_id', 'playerId', 'lang','muhatapNo']
						});

						let  bolge = await this.broker.call("location.find", {
							query: {
								_id:{$in:n.area.map((x) => x)}
						  	},	  							
						  });
						  let subscribers=[]

						if (n.district.length > 0) {
							let ilce=bolge.map((x)=>{
								let [ilce] = x.district.filter(a => n.district.some(b => a._id === b));  
								return ilce
							})
							if (n.neighbourhood.length > 0) {
								let mahalle=ilce.map((x)=>{
									let [mahalle] = x.neighbourhood.filter(a => n.neighbourhood.some(b => a._id === b));  
									return mahalle
								})
								await Promise.all(
									await registeredUsers.map(async(x)=>{
										let user = await ctx.broker.call("subscriber.find", {
											query: {
											  MUHATAP_NO: x.muhatapNo,
											  BOLGE:{$in:bolge.map((x) => x.area)},
											  ILCE:{$in:ilce.map((x) => x.name)},
											  MAHALLE:{$in:mahalle.map((x) => x.name)},
											},
										  });
										if(user.length>0){
										  subscribers.push(x)
										  return x
										}
									})
								);
								
							}
							else{
								
								await Promise.all(
									await registeredUsers.map(async(x)=>{
										let user = await ctx.broker.call("subscriber.find", {
											query: {
											  MUHATAP_NO: x.muhatapNo,
											  BOLGE:{$in:bolge.map((x) => x.area)},
											  ILCE:{$in:ilce.map((x) => x.name)}
											},
										  });
										if(user.length>0){
										  subscribers.push(x)
										  return x
										}
									})
								);
								
							}
						}
						else{	
							await Promise.all(
								await registeredUsers.map(async(x)=>{
									let user = await ctx.broker.call("subscriber.find", {
										query: {
										MUHATAP_NO: x.muhatapNo,
										BOLGE:{$in:bolge.map((x) => x.area)}
										},
									});
									if(user.length>0){
									subscribers.push(x)
									return x
									}
								}));
							}

						registeredUsers=subscribers

						registeredUsers.forEach(element => {
							if (element["lang"] === n.lang) {
								n.sendNotification = true
							}
							else n.sendNotification = false
						});
						 
					}
					else if(!(n.muhatapNo=="" || n.muhatapNo==null || n.muhatapNo=="undefined")) {
						console.log(n.muhatapNo)
						registeredUsers = await ctx.broker.call('registeredUsers.find', {
							query:  {muhatapNo:n.muhatapNo},
							fields: ['_id', 'playerId', 'lang','muhatapNo']
						});
						registeredUsers.forEach(element => {
							if (element["lang"] === n.lang) {
								n.sendNotification = true
							}
							else n.sendNotification = false
						});
					}
					else{
						console.log("TUMU");

						registeredUsers = await ctx.broker.call('registeredUsers.find', {
							query: {},
							fields: ['_id', 'playerId', 'lang']
						});
					}
					//burda aynı dil olmayanlar elenecek
					let hasPlayerIds = registeredUsers.filter(x => x.playerId != null && x.playerId.length > 0 && x.lang == n.lang);

					try {

						this.broker.call('notifications.insert', {
							entities: registeredUsers.map(a => ({
								isRead: false,
								registeredUserId: a._id,
								title: n.title,
								active: n.active,
								isIndividual: n.isIndividual,
								sendNotification: n.sendNotification,
								pushNotificationId: n._id,
								item_id: n.item_id,
								content: n.content,
								date: n.date,
								icon: n.icon,
							//	media: n.media,
							//	type: n.type,
							//	groups: n.groups || [],
								area: n.area || [],
								district: n.district || [],
								neighbourhood: n.neighbourhood || [],
								lang: n.lang || 'TR',
								subtype: n.subtype || ''
							}))
						});
						console.log("BİLDİRİM:", n.sendNotification + " - " + hasPlayerIds.length + " Kişi")
						if (hasPlayerIds.length > 0 && n.sendNotification) {

							let result = await axios.post('https://onesignal.com/api/v1/notifications', {
								'app_id': process.env.ONESIGNAL_APPID,
								'include_player_ids': hasPlayerIds.map(x => x.playerId),
								'data': {
									'title': n.title,
									'item_id': n.item_id,
									'subtype': n.subtype
								},
								'contents': { 'en': n.content, 'tr': n.content },
								'headings': { 'en': n.title, 'tr': n.title },
							}, {
								headers: {
									'Authorization': 'Basic ' + process.env.ONESIGNAL_APIKEY
								}
							});
						}




					}
					catch (err) {
						//this.logger.error(err);
					}
				});
			}
		},

		pharmacy: {
			async handler(ctx) {
				await ctx.broker.call('coordinates.pharmacy');
			}
		},


		deletingOldDataBill: {
            async handler(ctx) {
				
				let now = new Date();
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				 
				let result=await this.broker.call('bill.deletingOldDataBill',{month:month});
				let log=ctx.call('cronLog.create', 
				{
					table:"bill",
					action:"deletingOldDataBill",
					count: result
				});
				
				return log
            }
        },
 		deletebill: {
            async handler(ctx) {
				let now = new Date();
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				let today=now.getDay()+1
				let yesterday=now.getDay()
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('bill.deleteBill',{today:today,yesterday:yesterday,month:month});
			
				let log=ctx.call('cronLog.create', 
				{
					table:"bill",
					action:"delete",
					count: result
				});
				return log
            }
        },
		
        bill: {
            async handler(ctx) {

				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('bill.changeBill',{today:today,yesterday:yesterday,month:month});
					
				let log=ctx.call('cronLog.create', 
				{
					table:"bill",
					action:"insert",
					count: result
				});
				return log

            }
        },


		deletesubscriber: {
            async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('subscriber.deleteSubscriber',{today:today,yesterday:yesterday});
				let log=ctx.call('cronLog.create', 
				{
					table:"subscriber",
					action:"delete",
					count: result
				});
				return log
            }
        },
        subscriber: {
            async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('subscriber.changeSubscriber',{today:today,yesterday:yesterday});
				let log=ctx.call('cronLog.create', 
				{
					table:"subscriber",
					action:"insert",
					count: result
				});
				
				return log
            }
        },
		
		deleteinstallment: {
            async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('installment.deleteInstallment',{today:today,yesterday:yesterday,month:month});
				let log=ctx.call('cronLog.create', 
				{
					table:"installment",
					action:"delete",
					count: result
				});
				return log
            }
        },
        installment: {
            async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('installment.changeInstallment',{today:today,yesterday:yesterday,month:month});
				let log=ctx.call('cronLog.create', 
				{
					table:"installment",
					action:"insert",
					count: result
				});
				
				return log
            }
        },

		deletecomparisondata:{
			async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('comparisondata.deleteComparisonData',{today:today,yesterday:yesterday});
				let log=ctx.call('cronLog.create', 
				{
					table:"comparisondata",
					action:"delete",
					count: result
				});
				return log
			}
		},
		comparisondata:{
			async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('comparisondata.changeComparisonData',{today:today,yesterday:yesterday});
				let log=ctx.call('cronLog.create', 
				{
					table:"comparisondata",
					action:"insert",
					count: result
				});
				
				return log
			}
		},
		
		location: {
            async handler(ctx) {
				let now = new Date();
				let today=now.getDay()+1
				let yesterday=now.getDay()
				let month=now.getMonth()-6
				if(month<=0)month=12+month
				if(yesterday==0) yesterday=7
				let result=await this.broker.call('location.getLocation',{today:today,yesterday:yesterday,month:month});
				let log=ctx.call('cronLog.create', 
				{
					table:"location",
					action:"insert",
					count: result
				});
				
				return log
            }
        },
	}

};
