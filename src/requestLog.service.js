'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const nanoid = require('nanoid');


module.exports = {
	name: 'requestLog',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'requestLog',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			date: {type: 'date', optional: true},
			lang:{type: 'string', optional: true },
			version:{type: 'string', optional: true },
			userAgent:{type: 'string', optional: true },
			request: {type: 'object', optional: true },
			headers: {type: 'object', optional: true },
			user: {type: 'string', optional: true },
			url: {type: 'string', optional: true },
		}
	},

	actions: {
		async createLog(ctx) {
			let request = ctx.params.request;

			if(request.raw.originalUrl.startsWith("/files/")) {
				return
			} else {
				ctx.call('requestLog.create', 
				{
					date : new Date(),
					url : request.raw.originalUrl,
					version : request.headers['x-api-version'],
					lang : request.headers['x-api-lang'],
					userAgent : request.headers['user-agent'],
					headers : request.headers,
					request : request.body
				});
			}
		}
	},

	hooks: {
		before: {
			create: [(ctx) => {
			
				ctx.params._id = nanoid(25);
	
			}],
		}
	}
};