'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');
const dayjs = require('dayjs');
const nanoid = require('nanoid');

module.exports = {
	name: 'banners',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'banners',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			title:{type: 'string', optional: true},
			order: {type: 'number'},
			active: 'boolean',
			startDate: 'date',
			endDate: 'date',
			isLink :'boolean',
			redirectLink:{type: 'string', optional: true},
			type: {'type': 'string', optional: true},
			moduleId:{type: 'string', optional: true},
			media: {'type': 'object', props: {
				$$strict: true,
				type: {type:'string'},
				thumb: {type:'string'},
				duration: {type: 'number', optional: true},
				url: {type:'string'},
				_id: {type:'string'},
				mimeType: {type:'string'}
			}}
		}
	},


	hooks: {
		before: {
			create: [(ctx => {
				ctx.params._id = nanoid(25);
				ctx.params.redirectLink=ctx.params.redirectLink ? ctx.params.redirectLink : '';
				ctx.params.startDate = dayjs(ctx.params.startDate).toDate();
				ctx.params.endDate = dayjs(ctx.params.endDate).toDate();
			})],

			update: [(ctx => {
				ctx.params.redirectLink=ctx.params.redirectLink ? ctx.params.redirectLink : '';	
				ctx.params.startDate = dayjs(ctx.params.startDate).toDate();
				ctx.params.endDate = dayjs(ctx.params.endDate).toDate();
			})]
		}
	}
};