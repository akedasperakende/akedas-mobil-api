"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
var sql = require("mssql");
const dayjs = require('dayjs');
var config = {
  user: process.env.USER,
  password: process.env.PASSWORD,
  server: process.env.SERVER,
  database: process.env.DATABASE,
  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },
  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "productionQuantitiesTotal",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "uretim_oran_tablosu",
  settings: {
    maxPageSize: 1000000,
  },
  actions: {
    async getmove() {
      let pool = await sql.connect(config);
      const request = pool.request();
      let productionQuantitiestable = await this.broker.call(
        "productionQuantitiesTotal.count"
      );
      const result = await request.query(
        "SELECT * FROM uretim_oran_tablosu"
      );

      if (productionQuantitiestable > 0 && result.recordset.length > 0) {
        await this.adapter.removeMany({});
        let productionQuantitiesTotal = await this.broker.call(
          "productionQuantitiesTotal.count"
        );
        if (productionQuantitiesTotal == 0) {
          let record = await result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.tesis_turu =r.tesis_turu == null ? null:String(r.tesis_turu)),
              (r.total1 =r.total1  == null ? null: Number(r.total1)),
              (r.totar2 =r.totar2 == null ? null: Number(r.totar2)),
              (r.oran =r.oran == null ? null: Number(r.oran));
            return r;
          });
          await this.adapter.insertMany(record);
          return true;
        }
      }

      if (productionQuantitiestable == 0 && result.recordset.length > 0) {
        let record = await result.recordset.map((r) => {
          (r._id = nanoid(25)),
          (r.tesis_turu =r.tesis_turu == null ? null:String(r.tesis_turu)),
            (r.total1 =r.total1  == null ? null: Number(r.total1)),
            (r.totar2 =r.totar2 == null ? null: Number(r.totar2)),
            (r.oran =r.oran == null ? null: Number(r.oran));
          return r;
        });
        await this.adapter.insertMany(record);
        return true;
      }
    },
    async quantities() {
      let total= await this.broker.call("productionQuantitiesTotal.find", {
        query: { },
      });

      let result={
        _id:"2022",
        name:"2022 Yılı Toplam Üretim",
        total:total
      }
      return [result];
    }
  },
};
