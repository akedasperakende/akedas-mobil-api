'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'modules',
	mixins: [DbService],
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'modules',

	settings: {
		entityValidator: {
			$$strict: true,
			_id: {type: 'string'},
			name: 'string',
			icon: 'string',
			order: 'number',
			type: {type: 'string', enum: ['postwall', 'survey', 'events', 'support', 'workfamily', 'news', 'users', 'wordgames', 'education', 'infos']}
		}
	}
};