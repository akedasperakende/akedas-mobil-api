"use strict";
const DbService = require("moleculer-db");
const MongoDBAdapter = require("moleculer-db-adapter-mongo");
let nanoid = require("nanoid");
const dayjs = require("dayjs");
var sql = require("mssql");

var config = {
  user: process.env.USER,

  password: process.env.PASSWORD,

  server: process.env.SERVER,

  database: process.env.DATABASE,

  dialect: process.env.DIALECT,
  connectionTimeout: 600000,
  requestTimeout: 600000,
  pool: {
    idleTimeoutMillis: 600000,
    max: 100,
  },
  dialectOptions: {
    instanceName: "SQLEXPRESS",
  },

  options: {
    trustedConnection: true,
    encrypt: true,
    enableArithAbort: true,
    trustServerCertificate: true,
  },
};
module.exports = {
  name: "subscriber",
  mixins: [DbService],
  adapter: new MongoDBAdapter(
    process.env.MONGO_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    process.env.DB
  ),
  collection: "abone",

  actions: {
    async countSubscriberMssql() {
      //   let pool = ctx.params.pool;

      let pool = await sql.connect(config);
      const request = pool.request();
      const result = await request.query(
        "select count(*) as boyut from abone_2;"
      );

      pool.release();
      pool.close();
      let dongu = Math.ceil(result.recordset[0].boyut / 100000);
      return dongu;
    },
    async deleteSubscribers() {
      await this.adapter.removeMany({});
      return true;
    },
    async getmove(ctx) {
      let dongu = await this.broker.call("subscriber.countSubscriberMssql");
      let pool = await sql.connect(config);
      const request = pool.request();
      let subscribertable = await this.broker.call("subscriber.count");
      if (subscribertable > 0 && dongu > 0) {
        await this.adapter.removeMany({});
        let subscribertable = await this.broker.call("subscriber.count");
        let record; 
        if (subscribertable == 0) {
          for (var i = 0; i < dongu; i++) {
            const partial_result = await request.query(
              "select * from abone_2 ORDER BY SOZLESME_HESAP_NO OFFSET " +
                i * 100000 +
                " ROWS FETCH NEXT 100000 ROWS ONLY;"
            );
            record = await partial_result.recordset.map((r) => {
              (r._id = nanoid(25)),
              (r.TESISAT =r.TESISAT== null ? null:  String(r.TESISAT)),
              (r.KURULU_GUC =r.KURULU_GUC== null ? null:  Number(r.KURULU_GUC)),
              (r.GERILIM_GRUBU=r.GERILIM_GRUBU == null ? null: String(r.GERILIM_GRUBU)),
              (r.ETSO_KOD =r.ETSO_KOD == null ? null: String(r.ETSO_KOD)),
              (r.ABONE_TURU_STNT =r.ABONE_TURU_STNT == null ? null: String(r.ABONE_TURU_STNT)),
              (r.TEKIL_KOD =r.TEKIL_KOD == null ? null: String(r.TEKIL_KOD)),
              (r.TEL_NO = r.TEL_NO== null ? null: String(r.TEL_NO)),
              (r.MUHATAP_NO =r.MUHATAP_NO == null ? null: String(r.MUHATAP_NO)),
              (r.TC_NUMARASI =r.TC_NUMARASI == null ? null: String(r.TC_NUMARASI)),
              (r.SOZLESME_HESAP_NO =r.SOZLESME_HESAP_NO == null ? null: String(r.SOZLESME_HESAP_NO)),
              (r.SOZLESME_NO =r.SOZLESME_NO == null ? null: String(r.SOZLESME_NO)),
              (r.VERGI_NO =r.VERGI_NO  == null ? null: String(r.VERGI_NO)),
              (r.ILCE =r.ILCE == null ? null: String(r.ILCE)),
              (r.MAHALLE =r.MAHALLE == null ? null: String(r.MAHALLE)),
              (r.ADRES =r.ADRES == null ? null: String(r.ADRES)),
              (r.DURUM =r.DURUM == null ? null: String(r.DURUM)),
              (r.ZAMAN_TARIFESI =r.ZAMAN_TARIFESI == null ? null: String(r.ZAMAN_TARIFESI)),
              (r.ABONE_GRUBU =r.ABONE_GRUBU == null ? null: String(r.ABONE_GRUBU)),
              (r.AD =r.AD == null ? null: String(r.AD)),
              (r.SOYAD = r.SOYAD== null ? null: String(r.SOYAD));
              (r.ABONE_SINIFI =r.ABONE_SINIFI == null ? null: String(r.ABONE_SINIFI)),
              (r.ST_TAAHHUT_TARIHI =r.ST_TAAHHUT_TARIHI == null ? null: dayjs(r.ST_TAAHHUT_TARIHI).toDate()),
              (r.ST_ANLASMA_TURU =r.ST_ANLASMA_TURU == null ? null: String(r.ST_ANLASMA_TURU)),
              (r.KESIK_DURUM = r.KESIK_DURUM == null ? null: String(r.KESIK_DURUM)),
              (r.NAKIT_GB =r.NAKIT_GB == null ? null: Number(r.NAKIT_GB)),
              (r.SOZLESME_GUCU = r.SOZLESME_GUCU== null ? null: String(r.SOZLESME_GUCU)),
              (r.TEMINAT_MEKTUBU_GB =r.TEMINAT_MEKTUBU_GB == null ? null: Number(r.TEMINAT_MEKTUBU_GB)),
              (r.IL =r.IL == null ? null: String(r.IL)),
              (r.BOLGE =r.BOLGE == null ? null: String(r.BOLGE)),
              (r.MAIL =r.MAIL == null ? null: String(r.MAIL)),
              (r.ABONELIK_TARIH =r.ABONELIK_TARIH == null ? null: dayjs(r.ABONELIK_TARIH).toDate()),
              (r.ABONELIK_IPTAL_TARIH =r.ABONELIK_IPTAL_TARIH == null ? null: dayjs(r.ABONELIK_IPTAL_TARIH).toDate()),
              (r.KULLANIM_TIPI =r.KULLANIM_TIPI == null ? null: String(r.KULLANIM_TIPI)),
              (r.IBAN =r.IBAN == null ? null: String(r.IBAN)),
              (r.IBAN_KISI =r.IBAN_KISI == null ? null: String(r.IBAN_KISI));
              return r;
            });
            await this.adapter.insertMany(record);
          }
          return true;
        }
      }

      if (subscribertable == 0 && dongu > 0) {
        let record;
        for (var i = 0; i < dongu; i++) {
          const partial_result = await request.query(
            "select * from abone_2 ORDER BY SOZLESME_HESAP_NO OFFSET " +
              i * 100000 +
              " ROWS FETCH NEXT 100000 ROWS ONLY;"
          );
          record = await partial_result.recordset.map((r) => {
            (r._id = nanoid(25)),
            (r.TESISAT =r.TESISAT== null ? null:  String(r.TESISAT)),
            (r.KURULU_GUC =r.KURULU_GUC== null ? null:  Number(r.KURULU_GUC)),
            (r.GERILIM_GRUBU=r.GERILIM_GRUBU == null ? null: String(r.GERILIM_GRUBU)),
            (r.ETSO_KOD =r.ETSO_KOD == null ? null: String(r.ETSO_KOD)),
            (r.ABONE_TURU_STNT =r.ABONE_TURU_STNT == null ? null: String(r.ABONE_TURU_STNT)),
            (r.TEKIL_KOD =r.TEKIL_KOD == null ? null: String(r.TEKIL_KOD)),
            (r.TEL_NO = r.TEL_NO== null ? null: String(r.TEL_NO)),
            (r.MUHATAP_NO =r.MUHATAP_NO == null ? null: String(r.MUHATAP_NO)),
            (r.TC_NUMARASI =r.TC_NUMARASI == null ? null: String(r.TC_NUMARASI)),
            (r.SOZLESME_HESAP_NO =r.SOZLESME_HESAP_NO == null ? null: String(r.SOZLESME_HESAP_NO)),
            (r.SOZLESME_NO =r.SOZLESME_NO == null ? null: String(r.SOZLESME_NO)),
            (r.VERGI_NO =r.VERGI_NO  == null ? null: String(r.VERGI_NO)),
            (r.ILCE =r.ILCE == null ? null: String(r.ILCE)),
            (r.MAHALLE =r.MAHALLE == null ? null: String(r.MAHALLE)),
            (r.ADRES =r.ADRES == null ? null: String(r.ADRES)),
            (r.DURUM =r.DURUM == null ? null: String(r.DURUM)),
            (r.ZAMAN_TARIFESI =r.ZAMAN_TARIFESI == null ? null: String(r.ZAMAN_TARIFESI)),
            (r.ABONE_GRUBU =r.ABONE_GRUBU == null ? null: String(r.ABONE_GRUBU)),
            (r.AD =r.AD == null ? null: String(r.AD)),
            (r.SOYAD = r.SOYAD== null ? null: String(r.SOYAD));
            (r.ABONE_SINIFI =r.ABONE_SINIFI == null ? null: String(r.ABONE_SINIFI)),
            (r.ST_TAAHHUT_TARIHI =r.ST_TAAHHUT_TARIHI == null ? null: dayjs(r.ST_TAAHHUT_TARIHI).toDate()),
            (r.ST_ANLASMA_TURU =r.ST_ANLASMA_TURU == null ? null: String(r.ST_ANLASMA_TURU)),
            (r.KESIK_DURUM = r.KESIK_DURUM == null ? null: String(r.KESIK_DURUM)),
            (r.NAKIT_GB =r.NAKIT_GB == null ? null: Number(r.NAKIT_GB)),
            (r.SOZLESME_GUCU = r.SOZLESME_GUCU== null ? null: String(r.SOZLESME_GUCU)),
            (r.TEMINAT_MEKTUBU_GB =r.TEMINAT_MEKTUBU_GB == null ? null: Number(r.TEMINAT_MEKTUBU_GB)),
            (r.IL =r.IL == null ? null: String(r.IL)),
            (r.BOLGE =r.BOLGE == null ? null: String(r.BOLGE)),
            (r.MAIL =r.MAIL == null ? null: String(r.MAIL)),
            (r.ABONELIK_TARIH =r.ABONELIK_TARIH == null ? null: dayjs(r.ABONELIK_TARIH).toDate()),
            (r.ABONELIK_IPTAL_TARIH =r.ABONELIK_IPTAL_TARIH == null ? null: dayjs(r.ABONELIK_IPTAL_TARIH).toDate()),
            (r.KULLANIM_TIPI =r.KULLANIM_TIPI == null ? null: String(r.KULLANIM_TIPI)),
            (r.IBAN =r.IBAN == null ? null: String(r.IBAN)),
            (r.IBAN_KISI =r.IBAN_KISI == null ? null: String(r.IBAN_KISI));
            return r;
          });
          await this.adapter.insertMany(record);
        }
        return true;
      }
    },
    async listSubscriber(ctx) {
      let findSubscriber;
    
      let invoices;
      if (ctx.meta.user.vergiNo) {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and:[
              {VERGI_NO: ctx.meta.user.vergiNo}
            ]
          },
          fields: [
            "SOZLESME_HESAP_NO",
            "TEKIL_KOD",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "AD",
            "SOYAD",
            "ADRES",
            "ABONE_GRUBU",
            "ABONE_SINIFI",
            "ZAMAN_TARIFESI",
            "GERILIM_GRUBU",
            "KESIK_DURUM",
            "ST_TAAHHUT_TARIHI",
            "NAKIT_GB",
          "TEMINAT_MEKTUBU_GB"
          ],
        });
      } else {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo }           
            ],
          },

          fields: [
            "SOZLESME_HESAP_NO",
            "TEKIL_KOD",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "AD",
            "SOYAD",
            "ADRES",
            "ABONE_GRUBU",
            "ABONE_SINIFI",
            "ZAMAN_TARIFESI",
            "GERILIM_GRUBU",
            "KESIK_DURUM",
            "ST_TAAHHUT_TARIHI",
            "ABONELIK_TARIH",
            "NAKIT_GB",
          "TEMINAT_MEKTUBU_GB"
          ],
        });
      }
      if (findSubscriber) {
        let debit = await this.broker.call("debit.debitFind", {
            SOZLESME_HESAP_NO: findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
        });
        await findSubscriber.map((x) => {
            x.BINARYDEAL=false;
          if(x.ABONE_SINIFI=="SERBEST TUKETİCİ"){
            x.BINARYDEAL=true;
          }
          x.AD_SOYAD_UNVAN=x.AD+" "+x.SOYAD;
          x.TUKETICI_GRUBU_SINIFI =
            x.ZAMAN_TARIFESI +
            " / " +
            x.ABONE_GRUBU +
            " / " +
            x.GERILIM_GRUBU +
            " / " +
            x.ABONE_SINIFI;
          x.ABONELIK_TARIHI=x.ABONELIK_TARIH;
          x.GUVENCEBEDELI=x.NAKIT_GB? parseInt(x.NAKIT_GB):parseInt(x.TEMINAT_MEKTUBU_GB)||0;
          x.DEBITLIST=[];
          x.TOTALDEBIT = 0;
          x.MESSAGE = "Borcunuz Bulunmamaktadır.";
          return x;
        });

        findSubscriber.map((f, i) => {
          if (f.KESIK_DURUM === "Kesik") {
            findSubscriber[i].MESSAGE = "Hizmet kesildi";
            findSubscriber[i].DEBITSTATUS = "cleanClose";
          } else {
            findSubscriber[i].DEBITSTATUS = "cleanContinue";
          }
        });
        let debtFind;
        let findInstallment=false; 
        let invoiceBills;
        let installmentBill;
        let hire_purchase=[];
        let sub_array;
        if(debit){
        await Promise.all(
          debit.map(async (x)=>{
            if(x.FATURA_TIPI=="Taksit Planı"){
            [sub_array]=await this.broker.call('installment.find',{
                query:{
                  $and:[
                    {BELGE_NO:x.FATURA_NO},
                    {TAKSIT_SIRA:x.TAKSIT_SIRA}
                  ]
                }
              });
              if(sub_array){
              hire_purchase.push(sub_array);                
              }
              findInstallment=true;
            }
          })
        );
        if(hire_purchase.length>0){
        hire_purchase.sort(function(a, b) {
          return a.TAKSIT_SIRA - b.TAKSIT_SIRA;
        }); 
        }
      if(findInstallment==true){
      installmentBill= await this.adapter.db
      .collection("fatura")
      .distinct("TAKSIT_BELGE_NO", {
      TAKSIT_BELGE_NO:{$in: debit.map((x) => x.FATURA_NO)},
      });
      invoiceBills=await this.broker.call("bill.find",{query:{
      TAKSIT_BELGE_NO:{
        $in:installmentBill.map((x)=>x)
      }
      }})
    }
    invoices=await this.broker.call("bill.find",{query:{
      FATURA_SERI:{
                    $in:debit.map((x)=>x.FATURA_NO)
                  }
      },
      sort: '-FATURA_THK_BAS'});
          let totalDebit = await this.broker.call("debit.totalDebit", {
            SOZLESME_HESAP_NO: findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
          });
          let subJson;
          let installment;
          let notInstallment;
          let unpaid;
          await Promise.all(totalDebit.map(async (e) => {
            await Promise.all(findSubscriber.map(async (f, i) => {    
          let array=[];
              if (f.SOZLESME_HESAP_NO === e._id) {
                findSubscriber[i].TOTALDEBIT = e.TUTAR;
                findSubscriber[i].MESSAGE = "Borcunuz Bulunmaktadır.";
                debtFind=debit.filter(y => y.SOZLESME_HESAP_NO == e._id)
                installment=debtFind.filter(y=>y.FATURA_NO.startsWith("270")==true)
                notInstallment=invoices.filter(y=>y.FATURA_SERI.startsWith("270")==false)                
                unpaid=debtFind.filter(y=>y.FATURA_NO.startsWith("270")!=true)

                    unpaid = unpaid.filter(function(o1){
                            return !notInstallment.some(function(o2){    //  for diffrent we use NOT (!) befor obj2 here
                              return o1.FATURA_NO == o2.FATURA_SERI;          // id is unnique both array object
                            });
                          });
                          if(installment.length>0){
                            await Promise.all(invoiceBills.map(async (y)=>{
                              installmentBill=hire_purchase.filter(z => z.BELGE_NO == y.TAKSIT_BELGE_NO)
                              installmentBill=installmentBill.sort(function(a, b) {
                                return a.TAKSIT_SIRA - b.TAKSIT_SIRA;
                              });
                              
                              subJson={
                                  _id:y._id,
                                  SUBIDS:installmentBill
                                } 
                                array.push(subJson)
                            }))
                          }                   
                          if(notInstallment.length>0){
                              notInstallment=notInstallment.filter(y => y.SOZLESME_HESAP_NO == f.SOZLESME_HESAP_NO)
                              await Promise.all(notInstallment.map(async (x)=>{ 
                                if(f.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO ){
                                  subJson={
                                    _id:x._id,
                                    SUBIDS:[]
                                  }
                                  array.push(subJson)
                                }
                            }))
                          }
                          if(unpaid.length>0){
                          let t=unpaid.filter(y => y.SOZLESME_HESAP_NO == f.SOZLESME_HESAP_NO)
                            await Promise.all(t.map(async (x)=>{ 
                              if(f.SOZLESME_HESAP_NO == x.SOZLESME_HESAP_NO ){
                                subJson={
                                  _id:x._id,
                                  SUBIDS:[]
                                }
                                array.push(subJson)
                              }
                          }))
                        }
                findSubscriber[i].DEBITLIST=array
                if (f.KESIK_DURUM === "Kesik") {
                  findSubscriber[i].MESSAGE = "Hizmet kesildi";
                  findSubscriber[i].DEBITSTATUS = "debtClose";
                } else {
                  findSubscriber[i].DEBITSTATUS = "debtContinue";
                }
              }
            }));
          }));
          debit.map((x)=>{
            delete x.TAKSIT_SIRA,
            delete x.PARA_BIRIMI,
            delete x.TUTAR,
            delete x.SOYAD,
            delete x.AD,
            delete x.FATURA_TARIHI,
            delete x.SON_ODEME;
            delete x.SOZLESME_HESAP_NO;
            delete x.FATURA_NO;
            delete x.FATURA_TIPI;
          })
        }
        if(hire_purchase.length>0){
          hire_purchase.map((x)=>{
            delete x.BELGE_NO,
            delete x.TAKSIT_SIRA,
            delete x.SON_ODEME,
            delete x.ODEME_DURUM,
            delete x.SOZLESME_HESAP_NO,
            delete x.TUTAR
          })
        }
        findSubscriber.map((x) => {
          delete x.KESIK_DURUM;
          delete x.ST_TAAHHUT_TARIHI;
          delete x.AD,
          delete x.SOYAD
          delete x.NAKIT_GB,
          delete x.TEMINAT_MEKTUBU_GB
        });
      }
      return findSubscriber;
    },
    async boardApprovedListSubscriber(ctx) {
      let findSubscriber;
    
      if (ctx.meta.user.vergiNo) {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and:[
              {VERGI_NO: ctx.meta.user.vergiNo},
              { ABONE_TURU_STNT : "Kurul onaylı tarifelerle enerji alan tük" },
              { ABONE_GRUBU : "Mesken" }  
            ]
          },
          fields: [
            "SOZLESME_HESAP_NO",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "ADRES"
          ],
        });
      } else {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },
              { ABONE_TURU_STNT : "Kurul onaylı tarifelerle enerji alan tük" },
              { ABONE_GRUBU : "Mesken" }            
            ],
          },

          fields: [
            "SOZLESME_HESAP_NO",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "ADRES"
          ],
        });
      }
      return findSubscriber;
    },
    async changeSubscriber(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();
      
      let today="abone_"+ctx.params.today
      let yesterday="abone_"+ctx.params.yesterday
      const partial_result = await request.query(
       " select * from "+today+" except  select * from "+yesterday);

     let   record = await partial_result.recordset.map((r) => {
      (r._id = nanoid(25)),
      (r.TESISAT =r.TESISAT== null ? null:  String(r.TESISAT)),
      (r.KURULU_GUC =r.KURULU_GUC== null ? null:  Number(r.KURULU_GUC)),
      (r.GERILIM_GRUBU=r.GERILIM_GRUBU == null ? null: String(r.GERILIM_GRUBU)),
      (r.ETSO_KOD =r.ETSO_KOD == null ? null: String(r.ETSO_KOD)),
      (r.ABONE_TURU_STNT =r.ABONE_TURU_STNT == null ? null: String(r.ABONE_TURU_STNT)),
      (r.TEKIL_KOD =r.TEKIL_KOD == null ? null: String(r.TEKIL_KOD)),
      (r.TEL_NO = r.TEL_NO== null ? null: String(r.TEL_NO)),
      (r.MUHATAP_NO =r.MUHATAP_NO == null ? null: String(r.MUHATAP_NO)),
      (r.TC_NUMARASI =r.TC_NUMARASI == null ? null: String(r.TC_NUMARASI)),
      (r.SOZLESME_HESAP_NO =r.SOZLESME_HESAP_NO == null ? null: String(r.SOZLESME_HESAP_NO)),
      (r.SOZLESME_NO =r.SOZLESME_NO == null ? null: String(r.SOZLESME_NO)),
      (r.VERGI_NO =r.VERGI_NO  == null ? null: String(r.VERGI_NO)),
      (r.ILCE =r.ILCE == null ? null: String(r.ILCE)),
      (r.MAHALLE =r.MAHALLE == null ? null: String(r.MAHALLE)),
      (r.ADRES =r.ADRES == null ? null: String(r.ADRES)),
      (r.DURUM =r.DURUM == null ? null: String(r.DURUM)),
      (r.ZAMAN_TARIFESI =r.ZAMAN_TARIFESI == null ? null: String(r.ZAMAN_TARIFESI)),
      (r.ABONE_GRUBU =r.ABONE_GRUBU == null ? null: String(r.ABONE_GRUBU)),
      (r.AD =r.AD == null ? null: String(r.AD)),
      (r.SOYAD = r.SOYAD== null ? null: String(r.SOYAD));
      (r.ABONE_SINIFI =r.ABONE_SINIFI == null ? null: String(r.ABONE_SINIFI)),
      (r.ST_TAAHHUT_TARIHI =r.ST_TAAHHUT_TARIHI == null ? null: dayjs(r.ST_TAAHHUT_TARIHI).toDate()),
      (r.ST_ANLASMA_TURU =r.ST_ANLASMA_TURU == null ? null: String(r.ST_ANLASMA_TURU)),
      (r.KESIK_DURUM = r.KESIK_DURUM == null ? null: String(r.KESIK_DURUM)),
      (r.NAKIT_GB =r.NAKIT_GB == null ? null: Number(r.NAKIT_GB)),
      (r.SOZLESME_GUCU = r.SOZLESME_GUCU== null ? null: String(r.SOZLESME_GUCU)),
      (r.TEMINAT_MEKTUBU_GB =r.TEMINAT_MEKTUBU_GB == null ? null: Number(r.TEMINAT_MEKTUBU_GB)),
      (r.IL =r.IL == null ? null: String(r.IL)),
      (r.BOLGE =r.BOLGE == null ? null: String(r.BOLGE)),
      (r.MAIL =r.MAIL == null ? null: String(r.MAIL)),
      (r.ABONELIK_TARIH =r.ABONELIK_TARIH == null ? null: dayjs(r.ABONELIK_TARIH).toDate()),
      (r.ABONELIK_IPTAL_TARIH =r.ABONELIK_IPTAL_TARIH == null ? null: dayjs(r.ABONELIK_IPTAL_TARIH).toDate()),
      (r.KULLANIM_TIPI =r.KULLANIM_TIPI == null ? null: String(r.KULLANIM_TIPI)),
      (r.IBAN =r.IBAN == null ? null: String(r.IBAN)),
      (r.IBAN_KISI =r.IBAN_KISI == null ? null: String(r.IBAN_KISI));
      return r;
    });
     let addData= await this.adapter.insertMany(record);
     return addData.length
     
    },
    async deleteSubscriber(ctx) {

      let pool = await sql.connect(config);
      const request = pool.request();
      let today="abone_"+ctx.params.today
      let yesterday="abone_"+ctx.params.yesterday
      const partial_result = await request.query(
       " select * from "+yesterday+" except  select * from "+today);

      let rem = await this.adapter.removeMany({
        SOZLESME_HESAP_NO: {
           $in: partial_result.recordset.map((x) => x.SOZLESME_HESAP_NO)
      }
      });

      return rem
    },
    async binaryDeals(ctx){
      let findSubscriber;
      if (ctx.meta.user.vergiNo) {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and:[
              {VERGI_NO: ctx.meta.user.vergiNo},
              {ABONE_SINIFI:"SERBEST TUKETİCİ"} 
            ]
          },
          fields: [
            "SOZLESME_HESAP_NO",
            "TEKIL_KOD",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "AD",
            "SOYAD",
            "ADRES",
            "ABONE_GRUBU",
            "ABONE_SINIFI",
            "ZAMAN_TARIFESI",
            "GERILIM_GRUBU",
            "KESIK_DURUM",
            "ST_TAAHHUT_TARIHI"
          ],
        });
      } else {
        findSubscriber = await this.broker.call("subscriber.find", {
          query: {
            $and: [
              {$or:[
                { VERGI_NO: { $eq: null } },
                { VERGI_NO: { $eq: "null" } },
              ]},
              { TC_NUMARASI: ctx.meta.user.tcNo },
              {ABONE_SINIFI:"SERBEST TUKETİCİ"},             
            ],
          },

          fields: [
            "SOZLESME_HESAP_NO",
            "TEKIL_KOD",
            "MUHATAP_NO",
            "TC_NUMARASI",
            "AD",
            "SOYAD",
            "ADRES",
            "ABONE_GRUBU",
            "ABONE_SINIFI",
            "ZAMAN_TARIFESI",
            "GERILIM_GRUBU",
            "KESIK_DURUM",
            "ST_TAAHHUT_TARIHI"
          ],
        });
      }
      if (findSubscriber.length>0) {
        let debtFind;
        let debit = await this.broker.call("debit.debitFind", {
            SOZLESME_HESAP_NO:  findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
        });
        await findSubscriber.map((x) => {
          x.AD_SOYAD_UNVAN=x.AD+" "+x.SOYAD;
          x.TUKETICI_GRUBU_SINIFI =
            x.ZAMAN_TARIFESI +
            " / " +
            x.ABONE_GRUBU +
            " / " +
            x.GERILIM_GRUBU +
            " / " +
            x.ABONE_SINIFI;
          x.ABONELIK_TARIHI="2021-08-14T00:00:00.000Z";
          x.GUVENCEBEDELI=0;
          x.DEBITLIST=[];
          x.TOTALDEBIT = 0;
          x.MESSAGE = "Borcunuz Bulunmamaktadır.";
          return x;
        });

        findSubscriber.map((f, i) => {
          if (f.KESIK_DURUM === "Kesik") {
            findSubscriber[i].MESSAGE = "Hizmet kesildi";
            findSubscriber[i].DEBITSTATUS = "cleanClose";
          } else {
            findSubscriber[i].DEBITSTATUS = "cleanContinue";
          }
        });
        if (debit) {
          let totalDebit  = await this.broker.call("debit.debitFind", {
            SOZLESME_HESAP_NO:  findSubscriber.map((x) => x.SOZLESME_HESAP_NO)
        });
          totalDebit.map((e) => {
            findSubscriber.map((f, i) => {
              if (f.SOZLESME_HESAP_NO === e._id) {
                findSubscriber[i].TOTALDEBIT = e.TUTAR;
                findSubscriber[i].MESSAGE = "Borcunuz Bulunmaktadır.";
                debtFind=debit.filter(y => y.SOZLESME_HESAP_NO == e._id)
                findSubscriber[i].DEBITLIST=debtFind
                if (f.KESIK_DURUM === "Kesik") {
                  findSubscriber[i].MESSAGE = "Hizmet kesildi";
                  findSubscriber[i].DEBITSTATUS = "debtClose";
                } else {
                  findSubscriber[i].DEBITSTATUS = "debtContinue";
                }
              }
            });
          });
        }
        debit.map((x)=>{
          delete x.SOZLESME_HESAP_NO;
          delete x.FATURA_NO;
        })
        findSubscriber.map((x) => {
          delete x.KESIK_DURUM;
          delete x.ST_TAAHHUT_TARIHI;
          delete x.AD,
          delete x.SOYAD
        });
             return findSubscriber; 
      }
  else{
    let [resultMessage] = await ctx.call("resultMessage.find", {
    query: { code: "0035", lang: ctx.params.lang },
    });
    throw Error(resultMessage.message);
    }
    },
    async subscriberWithQr(ctx){
      let date=dayjs(ctx.params.ODEME_TARIHI).toDate()
      let SOZLESME_HESAP_NO=ctx.params.SOZLESME_HESAP_NO
      let [user] = await ctx.broker.call("subscriber.find", {
        query: {
          SOZLESME_HESAP_NO:SOZLESME_HESAP_NO
        },
        });
     if((user.TC_NUMARASI!=null&&user.TC_NUMARASI!="")||(user.VERGI_NO!=null&&user.VERGI_NO!="")){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0023", lang: 'TR'},
        });
        throw Error(resultMessage.message);
     }
     if((user.TC_NUMARASI!=ctx.meta.user.tcNo)||(user.VERGI_NO!=ctx.meta.user.vergiNo)){
      let [resultMessage] = await ctx.call("resultMessage.find", {
        query: { code: "0001", lang: "TR" },
      });
      throw Error(resultMessage.message);
    }
     let [paymentInfo] = await this.broker.call("paymentInfo.find", {
      sort: "-ODEME_TARIHI",
      query: {
          SOZLESME_HESABI:SOZLESME_HESAP_NO
        }
      });
      if(paymentInfo){
        if(dayjs(paymentInfo.ODEME_TARIHI).isSame(date)){
          let [user] = await ctx.broker.call("registeredUsers.updateInAkedas", SOZLESME_HESAP_NO, { meta: { user: ctx.meta.user } });
          let result={}
          result.resultBool= true
          result.messageId = "0033";
          return result;
        }
        else{
          let [resultMessage] = await ctx.call("resultMessage.find", {
            query: { code: "0030", lang: 'TR'},
            });
            throw Error(resultMessage.message);
        }
      }
      else{
        let [resultMessage] = await ctx.call("resultMessage.find", {
          query: { code: "0030", lang: 'TR'},
          });
          throw Error(resultMessage.message);
      }
    },
   
  },
};