'use strict';
const flexsearch = require('flexsearch');
const MongoClient = require('mongodb').MongoClient;
const uri = process.env.MONGO_URL;

flexsearch.registerMatcher({
 "ş": "s", "ı": "i", "ö": "o", "ğ": "g", "ü": "u"
})


module.exports = {
	name: 'search',

	actions: {
		async postSearch(ctx) {
			if (ctx.params.text)
				return this.posts ? this.posts.search({
					query: ctx.params.text.toLocaleLowerCase('tr'),
					suggest: true
				}) : [];
			else return []
		},

		async eventSearch(ctx) {
			if (ctx.params.text)
				return this.events ? this.events.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async openPositionsSearch(ctx) {
			if (ctx.params.text)
				return this.openPositions ? this.openPositions.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async campaingSearch(ctx) {
			if (ctx.params.text)
				return this.campaings ? this.campaings.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async coordinateSearch(ctx) {
			if (ctx.params.text)
				return this.coordinates ? this.coordinates.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async userSearch(ctx) {
			if (ctx.params.text) {
				return this.registeredUsers ? this.registeredUsers.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			} else {
				return []
			}
		},

		async informationsSearch(ctx) {
			if (ctx.params.text) {
				return this.registeredUsers ? this.registeredUsers.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			} else {
				return []
			}
		},

		async newsSearch(ctx) {
			if (ctx.params.text) {
				return this.news ? this.news.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			} else {
				return []
			}
		},

		async ourNewsSearch(ctx) {
			if (ctx.params.text) {
				return this.ourNews ? this.ourNews.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			} else {
				return []
			}
		},

		async podcastSearch(ctx) {
			if (ctx.params.text)
				return this.podcasts ? this.podcasts.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async secondhandSearch(ctx) {
			if (ctx.params.text)
				return this.secondhands ? this.secondhands.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async surveysSearch(ctx) {
			if (ctx.params.text)
				return this.surveys ? this.surveys.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async surveysWithMediaSearch(ctx) {
			if (ctx.params.text)
				return this.surveysWithMedia ? this.surveysWithMedia.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async ContactsSearch(ctx) {
			if (ctx.params.text)
				return this.contacts ? this.contacts.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async sessionCategoriesSearch(ctx) {
			if (ctx.params.text)
				return this.sessionCategories ? this.sessionCategories.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async speakersSearch(ctx) {
			if (ctx.params.text)
				return this.speakers ? this.speakers.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async sponsorsSearch(ctx) {
			if (ctx.params.text)
				return this.sponsors ? this.sponsors.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async competitionsSearch(ctx) {
			if (ctx.params.text)
				return this.competitions ? this.competitions.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async topicsSearch(ctx) {
			if (ctx.params.text)
				return this.topics ? this.topics.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async activityCategorySearch(ctx) {
			if (ctx.params.text)
				return this.activityCategory ? this.activityCategory.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},

		async shakeWinCategorySearch(ctx) {
			if (ctx.params.text)
				return this.shakeWinCategory ? this.shakeWinCategory.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},
		
		async serviceRoutesSearch(ctx) {
			if (ctx.params.text)
				return this.serviceRoutes ? this.serviceRoutes.search(ctx.params.text.toLocaleLowerCase('tr')) : [];
			else return []
		},



		async search(ctx) {
			if (ctx.params.text) {
				let txt = ctx.params.text.toLocaleLowerCase('tr');
				let registeredUsers = this.registeredUsers ? this.registeredUsers.search(txt, 5) : [];
				let posts = this.posts ? this.posts.search(txt, 5) : [];
				let informations = this.informations ? this.informations.search(txt, 5) : [];
				let events = this.events ? this.events.search(txt, 5) : [];
				let openPositions = this.openPositions ? this.openPositions.search(txt, 5) : [];
				let news = this.news ? this.news.search(txt, 5) : [];
				let ourNews = this.ourNews ? this.ourNews.search(txt, 5) : [];
				let coordinates = this.coordinates ? this.coordinates.search(txt, 5) : [];
				let campaings = this.campaings ? this.campaings.search(txt, 5) : [];
				let podcasts = this.podcasts ? this.podcasts.search(txt, 5) : [];
				let secondhands = this.secondhands ? this.secondhands.search(txt, 5) : [];
				let surveys = this.surveys ? this.surveys.search(txt, 5) : [];
				let surveysWithMedia = this.surveysWithMedia ? this.surveysWithMedia.search(txt, 5) : [];
				let contacts = this.contacts ? this.contacts.search(txt, 5) : [];
				let sessionCategories = this.sessionCategories ? this.sessionCategories.search(txt, 5) : [];
				let speakers = this.speakers ? this.speakers.search(txt, 5) : [];
				let sponsors = this.sponsors ? this.sponsors.search(txt, 5) : [];
				let competitions = this.competitions ? this.competitions.search(txt, 5) : [];
				let topics = this.topics ? this.topics.search(txt, 5) : [];
				let activityCategory = this.activityCategory ? this.activityCategory.search(txt, 5) : [];
				let shakeWinCategory = this.shakeWinCategory ? this.shakeWinCategory.search(txt, 5) : [];
				let serviceRoutes = this.serviceRoutes ? this.serviceRoutes.search(txt, 5) : [];



				let result = [...registeredUsers, ...posts, ...informations, ...events, ...openPositions, 
					...news, ...ourNews, ...coordinates, ...campaings, ...surveys, ...surveysWithMedia,
					...podcasts, ...secondhands, ...contacts, ...sessionCategories, ...speakers, ... sponsors,
					...competitions, ...topics, ...activityCategory, ...shakeWinCategory, ...serviceRoutes];
				
				// result = result.filter(i => i.startDate ? i.startDate && dayjs(i.startDate).toDate() < dayjs().toDate() : i)
				// result = result.filter(i => i.endDate ? i.endDate && dayjs(i.endDate).toDate() > dayjs().toDate() : i)
				// result = result.filter(i => i.active)
				return result
			}
			else return []
		},

		async reindex(ctx) {
			let client = this.client;
			this.indexUsers(client);
			this.indexInformations(client);
			this.indexPosts(client);
			this.indexEvents(client);
			this.indexOpenPositions(client);
			this.indexNews(client);
			this.indexOurNews(client);
			this.indexCoordinates(client);
			this.indexCampaings(client);
			this.indexPodcasts(client);
			this.indexSecondhands(client);
			this.indexSurveys(client);
			this.indexSurveysWithMedia(client);
			this.indexContacts(client);
			this.indexSessionCategories(client);
			this.indexSpeakers(client);
			this.indexSponsors(client);
			this.indexCompetitions(client);
			this.indexTopics(client);
			this.indexActivityCategory(client);
			this.indexShakeWinCategory(client);
			this.indexServiceRoutes(client);


		},

		async reindexCoordinates() {
			this.indexCoordinates(this.client);

		},
		async reindexUsers() {
			this.indexUsers(this.client);
		},
		async reindexInformations() {
			this.indexInformations(client);
		}
	},

	methods: {
		async indexPosts(client) {
			let posts = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB)
				.collection('posts').find({
					active: true,
					date: { $lte: new Date() }
				})
				.project({ comment: 1, coordinate: 1, active: 1 })
				.toArray();

			posts.add(items.map(x => ({
				_id: x._id,
				active: x.active,
				tid: 'postwall::' + x._id,
				type: 'postwall',
				isUser: false,
				title: (x.comment || '').toLocaleLowerCase('tr'),
				content: (x.coordinate ? (x.coordinate.address || '') : '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/postwall.png'
			})));

			this.posts = posts;
		},

		async indexEvents(client) {
			let events = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('events')
				.find({ 
					active: true,
					startDate: { $lte: new Date() },
					endDate: { $gte: new Date() }
				})
				.project({ name: 1, description: 1, content: 1, coordinate: 1, active: 1 })
				.toArray();

			events.add(items.map(x => ({
				_id: x._id,
				tid: 'events::' + x._id,
				active: x.active,
				type: 'events',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.description + ' ' + x.content + (x.coordinate && x.coordinate.address ? x.coordinate.address : '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/events.png'
			})));

			this.events = events;
		},

		async indexOpenPositions(client) {
			let openPositions = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('openPositions')
				.find({ 
					active: true,
					releaseDate: { $lte: new Date() },
					joinEndDate: { $gte: new Date() }
				})
				.project({ name: 1, description: 1, content: 1, coordinate: 1, active: 1 })
				.toArray();

			openPositions.add(items.map(x => ({
				_id: x._id,
				tid: 'openPositions::' + x._id,
				type: 'openPositions',
				active: x.active,
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.description + ' ' + x.content + (x.coordinate && x.coordinate.address ? x.coordinate.address : '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/openPositions.png'
			})));

			this.openPositions = openPositions;
		},

		async indexNews(client) {
			let news = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('news').find({
				active: true,
				date: { $lte: new Date() },
				$or: [{ notEnd: true }, { endDate: { $gte:  new Date() } }]
			})
				.project({ name: 1, active: 1 })
				.toArray();

			news.add(items.map(x => ({
				_id: x._id,
				tid: 'news::' + x._id,
				active: x.active,
				type: 'news',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/news.png'
			})));

			this.news = news;
		},

		async indexCampaings(client) {
			let campaings = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('campaings').find({
				active: true,
				startDate: { $lte: new Date() },
				endDate: { $gte: new Date() }
			})
				.project({ name: 1, sub_title: 1, cities: 1, active: 1 })
				.toArray();

			let cities = await client.db(process.env.DB).collection('cities').find({}).toArray();

			items.forEach(x => {
				if (x.cities.map) {
					x.cities = x.cities.map(cid => cities.find(city => city._id == cid))
						.filter(city => city != null)
						.map(city => city.name)
						.join(',');
				}
			});

			campaings.add(items.map(x => ({
				_id: x._id,
				tid: 'workfamily::' + x._id,
				active: x.active,
				type: 'workfamily',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.sub_title + ' ' + (x.cities || '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/workfamily.png'
			})));

			this.campaings = campaings;
		},

		async indexCoordinates(client) {
			let coordinates = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('coordinates').find({})
				.project({ name: 1, address: 1, search: 1 })
				.toArray();

			coordinates.add(items.map(x => ({
				_id: x._id,
				tid: 'health::' + x._id,
				type: 'health',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.search || '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/health.png'
			})));

			this.coordinates = coordinates;
		},

		async indexUsers(client) {

			let registeredUsers = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB)
				.collection('registeredUsers').find({
					deleted: { $ne: true }
				})
				.project({ name: 1, lastname: 1, email: 1, avatar: 1, department: 1, position: 1, company: 1, active: 1 })
				.toArray();

			registeredUsers.add(items.map(x => ({
				_id: x._id,
				tid: 'user::' + x._id,
				active: true, // active parametresi üstte ki filterda takılmaması için zorunlu olarak true gönderildi.
				type: 'user',
				isUser: true,
				title: (x.name + " " + x.lastname || '').toLocaleLowerCase('tr'),
				content: (x.department + " " + x.position || "" + " " + x.company || "" + " " + x.email).toLocaleLowerCase('tr'),
				// img: x.avatar.url
			})));

			this.registeredUsers = registeredUsers;
		},

		async indexInformations(client) {


			let informations = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB)
				.collection('informations').find({ 
					active: true 
				})
				.project({ header: 1, content: 1, module: 1, active: 1 })
				.toArray();

			informations.add(items.map(x => ({
				_id: x._id,
				tid: 'infos::' + x._id,
				type: 'infos',
				active: x.active,
				moduleId: x.module,
				isUser: true,
				title: (x.header || '').toLocaleLowerCase('tr'),
				content: (x.content || '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/' + x.module + '.png'
			})));

			this.informations = informations;
		},

		async indexOurNews(client) {
			let ourNews = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('ourNews').find({ 
				active: true,
				date: { $lte: new Date() },
				endDate: { $gte: new Date() }
			})
				.project({ name: 1, active: 1 })
				.toArray();

			ourNews.add(items.map(x => ({
				_id: x._id,
				tid: 'ourNews::' + x._id,
				type: 'ourNews',
				active: x.active,
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/ourNews.png'
			})));
			this.ourNews = ourNews;
		},

		async indexPodcasts(client) {
			let podcasts = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('podcasts').find({
				active: true
			})
				.project({ title: 1, active: 1})
				.toArray();

			podcasts.add(items.map(x => ({
				_id: x._id,
				tid: 'podcasts::' + x._id,
				type: 'podcasts',
				item_id: x._id,
				active: x.active,
				startDate: x.startDate,
				endDate: x.endDate,
				title: (x.title || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/podcasts.png'
			})));

			this.podcasts = podcasts;
		},

		async indexSecondhands(client) {
			let secondhands = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('secondhand').find({
				active: true
			})
				.project({ name: 1, active: 1, productName: 1, content: 1, carBrand: 1 })
				.toArray();

			secondhands.add(items.map(x => ({
				_id: x._id,
				tid: 'secondhand::' + x._id,
				type: 'secondhand',
				item_id: x._id,
				active: x.active,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: ((x.productName || '') + ' ' + (x.content || '') + ' ' + (x.carBrand || '')).toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/secondhand.png'
			})));

			this.secondhands = secondhands;
		},

		async indexSurveys(client) {
			
			let surveys = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('surveys').find({
				active: true
			})
				.project({ name: 1, active: 1})
				.toArray();

			surveys.add(items.map(x => ({
				_id: x._id,
				tid: 'survey::' + x._id,
				type: 'survey',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/survey.png'
			})));


			this.surveys = surveys;
		},

		async indexSurveysWithMedia(client) {
			let surveysWithMedia = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('surveysWithMedia').find({
				active: true
			})
				.project({ name: 1, active: 1})
				.toArray();

			surveysWithMedia.add(items.map(x => ({
				_id: x._id,
				tid: 'surveysWithMedia::' + x._id,
				type: 'surveysWithMedia',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/surveysWithMedia.png'
			})));

			this.surveysWithMedia = surveysWithMedia;
		},

		async indexContacts(client) {
			let contacts = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('contacts').find({})
				.project({ name: 1})
				.toArray();

			contacts.add(items.map(x => ({
				_id: x._id,
				tid: 'contacts::' + x._id,
				type: 'contacts',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/contacts.png'
			})));

			this.contacts = contacts;
		},

		async indexSessionCategories(client) {
			let sessionCategories = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('sessionCategory').find({})
				.project({ name: 1, search: 1 })
				.toArray();

			sessionCategories.add(items.map(x => ({
				_id: x._id,
				tid: 'sessions::' + x._id,
				type: 'sessions',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/sessions.png'
			})));

			this.sessionCategories = sessionCategories;
		},

		async indexSpeakers(client) {
			let speakers = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('speakers').find({})
				.project({ name: 1, company: 1, search: 1 })
				.toArray();

			speakers.add(items.map(x => ({
				_id: x._id,
				tid: 'speakers::' + x._id,
				type: 'speakers',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.company || '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/speakers.png'
			})));

			this.speakers = speakers;
		},

		async indexSponsors(client) {
			let sponsors = flexsearch.create('match', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('sponsors').find({})
				.project({ name: 1, company: 1, search: 1 })
				.toArray();

			sponsors.add(items.map(x => ({
				_id: x._id,
				tid: 'sponsors::' + x._id,
				type: 'sponsors',
				isUser: false,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: (x.company || '').toLocaleLowerCase('tr'),
				img: process.env.CDN + '/modules/sponsors.png'
			})));

			this.sponsors = sponsors;
		},

		async indexCompetitions(client) {
			let competitions = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('competitions').find({
				active: true,
				startDate: { $lte: new Date() },
				endDate: { $gte: new Date() }
			})
				.project({ name: 1, active: 1})
				.toArray();

			competitions.add(items.map(x => ({
				_id: x._id,
				tid: 'competitions::' + x._id,
				type: 'competitions',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/competitions.png'
			})));

			this.competitions = competitions;
		},

		async indexTopics(client) {
			let topics = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('topics').find({
				active: true
			})
				.project({ topic: 1, active: 1})
				.toArray();

			topics.add(items.map(x => ({
				_id: x._id,
				tid: 'topics::' + x._id,
				type: 'topics',
				item_id: x._id,
				title: (x.topic || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/topics.png'
			})));

			this.topics = topics;
		},

		async indexActivityCategory(client) {
			let activityCategory = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('activityCategory').find({
				active: true
			})
				.project({ name: 1, active: 1})
				.toArray();

			activityCategory.add(items.map(x => ({
				_id: x._id,
				tid: 'activity::' + x._id,
				type: 'activity',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/activity.png'
			})));

			this.activityCategory = activityCategory;
		},

		async indexShakeWinCategory(client) {
			let shakeWinCategory = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('shakeWinCategory').find({
				active: true
			})
				.project({ name: 1, active: 1})
				.toArray();

			shakeWinCategory.add(items.map(x => ({
				_id: x._id,
				tid: 'shakeWin::' + x._id,
				type: 'shakeWin',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/shakeWin.png'
			})));

			this.shakeWinCategory = shakeWinCategory;
		},

		async indexServiceRoutes(client) {
			let serviceRoutes = flexsearch.create('memory', {
				doc: {
					id: 'tid',
					field: ['title', 'content']
				}
			});

			let items = await client.db(process.env.DB).collection('serviceRoutes').find({
				active: true
			})
				.project({ name: 1, active: 1})
				.toArray();

			serviceRoutes.add(items.map(x => ({
				_id: x._id,
				tid: 'route::' + x._id,
				type: 'route',
				item_id: x._id,
				title: (x.name || '').toLocaleLowerCase('tr'),
				content: '',
				img: process.env.CDN + '/modules/route.png'
			})));

			this.serviceRoutes = serviceRoutes;
		},

	},

	async started() {
		const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

		await client.connect();
		this.client = client;
		this.indexUsers(client);
		this.indexPosts(client);
		this.indexEvents(client);
		this.indexOpenPositions(client);
		this.indexNews(client);
		this.indexCoordinates(client);
		this.indexCampaings(client);
		this.indexInformations(client);
		this.indexOurNews(client);
		this.indexPodcasts(client);
		this.indexSecondhands(client);
		this.indexSurveys(client);
		this.indexSurveysWithMedia(client);
		this.indexContacts(client);
		this.indexSessionCategories(client);
		this.indexSpeakers(client);
		this.indexSponsors(client);
		this.indexCompetitions(client);
		this.indexTopics(client);
		this.indexActivityCategory(client);
		this.indexShakeWinCategory(client);
		this.indexServiceRoutes(client);

	}
};

