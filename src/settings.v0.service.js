'use strict';
const DbService = require('moleculer-db');
const MongoDBAdapter = require('moleculer-db-adapter-mongo');

module.exports = {
	name: 'settings',
	mixins: [DbService],
	version: 0,
	adapter: new MongoDBAdapter(process.env.MONGO_URL, 
		{ useNewUrlParser: true , useUnifiedTopology: true }, process.env.DB),
	collection: 'settings',

	actions: {
		flat: {
			cache: {
        keys: ["userId"]
      },
			async handler (ctx) {
				let now = new Date();
				
				let userId = ctx.params.userId;
				let settings = await this.adapter.find({});
	
				let modules = await this.broker.call('modules.find',{
					sort: 'order', 
					query: {version: '0.0'}
				});
				let result = {};
				settings.forEach(x => {
					result[x._id] = 
						x.items ? x.items : x;
				});
	
				result.modules = modules;
				
				return result;
			}
		},

		async addType(ctx){
			ctx.call('settings.update', {
				_id: 'coordinate_types',
				name: 'Kordinat Tipleri',
				items: [
					{
						_id: 'hastane',
						name: 'Hastane',
						subs: [
							{ _id: 'hastane', name: 'Hastane' },
							{ _id: 'tipmerkezi', name: 'Tıp Merkezi' },
							{ _id: 'resmi', name: 'Resmi' }
						]
					},
					{ _id: 'eczane', name: 'Eczane', subs: [] },
					{ _id: 'laboratuvar', name: 'Laboratuvar', subs: [] },
					{ _id: 'doktor', name: 'Doktor', subs: [] },
					{ _id: 'optik', name: 'Optik', subs: [] },
					{ _id: 'tipmerkezi', name: 'Tıp Merkezi', subs: [] }
				]
			})
		}
	}
};